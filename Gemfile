source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '5.2.1'

gem 'rails-env-credentials'

gem 'pg'
gem 'pg_query' # used by pghero
gem 'pghero'

gem 'puma', '~> 3.7'

# memcached
gem 'dalli'

gem 'enumerize'
gem 'phony_rails'

gem 'browser'
gem 'ckeditor'
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'sass'
gem 'sass-rails'
gem 'simple_form'
gem 'slim'
gem 'turbolinks', '~> 5'

gem 'foundation_emails', github: 'zurb/foundation-emails', branch: 'develop'

gem 'hashids'
gem 'httparty'

gem 'oj'
gem 'searchkick'
gem 'typhoeus'

gem 'spree', github: 'spree/spree'
gem 'spree_auth_devise', github: 'public-market/spree_auth_devise'
gem 'spree_batch_api', github: 'public-market/spree_batch_api'
gem 'spree_gateway', github: 'spree/spree_gateway', tag: 'v3.3.3'
gem 'spree_multi_vendor', github: 'public-market/spree_multi_vendor'
gem 'spree_pages'
gem 'spree_reviews', github: 'public-market/spree_reviews'
gem 'spree_searchkick', github: 'public-market/spree_searchkick'
gem 'spree_sitemap', github: 'spree-contrib/spree_sitemap'
gem 'spree_social', github: 'public-market/spree_social'

gem 'tracking_number', github: 'public-market/tracking_number'

gem 'delayed_paperclip'
gem 'fog-google'
gem 'google-cloud-storage', '~> 1.8', require: false
gem 'sidekiq'
gem 'sidekiq-limit_fetch'
gem 'sidekiq-scheduler'

gem 'posix-spawn' # https://github.com/thoughtbot/paperclip/issues/2620 & https://github.com/thoughtbot/terrapin#posix-spawn

gem 'json_api_client'

gem 'mixpanel-ruby'

gem 'intercom-rails'

gem 'swagger-blocks'

gem 'slack-notifier'

gem 'dry-initializer', '2.5.0' # https://github.com/rom-rb/rom/issues/503#issuecomment-423478433

gem 'newrelic_rpm'

group :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'chromedriver-helper', require: false
  gem 'database_cleaner'
  gem 'elasticsearch-extensions'
  gem 'factory_bot'
  gem 'ffaker'
  gem 'rails-controller-testing'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'rspec-sidekiq'
  gem 'selenium-webdriver', require: false
  gem 'simplecov', require: false
  gem 'vcr'
  gem 'webmock'
  gem 'zonebie'
end

group :development do
  gem 'bullet'
  gem 'bundle-audit', require: false
  gem 'byebug'
  gem 'guard', require: false
  gem 'guard-rspec', require: false
  gem 'listen', require: false
  gem 'overcommit', require: false
  gem 'pry', '0.12.0'
  gem 'pry-rails'
  gem 'rack-mini-profiler', require: false
  gem 'spring', require: false
  gem 'spring-watcher-listen', require: false
  gem 'web-console'
end

group :test, :development do
  gem 'parallel_tests'
  gem 'rubocop', require: false
  gem 'rubocop-rspec', require: false
end

group :production, :staging do
  gem 'rack-attack'
  gem 'sentry-raven'
  gem 'therubyracer'
  gem 'uglifier'
end

group :api_db do
  gem 'sqlite3'
end
