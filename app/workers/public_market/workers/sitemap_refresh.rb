module PublicMarket
  module Workers
    class SitemapRefresh
      include Sidekiq::Worker

      def perform
        SitemapGenerator::Interpreter.run

        return unless Rails.env.production?

        SitemapGenerator::Sitemap.ping_search_engines
      end
    end
  end
end
