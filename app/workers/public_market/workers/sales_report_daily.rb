require 'sidekiq-scheduler'

module PublicMarket
  module Workers
    class SalesReportDaily
      include Sidekiq::Worker

      def perform(vendor_slug)
        vendor = Spree::Vendor.find_by(slug: vendor_slug)
        puts "Send daily sales report to #{vendor_slug}"

        return if vendor.blank?

        csv = Spree::Orders::SalesReportAction.call(vendor, Time.current)
        Spree::OrderMailer.sales_report(vendor, csv).deliver
      end
    end
  end
end
