require 'sidekiq-scheduler'

module PublicMarket
  module Workers
    class GenerateDailyRewards
      include Sidekiq::Worker

      def perform
        date = Time.current - 1.hour
        date = date.in_time_zone('Pacific Time (US & Canada)')

        puts "Generate daily rewards at #{date}..."
        Spree::Rewards::GenerateDailyRewardsAction.call(date)
      end
    end
  end
end
