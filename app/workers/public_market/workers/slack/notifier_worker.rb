module PublicMarket
  module Workers
    module Slack
      require 'slack-notifier'

      class NotifierWorker
        include Sidekiq::Worker

        attr_reader :options

        def perform(options)
          @options = options.with_indifferent_access
          send_slack_notification
        end

        private

        def send_slack_notification
          msg = message

          return if msg.blank?

          if Rails.env.development? || Rails.env.test?
            puts "---------- Slack Message: #{msg}"
          else
            notifier = ::Slack::Notifier.new(webhook_url, channel: channel_name, username: username)
            notifier.ping(msg)
          end
        end

        def message
          options[:message]
        end

        def channel_name
          options[:channel]
        end

        def username
          options[:username]
        end

        def webhook_url
          Rails.application.credentials[:slack_data_reconcilation]
        end

        def spree_url_helpers
          @spree_url_helpers ||= Spree::Core::Engine.routes.url_helpers
        end
      end
    end
  end
end
