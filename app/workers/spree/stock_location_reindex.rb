module Spree
  class StockLocationReindex
    include Sidekiq::Worker
    sidekiq_options queue: :recalculation

    def perform(stock_location_id)
      Variant.joins(stock_items: :stock_location)
             .where(spree_stock_locations: { id: stock_location_id })
             .find_in_batches(&method(:push_bulk))
    end

    private

    def push_bulk(variants)
      args = variants.map { |v| ['Spree::Variant', v.id] }
      Sidekiq::Client.push_bulk('class' => Spree::AsyncTouchWorker, 'queue' => :recalculation, 'args' => args)
    end
  end
end
