module Spree
  class AsyncTouchWorker
    include Sidekiq::Worker
    sidekiq_options queue: :recalculation

    def perform(klass, id)
      Searchkick.callbacks(:inline) do
        klass.constantize.find_by(id: id).touch # rubocop:disable Rails/SkipsModelValidations
      end
    end
  end
end
