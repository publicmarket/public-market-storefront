module Spree
  module UserMailerDecorator
    def self.prepended(base)
      base.helper Spree::FrontendHelper
    end

    def welcome(user_id)
      user = Spree::User.find(user_id)

      mail_template(user, :welcome, root_url: spree.root_url)
    end

    def reset_password_instructions(user, token, _opts = {})
      edit_password_reset_url = spree.edit_spree_user_password_url(reset_password_token: token)

      mail_template(user, :reset_password_instructions, edit_password_reset_url: edit_password_reset_url)
    end

    def confirmation_instructions(user, token, _opts = {})
      confirmation_url = spree.spree_user_confirmation_url(confirmation_token: token)

      template, user_email =
        if user.pending_reconfirmation?
          [:reconfirmation, user.unconfirmed_email]
        else
          [:confirmation, user.email]
        end

      mail_template(user_email, template, email: user_email, confirmation_url: confirmation_url)
    end

    def email_change(user_id, old_email)
      user = Spree::User.find(user_id)
      mail_template(old_email, :email_change, first_name: user.first_name)
    end

    def lead_whitelisted(lead_id)
      lead = Spree::Lead.find_by(id: lead_id)
      return unless lead

      headers(reply_to: '"Landon Howell" <landon@publicmarket.io>')
      mail_template(lead, :lead_whitelisted,
                    whitelisted_email: lead.email,
                    access_url: early_access_url(email: lead.email),
                    from: '"Landon from Public Market" <landon@publicmarket.io>')
    end

    def daily_rewards_update(user_id, date)
      user = Spree::User.find(user_id)
      date = Date.parse(date).in_time_zone('Pacific Time (US & Canada)')
      user_reward = user.user_rewards.find_by(date: date)
      return unless user_reward

      mail_template(user, :daily_rewards_update,
                    daily_rewards_total: ActionController::Base.helpers.number_to_currency(user_reward.amount),
                    daily_rewards: render_to_string(partial: 'mailer/rewards/daily_rewards', locals: { reward: user_reward }),
                    rewards_url: rewards_url,
                    date: date.strftime('%B %d, %Y'),
                    orders: render_to_string(partial: 'mailer/orders/date_orders', locals: { orders:
                      user.orders.with_state(:complete).completed_between(date.beginning_of_day, date.end_of_day),
                                                                                             rewards: user_reward.amount }))
    end
  end

  UserMailer.prepend(UserMailerDecorator)
end
