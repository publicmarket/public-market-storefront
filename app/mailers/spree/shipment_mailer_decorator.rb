module Spree
  module ShipmentMailerDecorator
    def self.prepended(base)
      base.helper Spree::FrontendHelper
    end

    def shipped_email(shipment, _resend = false)
      shipment = shipment.respond_to?(:id) ? shipment : Spree::Shipment.find(shipment)
      order = shipment.order

      opts = default_opts(order, shipment).merge(
        shipped_items: shipped_items(shipment),
        canceled_items: canceled_items(shipment),
        strive: strive(shipment),
        shipment_tracking: render_to_string(partial: 'mailer/shipments/shipment_tracking', locals: { shipment: shipment }),
        shipment_rewards: render_to_string(partial: 'mailer/shipments/shipment_rewards', locals: { shipment: shipment })
      ).merge(subject_opts(shipment.line_items.sort_by(&:quantity).reverse))

      mail_template(order, shipment.partial? ? :shipment_partially_shipped : :shipment_shipped, opts)
    end

    def cancel_email(shipment, _resend = false)
      shipment = shipment.respond_to?(:id) ? shipment : Spree::Shipment.find(shipment)
      order = shipment.order

      opts = default_opts(order, shipment).merge(
        canceled_items: canceled_items(shipment),
        strive: strive(shipment)
      ).merge(subject_opts(shipment.line_items.sort_by(&:quantity).reverse))

      mail_template(order, :shipment_cancel, opts)
    end

    def rate_email(shipment, resend = false)
      shipment = shipment.respond_to?(:id) ? shipment : Spree::Shipment.find(shipment)
      return if shipment.rated? && !resend

      order = shipment.order

      opts = default_opts(order, shipment).merge(
        shipped_items: shipped_items(shipment, title: t('emails.quantity_shipped')),
        canceled_items: canceled_items(shipment),
        shipment_rewards: render_to_string(partial: 'mailer/shipments/shipment_rewards', locals: { shipment: shipment }),
        shipment_rate: render_to_string(partial: 'mailer/shipments/shipment_rate', locals: { shipment: shipment })
      )

      mail_template(order, :shipment_rate, opts)
    end

    private

    def default_opts(order, shipment)
      {
        order_id: order.number,
        order_url: spree.order_url(order),
        first_name: user_first_name(order: order),
        vendor_name: shipment.vendor.name,
        order_info: order_info(order)
      }
    end

    def shipped_items(shipment, title: nil)
      render_to_string(partial: 'mailer/shipments/shipped_items', locals: { shipment: shipment, title: title })
    end

    def canceled_items(shipment)
      render_to_string(partial: 'mailer/shipments/canceled_items', locals: { shipment: shipment })
    end

    def order_info(order)
      render_to_string(partial: 'mailer/orders/order_info', locals: { order: order })
    end

    def strive(shipment)
      render_to_string(partial: 'mailer/shipments/strive', locals: { shipment: shipment })
    end
  end

  ShipmentMailer.prepend(ShipmentMailerDecorator)
end
