module Spree
  module OrderMailerDecorator
    def self.prepended(base)
      base.helper Spree::FrontendHelper
    end

    def confirm_email(order, _resend = false)
      return if order.blank?

      order = order.respond_to?(:id) ? order : Spree::Order.find(order)

      opts = {
        order_id: order.number,
        first_name: user_first_name(order: order),
        order_url: spree.order_url(order),
        order_card: render_to_string(partial: 'mailer/orders/order_card', locals: { order: order }),
        order_info: render_to_string(partial: 'mailer/orders/order_info', locals: { order: order })
      }.merge(subject_opts(order.line_items))

      mail_template(order, :order_confirmation, opts)
    end

    def vendor_order(order, vendor)
      return if order.blank? || vendor.blank?

      order = order.respond_to?(:id) ? order : Spree::Order.find(order)
      vendor = vendor.respond_to?(:id) ? vendor : Spree::Vendor.find(vendor)
      order_view = Orders::VendorView.new(order, vendor)

      opts = {
        order_id: order.number,
        vendor_name: vendor.name,
        order_url: spree.edit_admin_order_url(order),
        vendor_order_card: render_to_string(partial: 'mailer/orders/vendor_order_card', locals: { order: order_view })
      }.merge(subject_opts(order_view.line_items))

      mail_template(vendor.customer_support_email, :vendor_order, opts)
    end

    def sales_report(vendor, csv)
      headers(
        from: '"Public Market | Merchant Relations" <merchants@publicmarket.io>',
        reply_to: '"Public Market | Merchant Relations" <merchants@publicmarket.io>',
        bcc: 'darin@publicmarket.io'
      )

      opts = {
        vendor_name: vendor.name
      }

      attachments['sales_report.csv'] = csv

      mail_template(vendor.rep_email, :vendor_sales_report_daily, opts)
    end
  end

  OrderMailer.prepend(OrderMailerDecorator)
end
