module Spree
  module BaseMailerDecorator
    def self.prepended(base)
      base.include ActionView::Helpers::TextHelper
    end

    def preview_error_email(message)
      @body = "<h1 class='text-danger'>#{message}</h1>"
      @smtp_headers = { asm_groups_to_display: [] }
      mail(to: 'admin@publicmarket.io', subject: 'Error in preview', template_path: 'mailer', template_name: 'default')
    end

    protected

    def mail_template(resource, template, opts = {})
      @body = I18n.t(['emails', template, 'body'].join('.'), opts)
      @preview = I18n.t(['emails', template, 'preview'].join('.'), opts.merge(default: nil))
      @footer = I18n.t(['emails', template, 'footer'].join('.'), opts.merge(default: nil))
      @bottom = I18n.t(['emails', template, 'bottom'].join('.'), opts.merge(default: nil))
      subject = I18n.t(['emails', template, 'subject'].join('.'), opts)

      set_smtp_headers(template)

      resource_email = resource.is_a?(String) ? resource : resource.email

      headers = { to: resource_email, subject: subject, template_path: 'mailer', template_name: 'default' }
      headers[:from] = opts[:from] if opts[:from]
      mail(headers)
    end

    def set_smtp_headers(template)
      sendgrid_categories = I18n.t(['emails', template, 'categories'].join('.')) || [template]
      categories = [Rails.env, template] | sendgrid_categories

      @smtp_headers = {
        category: categories,
        sub: {
          '%rawUnsubscribe%': ['<%asm_group_unsubscribe_raw_url%>'],
          '%rawPreferences%': ['<%asm_preferences_raw_url%>']
        }
      }

      @smtp_headers[:asm_group_id] = unsubscribe_category(sendgrid_categories)
      @smtp_headers[:asm_groups_to_display] = unsubscribe_preferences

      headers['X-SMTPAPI'] = @smtp_headers.to_json
    end

    def unsubscribe_category(categories)
      # try to find unsubscribe group in the list of assigned categories
      # if not found - use fake "required" unsubscribe group, info ticket - #156958287
      category = categories.detect { |c| unsubscribe_groups[c] } || :required
      unsubscribe_groups[category]
    end

    def unsubscribe_preferences
      # find unsubscribe group ids for visible unsubscribe preferences
      I18n.t('emails.unsubscribe_preferences').map { |g| unsubscribe_groups[g] }.compact
    end

    def subject_opts(items)
      first_item = items[0]

      {
        qty: first_item.inventory_units.length,
        product_title: truncate(first_item.variant.product.name, length: 40),
        count: items.length - 1
      }
    end

    def user_first_name(user: nil, order: nil)
      name = user&.first_name.presence ||
             order&.user&.first_name.presence ||
             order&.billing_address&.first_name.presence

      name || Spree.t('there')
    end

    def unsubscribe_groups
      @unsubscribe_groups ||= YAML.load_file(Rails.root.join('config', 'data', 'unsubscribe-groups.yml'))
                                  .with_indifferent_access
    end
  end

  BaseMailer.prepend(BaseMailerDecorator)
end
