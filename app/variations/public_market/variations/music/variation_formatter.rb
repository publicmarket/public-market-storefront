module PublicMarket
  module Variations
    module Music
      class VariationFormatter < Variations::VariationFormatter
        protected

        def format_card
          format_default
        end

        def format_subtitle
          format_default
        end

        def format_default
          name = I18n.t(variation_value, scope: 'variations.presentations.music_format', default: variation_value&.titleize)

          if variation_value == 'vinyl'
            vinyl_speed = product.respond_to?(:property) ? product.property(:vinyl_speed) : product[:vinyl_speed]
            name += " (#{vinyl_speed.upcase})" if vinyl_speed
          end

          name
        end
      end
    end
  end
end
