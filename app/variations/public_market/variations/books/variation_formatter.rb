module PublicMarket
  module Variations
    module Books
      class VariationFormatter < Variations::VariationFormatter
        protected

        def format_default
          child = I18n.t(format_unique_pm, 'variations.presentations.book_format', default: format_unique_pm.titleize)

          titleized_variation = format_base.titleize
          # remove root format from unique PM format
          child = child.remove(titleized_variation).strip

          [titleized_variation, child].reject(&:blank?).uniq.join(': ')
        end

        def format_card
          I18n.t(format_unique_pm, scope: 'variations.presentations.book_format', default: format_unique_pm.titleize)
        end

        def format_subtitle
          format_default
        end

        def format_base
          return 'other' if (formats = product.variation_property_value).blank?

          child_format = find_matching_format(formats.split('; '), book_formats)

          find_matching_format([child_format], filterable_book_formats)
        end

        def format_unique_pm
          return 'other' if (formats = product.variation_property_value).blank?

          find_matching_format(formats.split('; '), book_formats) || 'other'
        end

        private

        def find_matching_format(initial_formats, formats)
          return 'other' if initial_formats.blank?

          matching_format =
            initial_formats.reject { |f| f.casecmp('other').zero? }.find do |format|
              mapped_format = find_book_format(formats, format)
              break mapped_format if mapped_format
            end

          matching_format || 'other'
        end

        def find_book_format(formats, format)
          return if format.blank?

          formats.find { |_k, v| v.include?(format.downcase) }&.first
        end

        def book_formats
          I18n.t('variations.formats.books').stringify_keys
        end

        def filterable_book_formats
          I18n.t('variations.filterable_variations.books').stringify_keys
        end
      end
    end
  end
end
