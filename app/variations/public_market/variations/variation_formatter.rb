module PublicMarket
  module Variations
    class VariationFormatter
      attr_reader :product

      def initialize(product)
        @product = product
      end

      def format(style = :default)
        send("format_#{style}")
      end

      protected

      # product page variations buttons
      def format_default
        I18n.t(product.variation_property_name, scope: 'variations.presentations', value: variation_value, default: variation_value)
      end

      # product cards
      def format_card; end

      # product page subtitle
      def format_subtitle; end

      # search index
      def format_base
        variation_value
      end

      def variation_value
        @variation_value ||= product.variation_property_value
      end
    end
  end
end
