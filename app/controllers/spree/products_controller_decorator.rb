module Spree
  module ProductsControllerDecorator
    def self.prepended(base)
      base.before_action :save_return_to, only: :show
      base.before_action :load_product, only: %i[show similar_variants]
      base.before_action :load_taxon_from_taxon_id, only: %i[show autocomplete]
      base.before_action :find_vendor, only: :show
      base.after_action :add_vary_header, only: %i[index best_selling]
      base.include FrontendHelper
      base.include SimilarVariantsHelper
    end

    def index
      @taxon = Spree::Taxon.find_by(id: params[:taxon])
      load_products

      render_taxon_or_products
    end

    def show
      @previous_variation = Spree::Product.find_by(slug: params[:variation])

      product_variants = @product.variants
      product_variants = product_variants.where(vendor: @vendor) if @vendor

      # use out of stock variant if none in stock
      @buy_box_variant =
        Spree::Variant.find_by_hash_id(params[:var]) || # rubocop:disable Rails/DynamicFindBy
        product_variants.find_best_price_in_option.first ||
        product_variants.find_best_price_in_option(in_stock: false).first

      @review = Spree::Review.find_or_initialize_by(user: spree_current_user, product: @product)

      redirect_if_legacy_path
    end

    def best_selling
      @hide_taxon_breadcrumbs = true

      params[:sort] ||= { popularity: 'all_time' }

      load_products

      if @taxon
        self.title = helpers.strip_tags(@taxon.promoted_title.presence || I18n.t(@taxon.name, scope: 'home.top_products_header'))
        @meta_description = @taxon.meta_description
      end

      render_taxon_or_products
    end

    def autocomplete
      opts = {
        limit: 10,
        keywords: params[:keywords]
      }
      opts[:taxon_ids] = taxon_opts

      @products = Spree::Inventory::Searchers::Autocomplete.call(opts).results

      respond_to do |format|
        format.json
      end
    end

    def similar_variants
      @variation = params[:variation]
      return redirect_to(@product) if @variation != @product.variation_formatter.format(:base)

      @variants = Spree::Inventory::FindProductVariations.call(@product, filter_by_variation: @variation, load_variants: true)
                                                         .first&.dig(:similar_variants)
    end

    private

    def render_taxon_or_products
      show_taxons = show_taxons?

      respond_to do |format|
        format.html { render show_taxons ? 'spree/taxons/show' : 'spree/products/index' }
        format.js { render 'spree/shared/search/products' }
      end
    end

    def load_products
      searcher_opts = {}
      searcher_opts[:taxon_ids] = taxon_opts

      @products = build_searcher(params, opts: searcher_opts).call
    end

    def load_taxon_from_taxon_id
      taxon_id = params[:taxon_id]
      @product_taxon = Spree::Taxon.find_by(id: taxon_id) if taxon_id.present?

      @taxon =
        if @product
          @product_taxon ||= @product.taxons.first
          @product_taxon&.root
        else
          @product_taxon
        end
    end

    def show_taxons?
      return false if @taxon.blank?

      @sidebar_taxons = helpers.child_taxons(@taxon)
      @card_size = :medium if @sidebar_taxons.present?

      # show taxon previews on mobile if no filter/sort applied
      !mobile? || show_taxon_previews?
    end

    def taxon_opts
      @taxon&.id
    end

    def find_vendor
      @vendor = Spree::Vendor.friendly.find(params[:merchant]) if params[:merchant]
    end
  end

  ProductsController.prepend(ProductsControllerDecorator)
end
