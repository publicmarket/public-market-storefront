module Spree
  module PagesControllerDecorator
    def how_it_works
      @meta_image = 'how-it-works'
    end
  end

  PagesController.prepend(PagesControllerDecorator)
end
