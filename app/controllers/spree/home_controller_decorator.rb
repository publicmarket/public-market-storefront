module Spree
  module HomeControllerDecorator
    def self.prepended(base)
      base.before_action :set_referral_metadata, only: :index, if: -> { !spree_user_signed_in? && params[:refuid].present? }
    end

    def index; end

    def sell
      @merchant = PublicMarket::Merchant.new
      @meta_image = 'merchants-apply'
    end

    def sell_apply_confirm
      flash[:success] = t('sell.success')
      redirect_to root_path
    end

    private

    def set_referral_metadata
      self.title = I18n.t('meta_titles.shared_referral')
      @meta_description = I18n.t('meta_descriptions.shared_referral')
    end
  end

  HomeController.prepend(HomeControllerDecorator)
end
