module Spree
  module ReviewsControllerDecorator
    # rubocop:disable Metrics/AbcSize
    def create
      params[:review][:rating].sub!(/\s*[^0-9]*\z/, '') if params[:review][:rating].present?

      @review = Spree::Review.new(review_params)
      @review.product = @product
      @review.user = spree_current_user if spree_user_signed_in?
      @review.ip_address = request.remote_ip
      @review.locale = I18n.locale.to_s if Spree::Reviews::Config[:track_locale]

      authorize!(:create, @review)
      if @review.save
        flash[:notice] = Spree.t(:review_successfully_submitted)
        redirect_to spree.product_path(@product)
      elsif @review.errors[:rating].blank?
        flash[:error] = @review.errors.full_messages.join(', ')
      end
    end

    def update
      if @review.update(review_params)
        flash[:notice] = Spree.t(:review_successfully_updated)
        redirect_to spree.product_path(@product)
      else
        flash[:error] = @review.errors.full_messages.join(', ') if @review.errors[:rating].blank?

        respond_to do |format|
          format.js { render 'spree/reviews/create' }
        end
      end
    end

    # rubocop:enable Metrics/AbcSize
  end

  ReviewsController.prepend(ReviewsControllerDecorator)
end
