module Spree
  module Api
    module V1
      class LeadsController < Spree::Api::BaseController
        def wh
          authorize!(:create, Spree::Lead)
          Leads::Push.call(params)
        end
      end
    end
  end
end
