Spree::VendorsController.class_eval do
  def show
    @products = build_searcher(params, opts: { filter: { vendors: [@vendor.id] }}).call
  end

  def accurate_title
    I18n.t('meta_titles.vendors/show', vendor_name: @vendor.name, store_name: default_title)
  end
end
