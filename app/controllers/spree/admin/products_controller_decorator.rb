module Spree
  module Admin
    module ProductsControllerDecorator
      protected

      def collection
        return @collection if @collection.present?

        return variant_collection if variant_collection.present?

        opts = {
          keywords: params[:name],
          per_page: params[:per_page] || Spree::Config[:admin_products_per_page],
          page: params[:page],
          includes: product_includes,
          sort: { name: :asc }
        }

        opts[:filter] = { vendors: spree_current_user.vendors.pluck(:id) } unless spree_current_user.admin?

        @collection = Spree::Inventory::Searchers::BaseSearcher.call(opts)
      end

      def product_includes
        [{ master: %i[images default_price] }]
      end

      def variant_collection
        return @variant_collection if @variant_collection

        variant_params = { sku: params[:name] }
        variant_params[:vendor_id] = spree_current_user.vendors.pluck(:id) unless spree_current_user.admin?

        @variant_collection = Spree::Product.includes(product_includes)
                                            .joins(:variants)
                                            .where(spree_variants: variant_params)
                                            .where.not(spree_variants: { vendor_id: nil })
                                            .page(1)
      end

      def location_after_save
        if current_spree_vendor.present? && @product.variants.count.zero?
          spree.new_admin_product_variant_url(@product)
        else
          spree.edit_admin_product_url(@product)
        end
      end
    end

    ProductsController.prepend(ProductsControllerDecorator)
  end
end
