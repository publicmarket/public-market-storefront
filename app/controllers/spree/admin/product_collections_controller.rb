module Spree
  module Admin
    class ProductCollectionsController < ResourceController
      custom_callback(:create).before(:build_image)
      custom_callback(:update).before(:build_image)

      def index
        respond_with(@collection)
      end

      def edit
        @product_collection_products =
          @object.product_collections_products
                 .includes(:product)
                 .page(params[:page])
                 .per(Spree::Config[:admin_properties_per_page])

        super
      end

      def destroy
        if @object.destroy
          flash[:success] = flash_message_for(@object, :successfully_removed)
          respond_with(@object) do |format|
            format.html { redirect_to location_after_destroy }
            format.js   { render_js_for_destroy }
          end
        else
          render plain: @object.errors.full_messages.join(', '), status: :unprocessable_entity
        end
      end

      private

      def build_image
        return if permitted_image_params[:image].blank?

        @object.build_image(attachment: permitted_image_params[:image])
      end

      def permitted_resource_params
        params.require(:product_collection).permit(:name, :slug, :influencer_slug, :description, :promoted, :taxonomy_id, :user_id)
      end

      def permitted_image_params
        params.require(:product_collection).permit(:image)
      end

      def find_resource
        ProductCollection.friendly.find(params[:id])
      end

      def collection
        return @collection if @collection.present?

        params[:q] = {} if params[:q].blank?

        @collection = super
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(Spree::Config[:admin_properties_per_page])
      end

      def location_after_save
        spree.edit_admin_product_collection_url(@object)
      end
    end
  end
end
