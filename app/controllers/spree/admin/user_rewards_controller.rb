module Spree
  module Admin
    class UserRewardsController < ResourceController
      def index
        if params[:button] == 'export'
          send_data export, filename: 'user-rewards.csv'
        else
          respond_with(@collection)
        end
      end

      private

      def export
        CSV.generate(headers: true) do |csv|
          csv << ['Date', 'Kind', 'User', 'Paid Amount', 'Reward Total Amount', 'Referees Amount']

          @search.result.find_each do |reward|
            csv << [
              reward.date,
              reward.kind,
              reward.user.email,
              reward.paid_amount,
              reward.amount,
              reward.referees_amount
            ]
          end
        end
      end

      def collection
        return @collection if @collection.present?

        params[:q] = {} if params[:q].blank?

        @collection = super.includes(:user).order(date: :desc)
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(Spree::Config[:admin_properties_per_page])
      end
    end
  end
end
