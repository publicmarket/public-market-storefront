Spree::Admin::OrdersController.class_eval do
  include BeforeRender
  before_render :show_vendor_info, only: %i[index]
  before_action :load_vendor_view, only: %i[edit cart show]

  def sales_report
    csv = Spree::Orders::SalesReportAction.call(current_spree_vendor, sales_date)
    send_data csv, filename: "sales-report-#{sales_date.strftime('%b-%Y')}.csv"
  end

  def reassign_item
    line_item = Spree::LineItem.find(params[:line_item_id])
    pm_vendor = Spree::Vendor.public_market
    Spree::Orders::ReassignLineItemAction.call(line_item, pm_vendor)
    redirect_to edit_admin_order_path(line_item.order)
  end

  def resend
    if @order.shipped?
      @order.shipments.shipped.each { |shipment| Spree::ShipmentMailer.shipped_email(shipment, true).deliver_later }
    else
      Spree::OrderMailer.confirm_email(@order.id, true).deliver_later
    end

    flash[:success] = Spree.t(:order_email_resent)

    redirect_back fallback_location: spree.edit_admin_order_url(@order)
  end

  private

  def sales_date
    @sales_date ||= Date.parse("1-#{params[:date][:month]}-#{params[:date][:year]}")
  end

  def load_vendor_view
    @order = vendor_view(@order) if current_spree_vendor
  end

  def show_vendor_info
    return unless current_spree_vendor

    @orders.each do |order|
      view = vendor_view(order)
      order.total = view.total
    end
  end

  def vendor_view(order)
    Spree::Orders::VendorView.new(order, current_spree_vendor)
  end
end
