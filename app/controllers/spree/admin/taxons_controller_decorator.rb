Spree::Admin::TaxonsController.class_eval do
  def update # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity, Metrics/MethodLength
    successful = @taxon.transaction do
      parent_id = params[:taxon][:parent_id]
      set_position
      set_parent(parent_id)

      @taxon.save!

      # regenerate permalink
      regenerate_permalink if parent_id

      set_permalink_params

      # check if we need to rename child taxons if parent name or permalink changes
      @update_children = true if params[:taxon][:name] != @taxon.name || params[:taxon][:permalink] != @taxon.permalink

      @taxon.create_icon(attachment: taxon_params[:icon]) if taxon_params[:icon]
      @taxon.update(taxon_params.except(:icon))
    end
    if successful
      flash[:success] = flash_message_for(@taxon, :successfully_updated)

      # rename child taxons
      rename_child_taxons if @update_children

      respond_with(@taxon) do |format|
        format.html { redirect_to edit_admin_taxonomy_taxon_path(@taxon.taxonomy_id, @taxon.id) }
        format.json { render json: @taxon.to_json }
      end
    else
      respond_with(@taxon) do |format|
        format.html { render :edit }
        format.json { render json: @taxon.errors.full_messages.to_sentence, status: :unprocessable_entity }
      end
    end
  end

  def reindex
    @taxon = Spree::Taxon.friendly.find(params[:id])
    authorize!(:update, @taxon)

    @taxon.self_and_descendants
          .find_each { |taxon| taxon.products.reorder(nil).find_each(&:reindex) }

    flash[:success] = 'Reindex process started'
    redirect_to edit_admin_taxonomy_taxon_path(@taxon.taxonomy_id, @taxon.id)
  end
end
