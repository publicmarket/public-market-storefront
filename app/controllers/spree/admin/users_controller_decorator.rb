module Spree
  module Admin
    module UsersControllerDecorator
      def create
        @user = Spree.user_class.new(user_params)
        build_avatar

        if @user.save
          flash[:success] = flash_message_for(@user, :successfully_created)
          redirect_to edit_admin_user_path(@user)
        else
          render :new
        end
      end

      def update # rubocop:disable Metrics/AbcSize
        if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
          params[:user].delete(:password)
          params[:user].delete(:password_confirmation)
        end

        build_avatar

        if @user.update(user_params)
          flash[:success] = Spree.t(:account_updated)
          redirect_to edit_admin_user_path(@user)
        else
          render :edit
        end
      end

      private

      def find_resource
        User.with_deleted.friendly.find(params[:id])
      end

      # added bio only for admins
      def user_params
        params.require(:user).permit(permitted_user_attributes |
                                     [:use_billing,
                                      :bio,
                                      :twitter_name,
                                      spree_role_ids: [],
                                      vendor_ids: [],
                                      ship_address_attributes: permitted_address_attributes,
                                      bill_address_attributes: permitted_address_attributes])
      end

      def build_avatar
        return if permitted_avatar_params[:avatar].blank?

        @user.build_avatar(attachment: permitted_avatar_params[:avatar])
      end

      def permitted_avatar_params
        params.require(:user).permit(:avatar)
      end
    end

    UsersController.prepend(UsersControllerDecorator)
  end
end
