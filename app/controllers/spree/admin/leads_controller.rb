module Spree
  module Admin
    class LeadsController < ResourceController
      def index
        respond_with(@collection)
      end

      def upload
        if params[:leads][:file].blank?
          flash[:error] = 'No file chosen'
          redirect_to new_admin_lead_path
        else
          Leads::Upload.call(params[:leads][:file], source: params[:leads][:source])
          flash[:success] = 'Successfully uploaded'
          redirect_to location_after_save
        end
      end

      def export
        send_data lead_csv, filename: "leads-#{Time.current.strftime('%d-%b-%Y')}.csv"
      end

      private

      def collection
        return @collection if @collection.present?

        params[:q] = {} if params[:q].blank?

        @collection = super
        @search = @collection.ransack(params[:q])
        @collection = @search.result.page(params[:page]).per(Spree::Config[:admin_properties_per_page])
      end

      def lead_csv
        CSV.generate(headers: true) do |csv|
          csv << ['Lead ID', 'Email', 'User ID', 'First Name', 'Last Name']

          Lead.find_each do |lead|
            csv << [lead.id, lead.email, lead.user_id, lead.user&.first_name, lead.user&.last_name]
          end
        end
      end

      def upload_params
        params.require(:leads).permit(:source, :file)
      end
    end
  end
end
