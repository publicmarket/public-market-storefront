module Spree
  module Admin
    module StockLocationsControllerDecorator
      def update
        invoke_callbacks(:update, :before)
        if @object.update(permitted_resource_params)
          invoke_callbacks(:update, :after)
          respond_with(@object) do |format|
            format.html do
              flash[:success] = Spree.t(:stock_location_updated)
              redirect_to location_after_save
            end
            format.js { render layout: false }
          end
        else
          invoke_callbacks(:update, :fails)
          respond_with(@object) do |format|
            format.html { render action: :edit }
            format.js { render layout: false }
          end
        end
      end

      def reindex
        StockLocationReindex.perform_async(params[:id])
        flash[:success] = 'Reindex process started'
        redirect_to admin_stock_locations_path
      end
    end

    StockLocationsController.prepend(StockLocationsControllerDecorator)
  end
end
