module Spree
  class ProductCollectionsController < Spree::StoreController
    include Spree::ProductsHelper
    helper_method :cache_key_for_product # not inclusion causes exception in product partial

    before_action :load_product_collection, only: [:show]
    before_action :set_influencer_cookie, only: [:show]

    after_action :add_vary_header, only: :show

    def show
      params[:filter] ||= {}
      params[:filter][:collections] = [@product_collection.id]

      @hide_taxon_breadcrumbs = true
      @taxon = @product_collection.taxonomy&.root
      @products = build_searcher(params).call

      self.title = @product_collection.name

      respond_to do |format|
        format.html { render 'spree/product_collections/show' }
        format.js { render 'spree/shared/search/products' }
      end
    end

    private

    def load_product_collection
      @product_collection = ProductCollection.find_by(slug: params[:id]) ||
                            ProductCollection.find_by(influencer_slug: params[:id])

      raise ActiveRecord::RecordNotFound, 'Record not found.' if @product_collection.blank?
    end

    def set_influencer_cookie
      return if @product_collection.influencer_slug != params[:id]

      cookies['_inf_src'] = @product_collection.influencer_slug if cookies['_inf_src'].blank?
      cookies['refuid'] = @product_collection&.user&.hash_id
    end
  end
end
