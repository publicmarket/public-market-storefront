Spree::UserRegistrationsController.class_eval do
  # POST /resource/sign_up
  # Storefront changes:
  # - save(context: :signup)
  def create # rubocop:disable Metrics/AbcSize
    @user = build_resource(spree_user_params)

    set_user_referrer

    resource_saved = resource.save(context: :signup)
    yield resource if block_given?

    if resource_saved
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up
        sign_up(resource_name, resource)
        session[:spree_user_signup] = true

        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end

      track_mixpanel(resource)
    else
      clean_up_passwords(resource)
      render :new
    end
  end

  protected

  def track_mixpanel(user)
    session[:mixpanel_actions] = [
      "mixpanel.alias('#{user.id}')",
      %(mixpanel.track('signup', {
        "user_id": "#{user.id}",
        "email": "#{user.email}",
      }))
    ]

    session[:mixpanel_actions] << "mixpanel.people.set_once('influencer_source', '#{cookies['_inf_src']}')" if cookies['_inf_src'].present?
  end

  def set_flash_message(key, kind, options = {})
    super(key, kind, options.merge(email: resource&.email))
  end

  def redirect_to_404
    redirect_to '/404'
  end

  def set_user_referrer
    return if (refuid = cookies[:refuid]).blank?

    referrer_user = Spree::User.find_by_hash_id(refuid) # rubocop:disable Rails/DynamicFindBy
    @user.referrer = referrer_user if referrer_user.present?
  end
end
