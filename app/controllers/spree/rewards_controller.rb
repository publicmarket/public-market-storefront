module Spree
  class RewardsController < Spree::StoreController
    before_action :authenticate_spree_user!

    before_action :load_user, only: %i[index]
    before_action :set_account_tab, only: %i[index]

    def index; end

    private

    def set_account_tab
      @account_tab = :rewards
    end

    def load_user
      @user = spree_current_user
    end
  end
end
