module Spree
  class LeadsController < Spree::BaseController
    def subscribe
      Leads::Push.call(params)

      head :ok
    end
  end
end
