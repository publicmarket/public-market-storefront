module PublicMarket
  class RewardsValue < BigDecimal
    include ActionView::Helpers::NumberHelper

    DIGITS = 10

    def initialize(initial, digits = DIGITS)
      super(initial, digits)
    end

    def *(other)
      self.class.new(super(other))
    end

    def +(other)
      self.class.new(super(other))
    end

    def to_s
      number_to_currency(super)
    end
  end
end
