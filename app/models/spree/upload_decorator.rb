module Spree
  module UploadDecorator
    def self.prepended(base)
      class << base
        prepend ClassMethods
      end
    end

    module ClassMethods
      def supported_formats
        %w[csv json csv_tab amz_csv]
      end

      def supported_product_types
        %w[generic books music]
      end
    end
  end

  Upload.prepend(UploadDecorator)
end
