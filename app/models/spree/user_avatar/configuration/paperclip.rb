module Spree
  class UserAvatar < Asset
    module Configuration
      module Paperclip
        extend ActiveSupport::Concern

        included do
          has_attached_file :attachment,
                            styles: { thumb: '200x200>' },
                            default_style: :thumb

          validates_attachment_content_type :attachment, content_type: %r{\Aimage\/.*\z}

          delegate :url, to: :attachment

          process_in_background :attachment
        end
      end
    end
  end
end
