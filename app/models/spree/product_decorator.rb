module Spree
  module ProductDecorator
    MISSING_TITLE = '[Missing title]'.freeze

    def self.included(base) # rubocop:disable Metrics/AbcSize
      class << base
        prepend ClassMethods
      end
      base.prepend InstanceMethods

      base.belongs_to :best_variant, class_name: 'Spree::Variant'
      base.has_many :product_collections_products, class_name: 'Spree::ProductCollectionProduct', dependent: :destroy
      base.has_many :product_collections, through: :product_collections_products, class_name: 'Spree::ProductCollection', source: :product_collection

      base.include Spree::Core::NumberGenerator.new(prefix: 'PM', letters: true, length: 13)
      base.include Spree::ProductKind

      base.before_validation :set_missing_title, if: -> { name.blank? }

      base.after_create_commit :notify_missing_title, if: -> { name == MISSING_TITLE }

      base.scope :in_stock, lambda {
        joins(variants: :stock_items).where('spree_stock_items.count_on_hand > ? OR spree_variants.track_inventory = ?', 0, false)
      }

      base.skip_callback :touch, :after, :touch_taxons if base._touch_callbacks.any?

      base.before_real_destroy :ensure_no_line_items
    end

    module InstanceMethods
      def should_generate_new_friendly_id?
        name_changed? || super
      end

      def update_best_variant
        # get minimum price of best condition variant
        best_variant = variants.in_stock
                               .joins(:prices)
                               .joins(%(
                                 LEFT JOIN "spree_option_value_variants" ON "spree_option_value_variants"."variant_id" = "spree_variants"."id"
                                 LEFT JOIN "spree_option_values" ON "spree_option_values"."id" = "spree_option_value_variants"."option_value_id"
                                 LEFT JOIN "spree_option_types" ON "spree_option_types"."id" = "spree_option_values"."option_type_id"
                                   and "spree_option_types"."name" = 'condition'))
                               .reorder('spree_option_values.position asc, spree_prices.amount asc')
                               .limit(1)
                               .first

        update(price: best_variant.price, best_variant: best_variant) if best_variant
      end

      # can be false
      def taxonomy
        return @taxonomy if defined?(@taxonomy)

        @taxonomy = taxons.first&.taxonomy
      end

      # fields merged to searchkick's search_data
      def index_data # rubocop:disable Metrics/AbcSize
        variant_vendors = variants.active_with_available_price
                                  .in_stock
                                  .reorder(nil)
                                  .distinct
                                  .pluck(:vendor_id)

        {
          conversions_month: orders.complete.where('completed_at > ?', 1.month.ago).count,
          slug: slug,
          variations: [variation_formatter.format(:base)],
          collections: product_collection_ids,
          in_stock: variant_vendors.count,
          vendors: variant_vendors.uniq,
          no_image: images.count.zero?,
          boost_factor: boost_factor.to_i + taxons_boost_factor
        }
      end

      def taxons_boost_factor
        taxon_and_ancestors.sum(&:boost_factor)
      end

      def should_index?
        name != MISSING_TITLE && orderable?
      end

      def rewards
        best_variant&.final_rewards || Spree::Config.rewards
      end

      def rewards_amount
        PublicMarket::RewardsCalculator.call(price, rewards)
      end

      def find_property(name)
        product_properties.joins(:property)
                          .find_by(spree_properties: { name: name })
      end

      private

      def set_missing_title
        self.name = MISSING_TITLE
      end

      def notify_missing_title
        PublicMarket::Workers::Slack::DataReconciliationWorker.perform_async(product_id: id, message_type: 'missing-title')
      rescue => e # rubocop:disable Style/RescueStandardError
        Rails.env.production? || Rails.env.staging? ? Raven.capture_exception(e) : raise(e)
      end

      def ensure_no_line_items
        return unless line_items.any?

        errors.add(:base, :cannot_destroy_if_attached_to_line_items)
        throw(:abort)
      end
    end

    module ClassMethods
      # enable searchkick callbacks in RecalculateVendorVariantPrice
      # when price is included in searchkick index
      def search_fields
        autocomplete_fields + %i[isbn]
      end

      def autocomplete_fields
        %i[name author artist manufacturer brand_name]
      end
    end
  end
end

Spree::Product.include(Spree::ProductDecorator)
