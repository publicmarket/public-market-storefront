module Spree
  module VendorDecorator
    PM_SLUG = 'public-market'.freeze

    module ClassMethods
      def public_market
        find_by(slug: PM_SLUG)
      end
    end

    def self.prepended(base)
      base.after_create :create_default_shipping_method
      base.alias_attribute :description, :note
      base.alias_attribute :image, :logo

      class << base
        prepend ClassMethods
      end
    end

    def public_market?
      slug == PM_SLUG
    end

    def final_rewards
      rewards || Spree::Config.rewards
    end

    def rep_email
      bank_account&.email
    end

    private

    def create_default_shipping_method
      category = ShippingCategory.find_or_create_by(name: 'Default')
      name = "seller_#{id}_free"

      shipping_methods.create(
        name: Spree.t(:free_shipping_name),
        display_on: 'both',
        admin_name: name,
        code: name,
        shipping_categories: [category],
        zones: Spree::Zone.all,
        calculator_type: Rails.application.config.spree.calculators.shipping_methods.first.name
      )
    end
  end

  Vendor.prepend(VendorDecorator)
end
