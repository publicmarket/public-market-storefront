require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class FlexiOrderRate < ShippingCalculator
      preference :first_item,      :decimal, default: 0.0
      preference :additional_item, :decimal, default: 0.0
      preference :max_items,       :integer, default: 0
      preference :currency,        :string,  default: -> { Spree::Config[:currency] }

      def self.description
        Spree.t(:shipping_flexible_order_rate)
      end

      def compute_package(package)
        quantity = package.contents.sum(&:quantity)

        if quantity.positive? && last_order_shipment?(package.contents.first.inventory_unit)
          flexi_rate_calculator.compute_from_quantity(quantity)
        else
          preferred_additional_item * quantity
        end
      end

      private

      def last_order_shipment?(unit)
        return true if unit.order.blank? # can be blank for fake packages

        unit.order.shipments
            .joins(:stock_location, shipping_methods: :calculator)
            .where.not(state: :canceled)
            .where(spree_calculators: { id: id })
            .where(spree_stock_locations: { vendor_id: unit.variant.vendor_id })
            .where('spree_shipments.id > ?', unit.shipment_id)
            .blank?
      end

      def flexi_rate_calculator
        ::Spree::Calculator::FlexiRate.new(
          preferred_additional_item: preferred_additional_item,
          preferred_first_item: preferred_first_item,
          preferred_max_items: preferred_max_items,
          preferred_currency: preferred_currency
        )
      end
    end
  end
end
