module Spree
  module TaxonDecorator
    UNCATEGORIZED_NAME = 'Uncategorized'.freeze

    def self.prepended(base)
      base.scope :visible, -> { where(hidden: false) }
      base.scope :hidden, -> { where(hidden: true) }
      base.scope :navigatable, -> { joins(:taxonomy).where.not(spree_taxonomies: { name: %w[Other Category] }) }
      base.scope :promoted, -> { where(promoted: true).order(:promoted_position) }

      class << base
        prepend ClassMethods
      end
    end

    module ClassMethods
      def hidden_taxon_ids(taxon_ids = nil)
        hidden_ids = Spree::Taxon.hidden.ids

        return hidden_ids if taxon_ids.blank?

        ancestors = taxon_ids.flat_map { |taxon_id| Spree::Taxon.find(taxon_id).self_and_ancestors.ids }
        hidden_ids - ancestors
      end
    end
  end

  Taxon.prepend(TaxonDecorator)
end
