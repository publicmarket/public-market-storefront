module Spree
  module ShippingMethodDecorator
    def free?
      calculator.preferences[:flat_percent]&.zero? || calculator.preferences[:amount]&.zero?
    end

    def include?(address)
      # HACK: for early shipments creation
      return true if address.blank?

      zones.includes(:zone_members).any? do |zone|
        zone.include?(address)
      end
    end
  end

  ShippingMethod.prepend(ShippingMethodDecorator)
end
