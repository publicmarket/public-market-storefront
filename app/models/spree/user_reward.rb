module Spree
  class UserReward < ApplicationRecord
    DAILY_KIND = :daily

    belongs_to :user, -> { with_deleted }, class_name: 'Spree::User', inverse_of: :user_rewards

    def referees_count
      user.referees.where('created_at <= ?', date).count
    end

    def self.total_pool_amount(date = Date.current)
      date < Date.new(2018, 12, 3) ? 5000 : 1500
    end

    def self.estimate(amount)
      prev_date = Time.zone.today.in_time_zone('Pacific Time (US & Canada)') - 1.day
      prev_paid = [prev_date_paid(prev_date), 1000].max

      (total_pool_amount * amount / (amount + prev_paid)).round(2)
    end

    def self.prev_date_paid(prev_date)
      Rails.cache.fetch([:v1, :reward_cache, prev_date.to_s]) do
        where(date: prev_date).sum(:paid_amount)
      end
    end
  end
end
