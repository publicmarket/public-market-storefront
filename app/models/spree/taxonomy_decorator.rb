module Spree
  module TaxonomyDecorator
    def self.prepended(base)
      class << base
        prepend ClassMethods
      end
    end

    module ClassMethods
      def other
        find_or_create_by!(name: 'Category')
      end
    end

    def variation_module
      "PublicMarket::Variations::#{parameterized_name.camelize}".constantize
    rescue NameError
      PublicMarket::Variations
    end

    def uncategorized_taxon
      root.children.find_or_create_by!(name: Taxon::UNCATEGORIZED_NAME, taxonomy: self)
    end

    def filterable_variations
      I18n.t(parameterized_name, scope: 'variations.filterable_variations', default: nil)&.stringify_keys&.keys || []
    end

    private

    def parameterized_name
      name.parameterize(separator: '_')
    end
  end

  Taxonomy.prepend(TaxonomyDecorator)
end
