Spree::LineItem.class_eval do
  scope :positive, -> { where(quantity: 1..Float::INFINITY) }

  money_methods :amount_by_status

  def rewards_amount
    single_rewards_amount * quantity
  end

  def final_rewards
    rewards || variant.final_rewards
  end

  def single_rewards_amount
    PublicMarket::RewardsCalculator.call(price, final_rewards)
  end

  def quantity_by_status
    quantity.zero? ? inventory_units.count : quantity
  end

  def rewards_amount_by_status
    single_rewards_amount * quantity_by_status
  end

  def amount_by_status
    price * quantity_by_status
  end
end
