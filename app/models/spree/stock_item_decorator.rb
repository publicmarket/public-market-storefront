Spree::StockItem.class_eval do
  after_commit :update_product_master_price, if: :saved_change_to_count_on_hand?

  def update_product_master_price
    return if variant.is_master
    return if count_on_hand_before_last_save.positive? && count_on_hand.positive?

    variant.product.update_best_variant
  end
end
