module Spree
  module ShippingCalculatorDecorator
    def compute_variant(variant, quantity = 1)
      package =
        Stock::Package.new(
          variant.vendor.stock_locations.first,
          [
            Stock::ContentItem.new(variant.inventory_units.new(quantity: quantity))
          ]
        )

      compute_package(package)
    end
  end

  ShippingCalculator.prepend(ShippingCalculatorDecorator)
end
