module Spree
  module OptionTypeDecorator
    def self.prepended(base)
      base.skip_callback :touch, :after, :touch_all_products if base._touch_callbacks.any?
    end
  end

  OptionType.prepend(OptionTypeDecorator)
end
