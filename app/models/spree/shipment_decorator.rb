Spree::Shipment.class_eval do
  def set_up_inventory(state, variant, order, line_item, quantity = 1)
    return if quantity <= 0

    quantity.times do
      inventory_units.create(
        state: state,
        variant_id: variant.id,
        order_id: order.id,
        line_item_id: line_item.id,
        quantity: 1
      )
    end
  end

  def partial?
    inventory_units.canceled.any?
  end

  def canceled_line_items
    @canceled_line_items ||= line_items.select { |i| i.quantity.zero? }
  end

  def vendor
    @vendor ||= stock_location.vendor
  end

  def after_cancel
    manifest.each { |item| manifest_restock(item) }
    order.update_with_updater!

    Spree::ShipmentMailer.cancel_email(id).deliver_later(wait: 10.seconds)
  end

  def tracking_url
    return if tracking.blank?

    @tracking_url ||= shipping_method&.build_tracking_url(tracking) || TrackingNumber.new(tracking).tracking_url
  end

  def rated?
    rating_uid.present?
  end

  def cost_rewards_amount
    return 0 if cost.blank? || rewards.blank?

    PublicMarket::RewardsCalculator.call(cost, rewards)
  end

  # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def determine_state(order)
    return 'canceled' if order.canceled? || inventory_units.all?(&:canceled?)
    return 'pending' unless order.can_ship?
    return 'pending' if inventory_units.any?(&:backordered?)
    return 'shipped' if shipped?

    order.paid? || Spree::Config[:auto_capture_on_dispatch] ? 'ready' : 'pending'
  end
  # rubocop:enable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

  def update_amounts
    return unless selected_shipping_rate

    # rubocop:disable Rails/SkipsModelValidations
    update_columns(
      cost: canceled? ? 0 : selected_shipping_rate.cost,
      adjustment_total: adjustments.additional.map(&:update!).compact.sum,
      rewards: rewards.presence || vendor.final_rewards,
      updated_at: Time.current
    )
    # rubocop:enable Rails/SkipsModelValidations
  end

  def to_package
    package = Spree::Stock::Package.new(stock_location)
    inventory_units.ordered.includes(:variant).joins(:variant).group_by(&:state).each do |state, state_inventory_units|
      package.add_multiple state_inventory_units, state.to_sym
    end
    package
  end
end
