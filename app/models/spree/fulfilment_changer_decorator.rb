Spree::FulfilmentChanger.class_eval do
  def initialize(params = {})
    @current_stock_location = params[:current_stock_location]
    @desired_stock_location = params[:desired_stock_location]
    @current_shipment       = params[:current_shipment]
    @desired_shipment       = params[:desired_shipment]
    @variant                = params[:variant]
    @quantity               = params[:quantity]
    @available_quantity     = [
      desired_stock_location.try(:count_on_hand, variant).to_i,
      @quantity
    ].max

    @desired_shipment.rewards = @current_shipment.rewards
  end
end
