Spree::ShipmentHandler.class_eval do
  def perform
    @shipment.inventory_units.select(&:ordered?).each(&:ship!)
    @shipment.touch(:shipped_at) if @shipment.shipped_at.blank? # rubocop:disable Rails/SkipsModelValidations
    update_order_shipment_state
    send_shipped_email
    send_rate_email
  end

  def send_rate_email
    wait_time = Rails.env.production? ? 14.days : 14.seconds
    Spree::ShipmentMailer.rate_email(@shipment.id).deliver_later(wait: wait_time)
  end
end
