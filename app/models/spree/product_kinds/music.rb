module Spree
  module ProductKinds
    class Music < ProductKinds::Base
      delegate :variation_property_value, to: :product

      def author_property_name
        'artist'
      end

      def additional_product_kind_name
        case child_kind
        when 'vinyl'
          :vinyl
        else
          super
        end
      end

      def child_kind
        @child_kind = variation_property_value&.downcase
      end

      def product_description
        case child_kind
        when 'vinyl'
          taxon_name = product.taxons.first.name
          opts = {
            genre: (taxon_name if taxon_name != Taxon::UNCATEGORIZED_NAME),
            record_format: variation_property_value&.titleize,
            rpm: property(:vinyl_speed)&.upcase,
            artist: subtitle_presentation,
            product_title: product.name,
            record_label: property(:music_label),
            catalog_number: property(:music_label_number)
          }

          I18n.t('products.description.vinyl', opts)
        else
          super
        end
      end

      def variation_property_name
        :music_format
      end

      def available_variations
        %w[vinyl cd]
      end
    end
  end
end
