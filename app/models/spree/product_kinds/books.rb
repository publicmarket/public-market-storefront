module Spree
  module ProductKinds
    class Books < ProductKinds::Base
      delegate :variation_formatter, to: :product

      EXCLUDED_VARIATION = 'digital'.freeze

      def image_aspect_ratio
        2 / 3.to_f
      end

      def additional_properties
        [:edition].map { |p| property(p) }.compact
      end

      def author_property_name
        'author'
      end

      def subtitle_property_name
        'book_subtitle'
      end

      def product_date
        @product_date ||= property(:published_at)
      end

      def variation_property_name
        :format
      end

      # ordered list
      def available_variations
        I18n.t('variations.formats.books').stringify_keys.keys
      end

      def orderable?
        variation_formatter.format(:unique_pm) != EXCLUDED_VARIATION
      end
    end
  end
end
