module Spree
  module ProductKinds
    class Base
      extend Dry::Initializer

      param :product

      delegate :property, :product_kind_name, to: :product

      def additional_product_kind_name
        product_kind_name
      end

      def subtitle
        @subtitle ||= property(author_property_name)
      end

      def subtitle_presentation
        subtitle&.titleize
      end

      def image_aspect_ratio
        1
      end

      def additional_properties
        []
      end

      def author_property_name
        'manufacturer'
      end

      def subtitle_property_name
        nil
      end

      def product_description
        product.description.to_s.gsub(/(.*?)\r?\n\r?\n/m, '<p>\1</p>')
      end

      def child_kind
        nil
      end

      def product_date
        nil
      end

      def products_by_subtitle
        return if subtitle.blank?

        Inventory::Searchers::ProductSearcher.call(
          keywords: subtitle,
          fields: [author_property_name],
          operator: :and,
          limit: 10,
          taxon_ids: product.taxonomy&.root&.id,
          filter: {
            id: { not: product.id },
            no_image: false
          }
        )
      end

      def variation_property_name
        property(:variation_property)
      end

      def available_variations
        nil
      end

      def orderable?
        true
      end
    end
  end
end
