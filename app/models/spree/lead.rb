module Spree
  class Lead < ApplicationRecord
    belongs_to :user, class_name: Spree.user_class.to_s, optional: true

    before_validation :normalize_email

    validates :email, presence: true

    # after_create :send_email

    private

    def send_email
      return if User.find_by(email: email).present?

      UserMailer.lead_whitelisted(id).deliver_later(wait: 1.minute)
    end

    def normalize_email
      email&.strip!
      email&.downcase!
    end
  end
end
