Spree::Image.class_eval do
  do_not_validate_attachment_file_type :attachment

  process_in_background :attachment

  _validators[:attachment].reject! { |validator| validator.is_a?(Paperclip::Validators::AttachmentContentTypeValidator) }
  _validate_callbacks.each do |callback|
    callback.raw_filter.attributes.delete(:attachment) if callback.raw_filter.is_a?(Paperclip::Validators::AttachmentContentTypeValidator)
  end

  validates_attachment_content_type :attachment, content_type: %r{\Aimage\/.*\z}

  def self.accepted_image_types
    %w[image/jpeg image/jpg image/png image/x-png image/gif]
  end

  def find_dimensions
    temporary = attachment.queued_for_write[:original]
    return if temporary.blank?

    filename = temporary.path unless temporary.nil?
    filename = attachment.path if filename.blank?
    geometry = Paperclip::Geometry.from_file(filename)
    self.attachment_width  = geometry.width
    self.attachment_height = geometry.height
  end

  attachment_definitions[:attachment][:path] = Paperclip::Attachment.default_options[:path]
end
