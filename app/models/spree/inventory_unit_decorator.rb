Spree::InventoryUnit.class_eval do
  include PublicMarket::Hashed

  scope :canceled, -> { where(state: 'canceled') }
  scope :ordered, -> { where.not(state: 'canceled') }

  unless state_machine.events[:cancel]
    state_machine do
      event :cancel do
        transition to: :canceled, from: %i[on_hand backordered]
      end
      after_transition to: :canceled, do: :after_cancel
      after_transition to: :shipped, do: :after_ship
    end
  end

  def ordered?
    on_hand? || backordered?
  end

  def allow_ship?
    ordered?
  end

  private

  def after_cancel
    line_item.decrement!(:quantity) # rubocop:disable Rails/SkipsModelValidations
    order.update_with_updater!

    cancel_shipment_if_needed
    ship_shipments_if_needed
  end

  def after_ship
    ship_shipments_if_needed
  end

  def ship_shipments_if_needed
    vendor_units = order.inventory_units.reload.includes(:variant).select do |unit|
      unit.variant.vendor_id == variant.vendor_id
    end

    return if vendor_units.any?(&:ordered?)

    vendor_units.map(&:shipment).uniq.select(&:ready?).each(&:ship!)
  end

  def cancel_shipment_if_needed
    return unless shipment.inventory_units.reload.all?(&:canceled?)

    shipment.cancel! unless shipment.canceled?
    cancel_order_if_needed
  end

  def cancel_order_if_needed
    return unless order.inventory_units.reload.all?(&:canceled?)

    order.cancel! if order.allow_cancel?
  end
end
