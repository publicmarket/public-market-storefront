module Spree
  module ProductKind
    extend ActiveSupport::Concern

    delegate :subtitle, :subtitle_presentation, :image_aspect_ratio, :additional_properties,
             :products_by_subtitle, :author_property_name, :product_description, :product_date,
             :additional_product_kind_name, :variation_property_name,
             :subtitle_property_name, :available_variations,
             to: :product_kind, allow_nil: true

    def product_kind_name
      @product_kind_name ||= taxonomy&.name&.downcase
    end

    def product_kind
      return @product_kind if @product_kind || product_kind_name.blank?

      kind_class = ProductKinds.const_get(product_kind_name.parameterize(separator: '_').camelize)
      @product_kind = kind_class.new(self)
    rescue NameError
      @product_kind = ProductKinds::Base.new(self)
    end

    def variation_property_value
      @variation_property_value ||= property(variation_property_name)
    end

    def variation_formatter
      @variation_formatter ||= variation_module.const_get('VariationFormatter').new(self)
    end

    def variation_module
      taxonomy ? taxonomy.variation_module : PublicMarket::Variations
    end

    def orderable?
      product_kind ? product_kind.orderable? : true
    end
  end
end
