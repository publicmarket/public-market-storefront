module Spree
  module TaxonsHelper
    def taxons_preview(taxon)
      Inventory::Searchers::SearchTaxonPreviews.call(taxon)
    end

    def child_taxons(taxon)
      Rails.cache.fetch([:v3, :child_taxons, taxon]) do
        Spree::Inventory::Searchers::TaxonSearcher.call(taxon)
      end
    end
  end
end
