module Spree
  module ProductsHelper
    ALLOWED_PRODUCT_DESCRIPTION_TAGS = %w[a abbr acronym address b blockquote br cite em hr i li ol p strike strong sub sup ul].freeze
    ALLOWED_PRODUCT_DESCRIPTION_ATTRS = %w[href].freeze

    def product_description(product)
      description = product.product_description

      return Spree.t(:product_has_no_description) if description.blank?

      sanitized_description = sanitize(description, tags: ALLOWED_PRODUCT_DESCRIPTION_TAGS, attributes: ALLOWED_PRODUCT_DESCRIPTION_ATTRS)

      raw(sanitized_description) # rubocop:disable Rails/OutputSafety
    end

    def product_subtitle(product, variation = nil, details: true)
      variation ||= product.variation_formatter.format(:subtitle)
      product_date = Date.parse(product.product_date)&.strftime('%b %-d, %Y') if details && product.product_date
      [variation, product_date].compact.join(' — ')
    end

    def additional_product_note(product, variant = nil)
      variant ||= product.variants.find_by(is_master: false)
      t(product.additional_product_kind_name, scope: 'products.additional_note',
                                              option_value: variant.main_option_value,
                                              note: variant.notes)
    end

    def cache_key_for_product(product = @product, opts = {})
      ([:v30, product.cache_key, product.rewards] + common_product_cache_keys + opts.to_a).compact.join('/')
    end

    def product_variants(product = @product, variant = @buy_box_variant)
      limit =
        case device
        when :mobile
          1
        when :tablet
          2
        end

      product.variants
             .limit(limit)
             .spree_base_scopes
             .where.not(vendor_id: variant&.vendor_id)
             .in_stock
             .active(current_currency)
             .includes(:option_values, :prices, vendor: { shipping_methods: :calculator })
             .reorder('spree_option_values.position ASC, spree_prices.amount ASC')
             .group_by(&:main_option_value)
    end

    def product_thumb_limit
      @product_thumb_limit ||=
        case device
        when :mobile
          3
        when :tablet
          4
        else
          5
        end
    end

    def property_value_format(property_name, value)
      case property_name
      when 'book_subject'
        value.split('; ', 2).first&.titleize
      when 'author'
        value
      when 'music_format'
        t(value, scope: 'variations.presentations.music_format', default: value.titleize)
      else
        value
      end
    end

    def product_variations
      return if @product.variation_module.blank?

      variations = Spree::Inventory::FindProductVariations.call(@product, @previous_variation)

      if (available_variations = @product.available_variations).present?
        # always use same variations order
        available_variations.map do |variation_name|
          variations.find { |var| var[:variation_name] == variation_name }
        end.compact
      else
        variations
      end
    end

    def product_kind_views_exists?(product, view)
      product.additional_product_kind_name &&
        lookup_context.exists?(product.additional_product_kind_name, ["spree/products/#{view}"], true, [], formats: [:html])
    end

    def fake_package_price(ship_calculator, variant)
      Spree::Money.new(ship_calculator.compute(variant).to_f)
    end
  end
end
