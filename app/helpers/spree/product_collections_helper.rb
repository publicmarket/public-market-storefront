module Spree
  module ProductCollectionsHelper
    ALLOWED_PROD_COLLECTION_DESCRIPTION_TAGS = %w[abbr acronym address b br cite em hr i li ol p span strike strong sub sup ul].freeze
    ALLOWED_PROD_COLLECTION_DESCRIPTION_ATTRS = %w[class].freeze

    def bio_length
      case device
      when :mobile
        130
      when :tablet
        300
      else
        900
      end
    end

    def product_collection_description(description)
      sanitize(description, tags: ALLOWED_PROD_COLLECTION_DESCRIPTION_TAGS, attributes: ALLOWED_PROD_COLLECTION_DESCRIPTION_ATTRS)
    end
  end
end
