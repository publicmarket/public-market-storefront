module Spree
  module BaseHelper
    def meta_data_tags
      meta = meta_data.map do |name, content|
        tag('meta', name: name, content: content)
      end

      meta += meta_properties.map do |name, content|
        tag('meta', property: name, content: content)
      end

      meta.join("\n")
    end

    def quantity_left(variant)
      return 0 if variant.blank?

      current_order ? current_order.quantity_left(variant) : variant.total_on_hand
    end

    def saved_as_default?(object)
      object.saved_changes
            .keys
            .reject { |key| %w[updated_at].include?(key) }
            .all? { |key| key == 'default' }
    end

    def stock_status(requested, available)
      if available.zero?
        :out_of_stock
      elsif available < requested
        :partial
      else
        :available
      end
    end

    def wrap_spans(collection)
      collection.map { |c| [content_tag(:span, c), c] }
    end

    def page_title
      I18n.t(controller_action_name, scope: 'meta_titles', default: title)
    end

    private

    def meta_image
      return if @page&.meta_image

      return image_url("sharing/#{@meta_image}.png") if @meta_image

      image = @product&.images&.first || controller_object_image

      if image
        image_url(image.attachment.url(:large))
      else
        image_url('logo/logo-facebook.png')
      end
    end

    def meta_data # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
      meta = {
        description: @meta_description
      }

      if controller_object.is_a?(ApplicationRecord)
        meta[:keywords] = controller_object.meta_keywords if controller_object[:meta_keywords].present?

        if meta[:description].blank? && controller_object[:meta_description].present?
          meta[:description] = strip_tags(controller_object.meta_description)
        end
      end

      if meta[:description].blank? && controller_object.respond_to?(:description)
        meta[:description] = truncate(strip_tags(controller_object.description), length: 160, separator: ' ')
      end

      meta[:keywords] = current_store.meta_keywords if meta[:keywords].blank?

      if meta[:description].blank?
        meta[:description] = I18n.t(controller_action_name, scope: 'meta_descriptions',
                                                            default: current_store.meta_description)
      end

      meta
    end

    def meta_properties
      opts = {
        'fb:app_id': Rails.application.credentials.facebook_api_key,
        'og:type': @product ? :product : :website
      }

      if (image = meta_image)
        opts['og:image'] = image
      end

      # avoid duplicate
      object_class = controller_object.class.to_s
      objects_with_own_tags = %w[Spree::Page]

      return opts if objects_with_own_tags.include?(object_class)

      opts.merge!(
        'og:title': page_title,
        'og:description': strip_tags(meta_data[:description])
      )
    end

    def controller_object
      @controller_object ||= instance_variable_get('@' + controller_name.singularize)
    end

    def controller_object_image
      controller_object.image if controller_object&.respond_to?(:image)
    end

    def controller_action_name
      [controller_name.singularize, action_name].join('/')
    end
  end
end
