Spree::Admin::BaseHelper.class_eval do
  def rewards_options(selected)
    options_for_select((10..60).map { |p| ["#{p / 2.0}%", p / 2.0] }, selected)
  end

  def subsidized_rewards_options(selected)
    options_for_select((0..30).map { |p| ["#{p / 2.0}%", p / 2.0] }, selected)
  end
end
