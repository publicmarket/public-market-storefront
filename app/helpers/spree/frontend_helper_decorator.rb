module Spree
  module FrontendHelper
    def body_class
      [@body_class, ('with-announcement' if spree_user_signed_in?)].compact.join(' ')
    end

    def compared_user_addresses(address)
      spree_current_user.addresses.map { |a| [a, address.same_as?(a)] }
    end

    def link_to_cart(text = nil)
      text = text ? h(text) : Spree.t('cart')
      css_class = nil

      if simple_current_order.nil? || (orders_count = simple_current_order.item_count).zero?
        text = image_tag('icons/cart.svg', alt: 'Cart')
        css_class = 'empty'
      else
        text = "#{image_tag('icons/cart.svg', alt: 'Cart')}<span class='badge'>#{orders_count}</span>"
        css_class = 'full'
      end

      link_to text.html_safe, spree.cart_path, class: "cart-info #{css_class}" # rubocop:disable Rails/OutputSafety
    end

    def inline_svg(path)
      source = Rails.application.assets_manifest.find_sources(path + '.svg').first
      raw(source) # rubocop:disable Rails/OutputSafety
    end

    def vendor_reputation_text(vendor, format: nil)
      reputation = cached_reputation(vendor)

      if reputation.present? && reputation.score.present?
        rating = number_to_percentage(reputation.score * 100, precision: 0)
        rating += ' Positive Ratings' if format == :text
        rating
      else
        Spree.t('not_enough_ratings')
      end
    end

    def rating_html(shipment)
      rating = cached_rating(shipment)

      rating.present? ? t("orders.rating#{rating.display_value}") : t('orders.rating-none')
    end

    def cached_reputation(vendor)
      uid = vendor&.reputation_uid
      return unless uid

      Rails.cache.fetch(GlobalReputation::Api::Reputation.cache_key(uid), expires_in: 1.hour) do
        GlobalReputation::Api::Reputation.find(uid).first
      end
    end

    def cached_rating(shipment)
      return unless shipment.rating_uid

      Rails.cache.fetch(shipment.rating_uid) do
        GlobalReputation::Api::Rating.find(shipment.rating_uid).first
      end
    end

    def can_rate?(shipment)
      !shipment.ready_or_pending? && cached_rating(shipment)&.modification.blank?
    end

    def product_image_or_default(product, style)
      return if product.blank?

      if (image = product.images.first).present?
        image.attachment.url(style)
      else
        image_path("noimage/#{style}.png")
      end
    end

    def product_card_size(size = nil)
      size == :medium ? 'col-lg-3 col-md-3 col-sm-4 col-xs-6' : 'col-lg-2 col-md-3 col-sm-4 col-xs-6'
    end

    # rubocop:disable Rails/OutputSafety
    def show_more(text, length: 200, link: 'Read more', omission: '...', arrow: false)
      return text.html_safe if text.length < length

      content = [content_tag(:span, text[0...length].html_safe)]
      content << content_tag(:span, omission)
      content << content_tag(:span, text[length..text.length].html_safe, class: 'hide')
      content << content_tag(:a, link, class: "show-more #{'arrow' if arrow}")

      safe_join(content)
    end
    # rubocop:enable Rails/OutputSafety

    def mobile?
      browser.device.mobile?
    end

    def mobile_or_tablet?
      @mobile_or_tablet ||= %i[mobile tablet].any? { |d| d == device }
    end

    def device
      @device ||=
        if mobile?
          :mobile
        elsif browser.device.tablet?
          :tablet
        else
          :desktop
        end
    end

    def show_taxon_previews?
      @show_taxon_previews ||= mobile? && @taxon.root? && %w[keywords filter sort].all? { |par| params[par].blank? }
    end

    def card_variation(product)
      product.variation_formatter.format(:card)
    end

    def show_cookies_bar?
      !spree_user_signed_in? && cookies['cookies-accepted'].blank?
    end

    def shipping_method_title(ship_method, price: 0, show_method_name: true, with_eta: true)
      eta = t('products.ship_eta', from: ship_method.eta_from, to: ship_method.eta_to) if with_eta

      if ship_method.free?
        [ship_method.name, eta].join(' ')
      else
        method_name = show_method_name ? ship_method.name : 'shipping'
        ['+', "<span class='ship-cost-value'>#{price}</span>", method_name, eta].join(' ')
      end
    end

    def user_rewards_cache_key
      return unless spree_current_user

      [spree_current_user, spree_current_user.orders_cache_key, spree_current_user.rewards_cache_key]
    end

    def variant_option_qty(variant, qty, order: false)
      option = variant.main_option_value
      option = ['Quantity', order ? 'ordered' : 'total'].join(' ') if option == 'Default'
      "(#{qty}) #{option}"
    end

    def user_referral_link
      spree.root_url(refuid: spree_current_user.referral_link_uid)
    end

    def estimate_daily_rewards(amount)
      spree_user_signed_in? ? spree_current_user.estimate_daily_rewards(amount) : Spree::UserReward.estimate(amount)
    end
  end
end
