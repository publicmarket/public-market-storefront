module Spree
  module HomepageHelper
    def home_top_collections_cache_key
      [:v11, :home, :top_collections, (ProductCollection.maximum(:updated_at) || Time.zone.today).to_s(:number)]
    end

    def home_top_selling_cache_key
      [:v2, :home, :top_selling, Taxon.promoted.maximum(:updated_at), spree_user_signed_in?]
    end

    def fetch_home_top_selling_products(taxon = nil)
      return [] if taxon.blank? || taxon.hidden?

      searcher_opts = {
        sort: {
          popularity: 'all_time'
        },
        limit: 6,
        taxon_ids: {
          in: [taxon&.id],
          not: Spree::Taxon.hidden.ids
        }
      }

      Inventory::Searchers::ProductSearcher.call(searcher_opts).results
    end

    def fetch_staff_picks
      return if (staff_picks_collection = ProductCollection.staff_picks).blank?

      staff_picks_opts = {
        limit: 3,
        sort: { popularity: 'all_time' },
        filter: { collections: [staff_picks_collection.id] }
      }

      Inventory::Searchers::ProductSearcher.call(staff_picks_opts)
    end

    def fetch_product_collections
      ProductCollection.where.not(name: ProductCollection::STAFF_PICKS_NAME)
                       .joins(:products)
                       .includes(:image)
                       .promoted
                       .order(Arel.sql('random()'))
                       .uniq
                       .take(4)
    end
  end
end
