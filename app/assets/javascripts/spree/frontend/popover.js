// allow to hover over popover
// trigger should be set to `manual`

$(document).on(window.isMobile() ? 'click' : 'mouseenter', '.hover-popover', function () {
  var _this = this;
  $(this).popover('show')
  $('.popover').on('mouseleave', function () {
    $(_this).popover('hide')
  })
}).on('mouseleave', '.hover-popover', function () {
  if (!window.isMobile() && !$('.popover:hover').length)
    $(this).popover('hide')
})

if (window.isMobile()) {
  $(document).on('click touchstart', 'body', function(e) {
    if (!$(e.target).hasClass('hover-popover') && !$(e.target).parents().is('.popover.in'))
      $('.hover-popover').popover('hide')
  })
}

$(document).on('turbolinks:load', function() {
  $('.daily-rewards-hint-trigger').popover({
    html: true,
    container: 'body',
    title: 'DAILY HOLIDAY <span class="hidden-xs">BONUS</span> REWARDS',
    content: "For a limited time, Public Market will be granting $5,000 a day in Holiday Bonus Rewards ($100k total) to early adopters for purchasing and referring their friends to purchase. <strong>The amount shown here is an estimate only based on our anticipated sales volume.</strong> <a href='/how-it-works#holiday-bonus-rewards'>Learn more</a>",
    placement: function() {
      return isMobile() ? 'bottom' : (this.$element.data('placement') || 'right')
    },
    trigger: 'manual',
    template: '<div class="popover popover--condensed popover--info" role="tooltip"><div class="arrow"></div><h3 class="popover-title info-icon-left condensed outline-info"></h3><div class="popover-content"></div></div>'
  });
})
