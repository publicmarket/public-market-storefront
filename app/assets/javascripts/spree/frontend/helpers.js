
window.pm = window.pm || {}

// swap with lodash
window.pm.debounce = function(callback, delay) {
  var timeout
  return function() {
    var args = arguments
    clearTimeout(timeout)
    timeout = setTimeout(function() {
      callback.apply(this, args)
    }.bind(this), delay)
  }
}

window.pm.setCookies = function(name, value, expiresInDays, cookiePath) {
  var path = cookiePath || '/'

  var date = new Date()
  expiresInDays = expiresInDays || 7300
  date.setTime(date.getTime() + expiresInDays * 24 * 60 * 60 * 1000)

  document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + 'path=' + path
}

// https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
window.pm.copyTextToClipboard = function(toCopy) {
  // if copying string - create new textarea, input, select and copy
  if (typeof toCopy === 'string') {
    var el = document.createElement('textarea')
    el.value = str
    el.setAttribute('readonly', '')
    el.style = { position: 'absolute', left: '-9999px' }
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
  // if copying from input - select and copy
  } else if (toCopy instanceof jQuery) {
    toCopy.select()
    document.execCommand('copy')
    document.getSelection().removeAllRanges()
  }
}

window.pm.mixpanelTrack = function(evtName, props) {
  if (typeof mixpanel !== 'undefined')
    mixpanel.track(evtName, props)
}
