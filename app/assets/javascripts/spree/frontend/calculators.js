Spree.flexiCalculator = function(shipCosts) {
  return function(quantity) {
    return shipCosts['first_item'] + shipCosts['additional_item'] * (quantity - 1)
  }
}

Spree.flatRateCalculator = function(shipCosts) {
  return function(quantity) {
    return shipCosts['amount'] * quantity
  }
}
