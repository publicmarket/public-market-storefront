//= require jquery.rating

$(document).on('turbolinks:load', function () {
  $('input[type=radio].star').rating({ required: true, callback: function() { $('#product-rating-error').hide() } });
});
