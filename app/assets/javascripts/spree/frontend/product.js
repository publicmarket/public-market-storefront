function initializeProductPage(calculatorType, shipCosts) {
  if ($.fancybox) {
    $.fancybox.defaults.hash = false;
    $.fancybox.defaults.buttons = ['zoom', 'close'];
  }

  Spree.buyBoxCalculator = (function() {
    switch(calculatorType) {
      case "Spree/Shipping/Flexi Rate":
      case "Spree/Shipping/Flexi Order Rate":
        return Spree.flexiCalculator(shipCosts)
      case "Spree/Shipping/Flat Rate":
      case "Spree/Shipping/Per Item":
        return Spree.flatRateCalculator(shipCosts)
    }
  })()

  Spree.changeQuantitySelectorOptions = function(count) {
    var $el = $('#buy-box #quantity');
    $el.empty(); // remove old options

    var max = count > 50 ? 50 : count;
    for (var i = 1; i <= max; i++) {
      $el.append($("<option></option>")
        .attr("value", i).text(i));
    }

    var lowStock = max < 10
    $('.buy-box--quantity-left--value').text(max)
    $('.buy-box--quantity-left').toggleClass('hide', !lowStock)
  }

  $(document).on('change', '#buy-box #quantity', function() {
    if (Spree.buyBoxCalculator == undefined) return
    $('.buy-box .ship-cost-value').text('$' + Spree.buyBoxCalculator($(this).val()).toFixed(2))
  })

  // thumbnail selector
  $(document).on('mouseenter', '#product-thumbnails li', function (event) {
    if ($(event.currentTarget).hasClass('thumb-selected')) return

    var img = $(event.currentTarget).find('a')
    var imgLink = img.attr('href')

    $('#product-thumbnails li.thumb-selected').removeClass('thumb-selected')
    $(event.currentTarget).addClass('thumb-selected')

    $('#main-image img').off('onreadystatechange load')

    $('#main-image a').attr('data-fancybox-index', img.data('fancybox-index'))
    $('#main-image img').fadeTo(200, 0.15, function() {
      $(this).attr('src', imgLink).one('onreadystatechange load', function(){
        if (this.complete) $(this).fadeTo(150, 1);
      })
    })
  })

  // TODO: implement generic scroller
  var similarItemsScrolling = false
  var similarItemsScroller = '.product-similar-items--scroller'
  var similarItemsControls = '.product-similar-items .left, .product-similar-items .right'

  if ($(similarItemsScroller).length == 0) return

  if ($(similarItemsScroller)[0].offsetWidth < $(similarItemsScroller)[0].scrollWidth) {
    $(similarItemsControls).fadeIn({ start: function() {
      $(this).css({ 'display': 'flex' })
    }})
  }

  $(document).on('mousedown', similarItemsControls, function (evt) {
    similarItemsScrolling = true
    startScrolling($(similarItemsScroller), $(similarItemsScroller).width() / 6, evt.target)
  }).on('mouseup', function () {
    similarItemsScrolling = false
  })

  function startScrolling(obj, spd, btn) {
    var step = $(btn).hasClass('left') ? '-=' + spd + 'px' : '+=' + spd + 'px'

    if (!similarItemsScrolling) {
      obj.stop()
    } else {
      obj.animate({
        'scrollLeft': step
      }, 'fast', function () {
        if (similarItemsScrolling) startScrolling(obj, spd, btn)
      })
    }
  }
}
