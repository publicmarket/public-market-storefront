//= require_tree .

$(document).on('click', 'a.show-more', function() {
  if ($(this).prev().hasClass('hide')) {
    $(this).text('Show less')
  } else {
    $(this).text('Read more')
  }

  $(this).prev().toggleClass('hide').prev().toggleClass('hide')
})

$(document).on('ajax:beforeSend', '.trigger-content-load-overlay', function() {
  $('.site-content-wrap').addClass('content-loading')
}).on('ajax:send', '.trigger-content-load-overlay', function(event, jqXHR) {
  jqXHR.always = function() {
    $('.site-content-wrap').removeClass('content-loading')
  }
})

$(document).on('turbolinks:load', function() {
  window.lozadObserver = window.lozad('.lozad', {
    loaded: function(el) {
      el.classList.add('opacity-1');
    }
  })
  window.lozadObserver.observe()
})
