window.pm = window.pm || {}

window.pm.initStickyAlert = function() {
  $('.alert-top-sticky').affix({
    offset: {
      top: function () {
        return $('#spree-header').outerHeight(true) + $('.announcement-bar').outerHeight(true)
      }
    }
  })
}

$(document).on('close.bs.alert', '#cookies-alert', function () {
  window.pm.setCookies('cookies-accepted', true)
})

window.pm.autoCloseAlerts = function(delay) {
  $('.alert-autoclose').each(function() {
    var totalDelay = $(this).data('close-delay')
    if (delay) totalDelay += delay
    $(this).delay(totalDelay).slideUp('fast')
  })
}

$(document).on('turbolinks:load', function() {
  window.pm.autoCloseAlerts()
})
