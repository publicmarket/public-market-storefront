// https://github.com/twbs/bootstrap-sass/blob/master/assets/javascripts/bootstrap-sprockets.js

//= require bootstrap/transition
//= require bootstrap/alert
//= require bootstrap/button

// require bootstrap/carousel

//= require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/modal
//= require bootstrap/tab
//= require bootstrap/affix

// require bootstrap/scrollspy

//= require bootstrap/tooltip
//= require bootstrap/popover

//= require jquery.payment
//= require spree
//= require spree/frontend/cart
//= require spree/frontend/checkout
//= require spree/frontend/checkout/address
//= require spree/frontend/checkout/payment
//= require spree/frontend/product
