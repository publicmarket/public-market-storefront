Deface::Override.new(
  virtual_path: 'spree/admin/users/_form',
  name: 'Add additional user fields',
  insert_bottom: 'div[data-hook="admin_user_form_roles"]',
  text:
    <<-HTML
      <%= f.field_container :first_name, class: ['form-group'] do %>
        <%= f.label :first_name %>
        <%= f.text_field :first_name, class: 'form-control' %>
        <%= f.error_message_on :first_name %>
      <% end %>

      <%= f.field_container :last_name, class: ['form-group'] do %>
        <%= f.label :last_name %>
        <%= f.text_field :last_name, class: 'form-control' %>
        <%= f.error_message_on :last_name %>
      <% end %>

      <%= f.field_container :login, class: ['form-group'] do %>
        <%= f.label :login %>
        <%= f.text_field :login, class: 'form-control' %>
        <%= f.error_message_on :login %>
      <% end %>

      <%= f.field_container :twitter_name, class: ['form-group'] do %>
        <%= f.label :twitter_name %>
        <%= f.text_field :twitter_name, class: 'form-control' %>
        <%= f.error_message_on :twitter_name %>
      <% end %>

      <%= f.field_container :bio, class: ['form-group'] do %>
        <%= f.label :bio %>
        <%= f.text_area :bio, class: 'form-control' %>
        <%= f.error_message_on :bio %>
      <% end %>

      <%= f.field_container :avatar, class: ['form-group'] do %>
        <%= f.label :avatar, Spree.t(:avatar) %>
        <%= f.file_field :avatar %>

        <% if f.object.avatar %>
          <%= image_tag f.object.avatar.url(:thumb) %>
        <% end %>
      <% end %>
    HTML
)
