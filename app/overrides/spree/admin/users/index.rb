Deface::Override.new(
  virtual_path: 'spree/admin/users/index',
  name: 'Add additional user field columns',
  insert_before: '[data-hook="admin_users_index_header_actions"]',
  text: %(
    <th>Referees</th>
    <th>Rewards</th>
  )
)

Deface::Override.new(
  virtual_path: 'spree/admin/users/index',
  name: 'Add additional user fields',
  insert_after: '[data-hook="admin_users_index_rows"] .user_email',
  text: %(
    <td><%= user.referees.count %></td>
    <td><%= user.rewards %></td>
  )
)
