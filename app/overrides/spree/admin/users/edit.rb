Deface::Override.new(
  virtual_path: 'spree/admin/users/edit',
  name: 'Add referral program to user edit in dashboard',
  insert_after: '[data-hook="admin_user_edit_general_settings"]',
  partial: 'spree/admin/users/referrals'
)
