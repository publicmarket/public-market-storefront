Deface::Override.new(
  virtual_path: 'spree/layouts/admin',
  name: 'Add correct title and favicon',
  insert_before: 'head',
  text: <<-HTML
          <% content_for :head do %>
            <%= favicon_link_tag 'favicon-dashboard.png' %>
          <% end %>
          <% content_for :title do %>
            <%= "Dashboard – " %>
            <%= Spree.t(controller.controller_name, default: controller.controller_name.titleize) %>
          <% end %>
  HTML
)

Deface::Override.new(
  virtual_path: 'spree/layouts/admin',
  name: 'Add leads and rewards icon',
  insert_bottom: '#main-sidebar',
  text: <<-HTML
                <% if current_spree_user.respond_to?(:has_spree_role?) && current_spree_user.has_spree_role?(:admin) %>
                  <ul class="nav nav-sidebar">
                    <%= tab plural_resource_name(Spree::Lead), url: admin_leads_path, icon: 'list-alt' %>
                  </ul>
                  <ul class="nav nav-sidebar">
                    <%= tab plural_resource_name(Spree::UserReward), url: admin_user_rewards_path, icon: 'usd' %>
                  </ul>
                <% end %>
  HTML
)
