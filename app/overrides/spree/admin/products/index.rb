Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'Use light image finder in dashboard products section',
  replace: '[data-hook="admin_products_index_rows"] td.image',
  text:
    <<-HTML
      <td class='image'><%= image_tag(product_image_or_default(product, :mini)) %></td>
    HTML
)

Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'Changes product table filters',
  replace: '[data-hook="admin_products_sidebar"]',
  partial: 'spree/admin/products/table_filters'
)

Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'Changes product table header',
  replace: '[data-hook="admin_products_index_headers"]',
  partial: 'spree/admin/products/table_head'
)
