Deface::Override.new(
  virtual_path: 'spree/admin/shipping_methods/_form',
  name: 'Add ETA in product form',
  insert_after: 'div[data-hook="admin_shipping_method_form_display_field"]',
  text: <<-HTML
          <div class="col-xs-6 col-md-2">
            <%= f.field_container :eta_from, class: ['form-group'] do %>
              <%= f.label :eta_from %>
              <%= f.number_field :eta_from, class: 'form-control' %>
            <% end %>
          </div>
          <div class="col-xs-6 col-md-2">
            <%= f.field_container :eta_to, class: ['form-group'] do %>
              <%= f.label :eta_to %>
              <%= f.number_field :eta_to, class: 'form-control' %>
            <% end %>
          </div>
  HTML
)