Deface::Override.new(
  virtual_path: 'spree/admin/stock_locations/index',
  name: 'Add Stock reindex button',
  insert_top: "[data-hook='stock_location_row'] .actions",
  text: <<-HTML
          <%= link_to_with_icon('cog', 'Reindex', spree.reindex_admin_stock_location_path(stock_location), class: 'btn btn-primary btn-sm', no_text: true, method: :post) if spree_current_user.admin? %>
  HTML
)
