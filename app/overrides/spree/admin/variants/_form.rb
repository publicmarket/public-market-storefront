Deface::Override.new(
  virtual_path: 'spree/admin/variants/_form',
  name: 'Remove price from variant forms',
  remove: '[data-hook="price"]'
)
