Deface::Override.new(
  virtual_path: 'spree/admin/taxons/_form',
  name: 'Show full path',
  replace_contents: '#permalink_part_display',
  text: %{
    <%= spree.root_url %>t/<%= @taxon.permalink.split('/')[0...-1].join('/') + '/' %>
  }
)

Deface::Override.new(
  virtual_path: 'spree/admin/taxons/_form',
  name: 'add_hidden_checkbox_to_taxons_form',
  insert_after: "erb[loud]:contains('text_field :meta_keywords')",
  text: %{
    <%= f.field_container :hidden, class: ['checkbox'] do %>
      <%= f.label :hidden do %>
        <%= f.check_box :hidden %> <%=raw Spree.t(:hidden, scope: :taxon) %>
      <% end %>
      <%= f.error_message_on :hidden %>
    <% end %>
  }
)

Deface::Override.new(
  virtual_path: 'spree/admin/taxons/_form',
  name: 'Add boost factor to taxons',
  insert_after: "erb[loud]:contains('text_field :meta_keywords')",
  text: %(
    <%= f.field_container :boost_factor, class: ['form-group'] do %>
      <%= f.label :boost_factor %>
      <%= f.number_field :boost_factor, class: 'form-control' %>
      <%= f.error_message_on :boost_factor %>
    <% end %>
  )
)

Deface::Override.new(
  virtual_path: 'spree/admin/taxons/_form',
  name: 'Add promoted fields to taxons',
  insert_after: "erb[loud]:contains('text_field :meta_keywords')",
  text: %(
    <%= f.field_container :promoted, class: ['checkbox'] do %>
      <%= f.label :promoted do %>
        <%= f.check_box :promoted %> Promoted
      <% end %>
      <%= f.error_message_on :promoted %>
    <% end %>

    <%= f.field_container :promoted_title, class: ['form-group'] do %>
      <%= f.label :promoted_title %>
      <%= f.text_field :promoted_title, class: 'form-control' %>
      <%= f.error_message_on :promoted_title %>
    <% end %>

    <%= f.field_container :promoted_position, class: ['form-group'] do %>
      <%= f.label :promoted_position %>
      <%= f.number_field :promoted_position, class: 'form-control', min: 0 %>
      <%= f.error_message_on :promoted_position %>
    <% end %>
  )
)
