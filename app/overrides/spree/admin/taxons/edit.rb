Deface::Override.new(
  virtual_path: 'spree/admin/taxons/edit',
  name: 'Add reindex button in taxons form',
  insert_after: '[data-hook="buttons"]',
  text: <<-HTML
    <br>
    <%= link_to_with_icon('cog', 'Reindex', spree.reindex_admin_taxon_path(@taxon), class: 'btn btn-danger', method: :post) if spree_current_user.admin? %>
  HTML
)
