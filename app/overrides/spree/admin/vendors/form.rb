Deface::Override.new(
  virtual_path: 'spree/admin/vendors/_form',
  name: 'Add Global Rewards to Vendor form',
  insert_bottom: 'div.form-group',
  text: <<-HTML
          <%= f.field_container :rewards do %>
            <%= f.label :rewards %>
            <%= f.select :rewards, rewards_options(f.object.rewards), { include_blank: Spree.t('default_rewards', rewards: Spree::Config[:rewards]) }, class: 'form-control' %>
          <% end %>

          <% if spree_current_user.admin? %>
            <%= f.field_container :subsidized_rewards do %>
              <%= f.label :subsidized_rewards %>
              <%= f.select :subsidized_rewards, subsidized_rewards_options(f.object.subsidized_rewards), {}, class: 'form-control' %>
            <% end %>
          <% end %>
  HTML
)

Deface::Override.new(
  virtual_path: 'spree/admin/vendors/_form',
  name: 'Add email settings to Vendor form',
  insert_after: "erb[loud]:contains('text_field :customer_support_email')",
  text: <<-HTML
          <% if spree_current_user.admin? %>
            <%= f.field_container :notify_orders do %>
              <%= f.label :notify_orders %>
              <%= f.check_box :notify_orders, class: 'form-control' %>
            <% end %>
          <% end %>
  HTML
)
