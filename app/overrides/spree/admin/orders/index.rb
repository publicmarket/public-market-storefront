Deface::Override.new(
  virtual_path: 'spree/admin/orders/index',
  name: 'Add report button',
  replace: "erb[loud]:contains('admin_new_order')",
  text: <<-HTML
    <%= form_with url: admin_sales_report_path, local: true do |form| %>
      <%= select_month(Date.today, {}, { class: 'select2', style: 'display: inline-block' }) %>
      <%= select_year(Date.today, { start_year: Date.today.year, end_year: 2018 }, { class: 'select2', style: 'display: inline-block' }) %>
      <button class='btn btn-success'>Download Sales Report</button>
    <% end %>
  HTML
)
