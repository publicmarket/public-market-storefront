Deface::Override.new(
  virtual_path: 'spree/admin/properties/_form',
  name: 'Add hidden flag to product form',
  insert_bottom: 'div[data-hook=admin_property_form]',
  text: <<-HTML
          <div class='col-md-6'>
            <%= f.field_container :hidden, class: ['form-group'] do %>
              <%= f.label :hidden, Spree.t(:hidden) %>
              <%= f.check_box :hidden, class: 'form-control' %>
            <% end %>
          </div>
  HTML
)
