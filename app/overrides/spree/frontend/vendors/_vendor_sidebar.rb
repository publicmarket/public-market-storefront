Deface::Override.new(
  virtual_path: 'spree/vendors/_vendor_sidebar',
  name: 'Add rating under vendor name',
  insert_after: '.vendor-page-sidebar .vendor-page-info-name',
  text: "<div class='text-muted normal-font-weight'><%= vendor_reputation_text(@vendor, format: :text) %></div>"
)

Deface::Override.new(
  virtual_path: 'spree/vendors/show',
  name: 'Add disclaimer',
  insert_after: '.vendor-page',
  text: <<~HTML
    <hr class='blue-divider' style='margin-top: 80px'>
    <div class='disclaimer-block'><%= render 'shared/disclaimer', anchor: false %></div>
  HTML
)
