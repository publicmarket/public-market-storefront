module Spree
  module Leads
    class Push < Spree::BaseAction
      param :input

      def call
        response = add_lead
        return if response && response['new_count'].zero?

        add_leads_to_list(response['persisted_recipients'])
        send_newsletter_mail
      end

      private

      def add_lead
        HTTParty.post(
          'https://api.sendgrid.com/v3/contactdb/recipients',
          headers: headers,
          body: body
        )
      end

      def add_leads_to_list(leads)
        HTTParty.post(
          "https://api.sendgrid.com/v3/contactdb/lists/#{sg_list}/recipients",
          headers: headers,
          body: leads.to_json
        )
      end

      def send_newsletter_mail
        HTTParty.post(
          'https://api.sendgrid.com/v3/mail/send',
          headers: headers,
          body: mail_body
        )
      end

      def sg_list
        Rails.application.credentials.sendgrid_io_newsletter_list
      end

      def headers
        {
          'Authorization' => "Bearer #{Rails.application.credentials.sendgrid_campaigns_key}",
          'Content-Type' => 'application/json'
        }
      end

      def body
        [
          {
            email: input['email'],
            first_name: input['given_name'],
            last_name: input['family_name']
          }
        ].to_json
      end

      def mail_body # rubocop:disable Metrics/MethodLength
        {
          personalizations: [
            {
              to: [
                {
                  email: input['email']
                }
              ],
              dynamic_template_data: {
                email: input['email']
              }
            }
          ],
          from: {
            email: 'info@publicmarket.io',
            name: 'Public Market Updates'
          },
          reply_to: {
            email: 'team@publicmarket.io',
            name: 'Public Market Team'
          },
          categories: %w[newsletter],
          template_id: Rails.application.credentials.sendgrid_newsletter_template,
          asm: {
            group_id: unsubscribe_group,
            groups_to_display: [unsubscribe_group]
          }
        }.to_json
      end

      def unsubscribe_group
        @unsubscribe_group ||= YAML.load_file(Rails.root.join('config', 'data', 'unsubscribe-groups.yml'))['newsletter']
      end
    end
  end
end
