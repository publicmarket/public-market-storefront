module Spree
  module Leads
    class Upload < Spree::BaseAction
      param :file
      option :source, optional: true

      def call
        process_leads
      end

      private

      def process_leads
        CSV.foreach(file.path, headers: true) do |row|
          save_lead(row.to_hash)
        end
      end

      def save_lead(row)
        attrs = map_lead_row(row)
        lead = Lead.find_or_initialize_by(email: attrs[:email])
        lead.assign_attributes(attrs)
        lead.save
      end

      def map_lead_row(row)
        {
          email: row['Email'],
          source: source,
          source_created_at: row['Date'],
          source_id: row['Social ID'],
          source_parent_id: row['Referral Social ID'],
          metadata: {
            first_name: row['First Name'],
            last_name: row['Last Name'],
            parent_email: row['Referral Email'],
            share_url: row['Share Url'],
            status_url: row['Thank You Url']
          }
        }
      end
    end
  end
end
