module Spree
  module Orders
    class UpdateLineItemAction
      attr_accessor :options

      def initialize(options)
        self.options = options.deep_dup
      end

      def call
        if item_number.present?
          update_inventory_unit
        elsif sku.present?
          update_line_item
        end
      end

      private

      def update_inventory_unit
        unit = InventoryUnit.find_by_hash_id(item_number) # rubocop:disable Rails/DynamicFindBy
        return { item_number => 'Not found' } if unit.blank? || !check_order(unit)
        return slack_cancellation(unit) if action == 'cancel'

        perform_action(unit)

        { item_number => unit.state }
      end

      def update_line_item # rubocop:disable Metrics/AbcSize
        line_item = LineItem.joins(:order, :variant)
                            .where(spree_orders: { number: order_number })
                            .where(spree_variants: { sku: sku }).first
        return { sku => 'Not found' } if line_item.blank?
        return slack_cancellation(line_item.inventory_units[0]) if action == 'cancel'

        line_item.inventory_units.each(&method(:perform_action))

        { sku => line_item.inventory_units.first.state }
      end

      def check_order(unit)
        options[:order_number] == unit.order.number || options[:order_id] == unit.order.hash_id
      end

      def item_number
        options[:item_number]
      end

      def sku
        options[:sku]
      end

      def order_number
        options[:order_number]
      end

      def tracking_number
        options[:tracking_number]
      end

      def shipped_at
        options[:shipped_at].to_i
      end

      def action
        options[:action]
      end

      def courier
        options[:courier]
      end

      def perform_action(unit)
        case action
        when 'cancel'
          cancel_unit(unit)
        when 'confirm', 'ship'
          ship_unit(unit)
        end
      end

      def cancel_unit(unit)
        unit.cancel!
      end

      def ship_unit(unit)
        return if unit.shipped?

        shipment = unit_shipment(unit)
        update_shipping_data(shipment)
        unit.ship!
      end

      def unit_shipment(unit)
        shipment = unit.shipment
        return shipment if tracking_number.blank? || shipment.tracking.blank? || tracking_number == shipment.tracking

        split_shipment(unit)
      end

      def split_shipment(unit)
        shipment = unit.shipment
        new_shipment = shipment.order.shipments.create!(stock_location: shipment.stock_location, address: shipment.address)
        unit.update(shipment: new_shipment)
        unit.order.update_with_updater!
        new_shipment
      end

      def update_shipping_data(shipment)
        shipment.shipped_at = Time.zone.at(shipped_at) if shipped_at.positive?
        shipment.tracking = tracking_number if tracking_number.present? && shipment.tracking.blank?
        shipment.courier = courier if courier.present?
        shipment.save
      end

      def slack_cancellation(unit) # rubocop:disable Metrics/AbcSize
        order = unit.order

        message = I18n.t('slack.item-cancellation',
                         order_id: order.id,
                         order_number: order.number,
                         user_name: order.user.full_name,
                         vendor_name: unit.variant.vendor.name,
                         order_url: spree_url_helpers.edit_admin_order_url(order),
                         product_name: unit.variant.product.name)

        slack(message, unit.variant.vendor)

        { (item_number || sku) => unit.state }
      end

      def slack(message, vendor)
        PublicMarket::Workers::Slack::NotifierWorker.perform_async(
          message: message,
          channel: 'order-issues',
          username: vendor.name
        )
      end

      def spree_url_helpers
        @spree_url_helpers ||= Spree::Core::Engine.routes.url_helpers
      end
    end
  end
end
