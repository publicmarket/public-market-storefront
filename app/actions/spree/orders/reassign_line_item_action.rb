module Spree
  module Orders
    class ReassignLineItemAction < Spree::BaseAction
      param :line_item
      param :vendor

      attr_accessor :new_shipment, :new_variant, :original_shipments, :variant

      def call
        Spree::LineItem.transaction do
          find_or_create_shipment
          find_or_create_variant

          update_line_item
          process_shipments
        end
        @new_shipment
      end

      private

      def update_line_item
        line_item.inventory_units.each { |iu| iu.update(variant: new_variant, shipment: new_shipment) }
        line_item.update(variant: new_variant)
      end

      def process_shipments
        original_shipments.each do |shipment|
          if shipment.inventory_units.length.zero?
            shipment.destroy!
          else
            shipment.refresh_rates
          end
        end

        new_shipment.refresh_rates
        new_shipment.order.update_with_updater!
      end

      def find_or_create_shipment
        @original_shipments = line_item.inventory_units.map(&:shipment)

        vendor_view = VendorView.new(line_item.order, vendor)
        @new_shipment = vendor_view.shipments.first || line_item.order.shipments.create!(stock_location: new_stock_location)
      end

      def find_or_create_variant
        @variant = line_item.variant

        attrs = @variant.attributes.except('id', 'updated_at', 'created_at')
        @new_variant = Variant.find_by(vendor: vendor.id, sku: @variant.sku) ||
                       Variant.create!(attrs.merge(vendor_id: vendor.id, option_values: @variant.option_values))
        # @new_variant.stock_items.first.set_count_on_hand(line_item.quantity)
      end

      def new_stock_location
        vendor.stock_locations.first
      end
    end
  end
end
