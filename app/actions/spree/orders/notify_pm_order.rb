module Spree
  module Orders
    class NotifyPmOrder < Spree::BaseAction
      param :order
      param :shipment

      def call
        notify_slack
      end

      private

      def notify_slack
        PublicMarket::Workers::Slack::NotifierWorker.perform_async(
          message: message,
          channel: 'pmf-orders',
          username: order.email
        )
      end

      def message
        I18n.t(
          'slack.pm-order',
          email: order.email,
          product_title: shipment.line_items.map { |li| li.variant.name }.join(','),
          order_number: order.number,
          url: spree_url_helpers.edit_admin_order_url(order)
        )
      end

      def spree_url_helpers
        @spree_url_helpers ||= Spree::Core::Engine.routes.url_helpers
      end
    end
  end
end
