module Spree
  module Orders
    class SalesReportAction < Spree::BaseAction
      include ActionView::Helpers::NumberHelper

      param :vendor
      param :date

      def call
        vendor.present? ? vendor_report : admin_report
      end

      private

      def vendor_report
        orders = fetch_orders.where(spree_variants: { vendor_id: vendor.id })
                             .where(spree_payment_transfers: { vendor_id: vendor.id })

        CSV.generate(headers: true) do |csv|
          csv << header_row

          orders.each { |order| csv << order_row(order, vendor) }
        end
      end

      def admin_report
        orders = fetch_orders

        CSV.generate(headers: true) do |csv|
          csv << header_row.push('Vendor')

          orders.each do |order|
            order.vendors.each do |vendor|
              csv << order_row(order, vendor).push(vendor.name)
            end
          end
        end
      end

      def fetch_orders
        Order.joins(line_items: :variant, payments: :payment_transfers)
             .includes(ship_address: %i[state])
             .complete
             .where(payment_state: :paid)
             .where(shipment_state: :shipped)
             .where(spree_payment_transfers: { created_at: date.beginning_of_month..date.end_of_month })
             .order('spree_orders.updated_at DESC')
             .distinct
      end

      def header_row
        ['Ship Date', 'Order Number', 'Total Sale', 'Stripe Fee', 'Rewards', 'Amount', 'Refunded',
         'Address1', 'Address2', 'City', 'State', 'Country', 'Zip', 'First Name', 'Last Name']
      end

      def order_row(order, vendor) # rubocop:disable Metrics/AbcSize
        view = VendorView.new(order, vendor)

        [
          ship_date(view)&.strftime('%D'),
          view.number,
          number_to_currency(view.total),
          number_to_currency(view.stripe_fee),
          number_to_currency(view.rewards_amount),
          number_to_currency(view.transfer_amount),
          number_to_currency(view.refunded_amount),
          view.ship_address.address1,
          view.ship_address.address2,
          view.ship_address.city,
          view.ship_address.state&.abbr,
          view.ship_address.country,
          view.ship_address.zipcode,
          view.ship_address.first_name,
          view.ship_address.last_name
        ]
      end

      def ship_date(order)
        order.shipments.last.shipped_at
      end
    end
  end
end
