module Spree
  module Inventory
    class FindProductVariations < Spree::BaseAction
      param :product
      param :previous_variation, optional: true
      option :filter_by_variation, optional: true, default: proc { nil }
      option :load_variants, optional: true, default: proc { false }

      def call
        if (variation_property = product.property(:variation_property))
          find_variation_group(variation_property)
        else
          products = Searchers::FindSimilarProducts.call(product, filter_by_variation: filter_by_variation)

          map_search_results(products.results)
        end
      end

      private

      def find_variation_group(variation_property)
        variation_parent = product.find_property(:variation_group_id)

        # if there is no variation group but variation property exists - show only current product
        if variation_parent.blank?
          products = [product]
        else
          products =
            Spree::Product.joins(:product_properties)
                          .where(spree_product_properties: { property_id: variation_parent.property_id, value: variation_parent.value })

          products = products.where(id: product.id) if filter_by_variation
        end

        products.map do |product|
          variation_value = product.property(variation_property)

          map_variation(variation_value, product)
        end
      end

      def map_search_results(products)
        flatten_variation_properties(products).group_by { |p| p['variations'] }.map do |k, v|
          # always select current product of previous product as variation
          product_variation = v.find do |var|
            selectable_variations.include?(var[:_id].to_i)
          end

          # select product with min price as variation
          product_variation ||= v.min_by { |prod| prod[:price] }

          ids = v.map(&:_id)
          # don't show current product variant in variation box if it's only one existing and variation exists
          # ids = [] if !k.nil? && ids.one? && ids[0] == product_variation[:_id]

          # variants = find_similar_variants(ids, blank_variation: k.nil?)
          variants = find_similar_variants(ids)

          product_variation = Spree::Product.find_by(id: product_variation[:_id])
          map_variation(k, product_variation, variants)
        end
      end

      def selectable_variations
        @selectable_variations ||= [product.id, previous_variation&.id]
      end

      def flatten_variation_properties(products)
        # allow to group by all variations
        products.flat_map do |product|
          if product['variations'].present?
            product['variations'].map do |v|
              new_prod = product.dup
              new_prod['variations'] = v
              new_prod
            end
          else
            # keep nil in array to show blank variation
            new_prod = product.dup
            new_prod['variations'] = nil
            [new_prod]
          end
        end
      end

      # def find_similar_variants(ids, blank_variation: false)
      def find_similar_variants(ids)
        return [] if ids.blank?

        variants = Spree::Variant.not_discontinued
                                 .not_deleted
                                 .in_stock
                                 .where(is_master: false, product_id: ids)
                                 .includes(:default_price)

        # find best variant for products if variation present
        # variants = variants.find_best_price_in_option unless blank_variation

        if load_variants
          variants.includes(:vendor, :product)
                  .sort_by(&:price)
        else
          variants.sort_by { |v| v.main_option&.position || 0 }
                  .group_by(&:mapped_main_option_value)
                  .map { |k, v| map_similar_variants(k, v) }
        end
      end

      def map_variation(variation, product, variants = [])
        {
          variation_name_presentation: product.variation_formatter.format,
          variation_name: variation,
          similar_variants: variants,
          price: product.price,
          slug: product.slug,
          id: product.id.to_i
        }
      end

      def map_similar_variants(option_value, variants)
        {
          option_value: option_value.titleize,
          size: variants.size,
          price: variants.min_by(&:price).price
        }
      end
    end
  end
end
