module Spree
  module Inventory
    module Searchers
      class TaxonSearcher < ProductSearcher
        param :taxon
        option :taxon_ids, optional: true, default: proc { taxon.id }

        def call
          result = search_products.aggs
          @buckets = result.dig('categories', 'buckets')

          taxon.children
               .visible
               .where.not(name: Taxon::UNCATEGORIZED_NAME)
               .reorder(:name)
               .map(&method(:find_bucket))
               .compact
        end

        def apply_rand?
          false
        end

        def aggs
          {
            categories: {
              terms: { field: :taxon_ids, size: 1000, min_doc_count: 1 }
            }
          }
        end

        private

        def find_bucket(child_taxon)
          bucket = @buckets.find { |b| b['key'] == child_taxon.id }
          child_taxon if bucket.present?
        end
      end
    end
  end
end
