module Spree
  module Inventory
    module Searchers
      class ProductSearcher < BaseSearcher
        NO_IMAGE_FACTOR = -10

        option :current_user, optional: true
        option :sort, optional: true
        option :taxon_ids, optional: true, default: proc { [] }
        option :enable_aggs, optional: true, default: proc { false }
        option :includes, optional: true, default: proc { [:tax_category, :best_variant, master: %i[default_price prices images]] }
        option :operator, optional: true, default: proc { :or }

        def call
          search_products
        end

        def retrieve_products
          search_products
        end

        private

        def prepare_body(body)
          if apply_rand?
            functions = body.dig(:query, :function_score, :functions)
            functions << { random_score: { seed: rand_seed }}
          end

          return if keywords.blank?

          keyword_query = %i[query bool must dis_max]

          # function score is used with boosting enabled
          if (function_score = body.dig(:query, :function_score, *keyword_query)).present?
            function_score[:tie_breaker] = 1
          elsif (body_keyword_query = body.dig(*keyword_query)).present?
            body_keyword_query[:tie_breaker] = 1
          end
        end

        def apply_rand?
          [keywords.blank?, order.blank?].all?
        end

        def rand_seed
          Time.now.to_i / 3600
        end

        def where
          where_query = {
            active: true,
            in_stock: { gt: 0 },
            or: [],
            price: { gt: 0.3 }
          }

          if (taxon_filter_values = taxon_filter).present?
            where_query[:taxon_ids] = taxon_filter_values
          end

          where_query.merge(filter)
        end

        def taxon_filter
          if taxon_ids.present?
            if taxon_ids.is_a?(Hash)
              taxon_ids
            else
              taxon_ids_cast = Array(taxon_ids)
              hidden_taxon_ids = Spree::Taxon.hidden_taxon_ids(taxon_ids_cast)
              {
                in: taxon_ids_cast,
                not: hidden_taxon_ids.presence
              }
            end
          else
            {
              not: Spree::Taxon.hidden_taxon_ids.presence
            }
          end
        end

        def aggs
          return unless enable_aggs

          aggregations = {
            variations: {
              terms: {
                field: :variations
              }
            }
          }

          Spree::Taxonomy.filterable.each do |taxonomy|
            field = taxonomy.filter_name.to_sym
            aggregations[field] = { terms: { field: field }}
          end

          Spree::Property.filterable.each do |property|
            field = property.filter_name.to_sym
            aggregations[field] = { terms: { field: field }}
          end

          aggregations
        end

        def order
          return if sort.blank?

          [add_popularity_sort, add_price_sort].compact.map { |s| default_sort.merge(s) }
        end

        def add_popularity_sort
          return if (sort_option = sort.symbolize_keys[:popularity]).blank?

          case sort_option
          when 'all_time'
            { conversions: { order: :desc, unmapped_type: 'short' }}
          when 'month'
            { conversions_month: { order: :desc, unmapped_type: 'short' }}
          end
        end

        ALLOWED_PRICE_ORDER = %w[asc desc].freeze
        def add_price_sort
          sort_option = sort.symbolize_keys[:price]
          return if sort_option.blank? || !ALLOWED_PRICE_ORDER.include?(sort_option)

          { price: { order: sort_option, unmapped_type: 'short' }}
        end

        def default_sort
          {
            no_image: { order: :asc, unmapped_type: 'boolean' },
            _script: {
              type: 'number',
              script: {
                lang: 'painless',
                source: "doc['boost_factor'].value > 0 ? 1 : 0"
              },
              order: 'desc'
            }
          }
        end

        def boost_by
          {
            boost_factor: { factor: 1, missing: 1, modifier: 'none' },
            no_image: { factor: NO_IMAGE_FACTOR, missing: 0, modifier: 'none' }
          }
        end
      end
    end
  end
end
