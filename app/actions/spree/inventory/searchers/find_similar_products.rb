module Spree
  module Inventory
    module Searchers
      class FindSimilarProducts < BaseSearcher
        param :product
        option :filter_by_variation, optional: true

        def call
          find_similar
        end

        private

        def find_similar
          where_filters = fields_to_match
          where_filters.merge!(required_fields)
          where_filters.merge!(filters)

          Spree::Product.search(
            '*',
            where: where_filters,
            load: false
          )
        end

        def fields_to_match
          fields = {
            'name' => product.name,
            'active' => true,
            'in_stock' => { gt: 0 },
            'price' => { gt: 0.3 }
          }

          if (hidden_taxon_ids = Spree::Taxon.hidden.ids).present?
            fields['taxon_ids'] = { not: hidden_taxon_ids }
          end

          fields[product.author_property_name] = product.subtitle if product.subtitle
          fields[product.subtitle_property_name] = product.property(product.subtitle_property_name) if product.subtitle_property_name

          fields
        end

        def required_fields
          return {} if product.taxonomy.blank?

          taxonomy_field = "#{product.taxonomy.name.downcase}_ids"
          { taxonomy_field => { not: nil }}
        end

        def filters
          filter_by_variation ? { variations: filter_by_variation } : {}
        end
      end
    end
  end
end
