module Spree
  module Inventory
    module Providers
      class DefaultClassifier < Spree::BaseAction
        param :product
        param :taxon_candidates, optional: true, default: proc { [[]] }
        option :default_taxonomy, optional: true, default: proc { other_taxonomy }

        def call
          categorize
        end

        private

        def categorize
          # find child taxons inside array of arrays with taxon hierarchy
          matching_taxons = taxon_candidates.map(&method(:map_taxon_candidates))
                                            .compact
                                            .uniq

          assign_to_taxons(matching_taxons)
        end

        def map_taxon_candidates(taxons)
          current_taxon =
            # if default taxonomy wasn't passed to classifier - try to use first taxon as taxonomy
            if default_taxonomy == other_taxonomy && (first_taxon_is_taxonomy = Taxonomy.find_by('name ILIKE ?', taxons.first)).present?
              # remove first taxon from list if it's taxonomy
              taxons.shift
              first_taxon_is_taxonomy.root
            else
              default_taxonomy.root
            end

          taxons.reject { |taxon_name| taxon_name == rejected_taxon_name }.each do |taxon|
            new_current_candidate = find_taxon(current_taxon, taxon)

            # save last matched taxon as current
            new_current_candidate.present? ? current_taxon = new_current_candidate : break
          end

          current_taxon if current_taxon != other_taxonomy_root
        end

        def rejected_taxon_name
          nil
        end

        def find_taxon(current_taxon, taxon)
          # match using ILIKE to make match case-insensitive
          current_taxon.children.find_by('name ILIKE ?', taxon)
        end

        def assign_to_taxons(matching_taxons)
          if matching_taxons.any?
            matching_taxons.each do |taxon|
              next if product.taxons.include?(taxon)

              if taxon.root?
                categorize_as_uncategorized(product, taxon.taxonomy)
              else
                taxon.products << product
              end
            end
          else
            categorize_as_uncategorized(product)
          end
        end

        def categorize_as_uncategorized(product, taxonomy = default_taxonomy)
          uncategorized_taxon = taxonomy.uncategorized_taxon
          uncategorized_taxon.products << product unless product.taxons.include?(uncategorized_taxon)
        end

        def other_taxonomy
          @other_taxonomy ||= Taxonomy.other
        end

        def other_taxonomy_root
          @other_taxonomy_root ||= other_taxonomy.root
        end
      end
    end
  end
end
