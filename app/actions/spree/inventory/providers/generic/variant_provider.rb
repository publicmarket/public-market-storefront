require 'dry-validation'

module Spree
  module Inventory
    module Providers
      module Generic
        class VariantProvider < DefaultVariantProvider
          DEFAULT_OPTION_TYPE_NAME = 'Default'.freeze
          VALIDATION_SCHEMA =
            ::Dry::Validation.Schema do
              required(:sku).filled(:str?)
              required(:quantity).filled(:int?)

              optional(:product_id) { none? | str? }
              optional(:categories) { array? | str? }
              optional(:price) { none? | decimal? }
              optional(:product_id_type) { none? | str? }
              optional(:title) { str? }
              optional(:description) { str? }
              optional(:notes) { str? }
              optional(:option_types) { array? | str? }
              optional(:images) { array? | str? }
              optional(:weight) { none? | decimal? }
              optional(:height) { none? | decimal? }
              optional(:width) { none? | decimal? }
              optional(:depth) { none? | decimal? }
              optional(:rewards_percentage) { int? }
              optional(:keywords) { str? }
            end

          protected

          def find_metadata(_identifier)
            MetadataProvider.call(item_json.stringify_keys, VALIDATION_SCHEMA.rules.keys)
          end

          def categorize(product, taxons)
            case taxons.first
            when 'Books'
              Books::Classifier.call(product, taxons)
            else
              classifier.call(product, taxons)
            end
          end

          def update_variant_hook(variant, item)
            variant.rewards = item[:rewards_percentage]
            super
          end

          def cast_values(item_json)
            item_json[:quantity] = cast(item_json[:quantity], &:to_i)

            %i[price weight height width depth].each do |key|
              item_json[key] = cast(item_json[key]) { |v| cast_float(v) }
            end

            item_json
          end

          private

          def cast_float(value)
            Float(value.tr(',', '.')).to_d
          rescue ArgumentError
            value
          end

          def product_identifier(hash)
            [hash[:product_id_type] || 'gen', hash[:product_id]].join('-').downcase
          end

          def master_variant_attributes(metadata)
            dimemsions = metadata[:dimensions]
            {
              weight: dimemsions[:weight],
              height: dimemsions[:height],
              width: dimemsions[:width],
              depth: dimemsions[:depth]
            }
          end

          def variant_option_value(item, option_type)
            return DEFAULT_OPTION_TYPE_NAME if option_type[:name] == DEFAULT_OPTION_TYPE_NAME

            super(item, option_type)
          end

          def option_types
            types = item_json[:option_types].is_a?(Array) ? item_json[:option_types] : item_json[:option_types].to_s.split(',')

            return types.map(&:strip).map { |t| { name: t, presentation: t.humanize } } if types.present?

            [{ name: DEFAULT_OPTION_TYPE_NAME, presentation: 'Default', values: [{ name: DEFAULT_OPTION_TYPE_NAME }] }]
          end
        end
      end
    end
  end
end
