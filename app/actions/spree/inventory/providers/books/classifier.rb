module Spree
  module Inventory
    module Providers
      module Books
        class Classifier < DefaultClassifier
          option :default_taxonomy, optional: true, default: proc { Taxonomy.create_with(filterable: true).find_or_create_by(name: 'Books') }

          private

          def categorize
            matching_taxons = taxon_candidates.map(&method(:map_taxon_candidates))
                                              .compact
                                              .uniq

            # drop parent taxons if any of matches are childs
            matching_taxons = matching_taxons.reject do |taxon|
              matching_taxons.any? { |b| b != taxon && b.ancestors.find_by(id: taxon.id).present? }
            end

            assign_to_taxons(matching_taxons)
          end

          def rejected_taxon_name
            'General'
          end
        end
      end
    end
  end
end
