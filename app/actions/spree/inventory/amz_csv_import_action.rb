module Spree
  module Inventory
    class AmzCsvImportAction < CSVImportAction
      LINES_TO_SKIP = /The top 3 rows are for Amazon.com|Product Type/.freeze
      EXCLUDED_FIELDS = %w[
        feed_product_type
        item_type
        item_weight_unit_of_measure
        parent_child
        relationship_type
      ].freeze

      param :local_file

      def map_items
        index = 0
        CSV.foreach(local_file, headers: true, skip_blanks: true, skip_lines: LINES_TO_SKIP) do |row|
          next if (item = item_json(row)).blank?

          yield(item, index)
          index += 1
        end
      rescue Errno::ENOENT
        raise ImportError, t('invalid_csv.default')
      end

      private

      def item_json(row) # rubocop:disable Metrics/AbcSize
        row_json = row.to_hash.with_indifferent_access

        return if row_json[:parent_child]&.casecmp('Parent')&.zero?

        row_json = prepare_row_json(row_json)

        {
          product_id: row_json.delete(:external_product_id).presence || row_json[:item_sku],
          product_id_type: row_json.delete(:external_product_id_type).presence || :sku,
          sku: row_json.delete(:item_sku),
          categories: [],
          quantity: row_json.delete(:quantity),
          price: row_json.delete(:standard_price),
          title: row_json.delete(:item_name),
          description: item_description(row_json),
          images: item_images(row_json),
          weight: row_json.delete(:item_weight),
          height: row_json.delete(:item_height),
          width: row_json.delete(:item_width),
          depth: row_json.delete(:item_length),
          variation_group_id: row_json.delete(:parent_sku),
          variation_property: variation_property(row_json)
        }.merge(row_json)
      end

      def prepare_row_json(row)
        row.compact
           .delete_if { |k, _v| EXCLUDED_FIELDS.include?(k) }
      end

      def item_description(json)
        bullet_points =
          json.select { |k, _v| k =~ /bullet_point/ }
              .map do |k, v|
                json.delete(k)
                "<li>#{v}</li>" if v
              end.compact.join

        [json.delete(:product_description), '<ul>' + bullet_points + '</ul>'].compact.join(' ')
      end

      def item_images(json)
        other_images =
          json.select { |k, _v| k =~ /other_image_url/ }
              .map do |k, v|
                json.delete(k)
                v
              end.compact

        [json.delete(:main_image_url)] + other_images
      end

      def variation_property(row_json)
        return if (property_name = row_json.delete(:variation_theme)).blank?

        property_name.underscore
      end
    end
  end
end
