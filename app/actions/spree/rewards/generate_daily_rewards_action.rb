module Spree
  module Rewards
    class GenerateDailyRewardsAction < Spree::BaseAction
      REFEREES_PERCENTAGE = 0.3
      REFEREES_THRESHOLD = 1000

      param :date

      def call
        spent = user_spent
        total_spent = spent[:users].values.sum + REFEREES_PERCENTAGE * spent[:referees].values.sum

        return if total_spent.zero?

        generate_rewards(spent, total_spent)
      end

      private

      def generate_rewards(spent, total_spent)
        UserReward.where(date: date.to_date).delete_all
        UserReward.bulk_insert(:kind, :user_id, :amount, :referees_amount, :paid_amount, :date, :created_at) do |worker|
          add_user_rewards(spent, UserReward.total_pool_amount(date.to_date) / total_spent, worker)
        end
      end

      def total_spent
        Payment.with_states(:completed, :pending)
               .reorder(nil)
               .joins(order: :user)
               .where(created_at: date.beginning_of_day..date.end_of_day)
               .group('spree_users.id')
               .pluck(Arel.sql('spree_users.id, sum(spree_payments.amount), min(spree_users.referrer_id)'))
      end

      def user_spent
        total_spent.each_with_object(users: {}, referees: {}) do |user_spent, result|
          user_id, spent, referrer_id = user_spent
          result[:users][user_id] ||= 0
          result[:users][user_id] += spent

          next if referrer_id.blank?

          result[:users][referrer_id] ||= 0
          result[:referees][referrer_id] ||= 0
          result[:referees][referrer_id] += lifetime_threshold(user_id, spent)
        end
      end

      def add_user_rewards(spent, quote, worker)
        spent[:users].keys.map do |user_id|
          referees_amount = quote * REFEREES_PERCENTAGE * (spent[:referees][user_id] || 0)
          paid_amount = spent[:users][user_id]
          rewards_amount = quote * paid_amount + referees_amount

          worker.add([:daily, user_id, rewards_amount, referees_amount, paid_amount, date.to_date])
          schedule_email_update(user_id, date.to_date)
        end
      end

      def lifetime_threshold(user_id, spent)
        total_paid = UserReward.where(user_id: user_id).where.not(date: date.to_date).sum(:paid_amount)
        allowed = [REFEREES_THRESHOLD - total_paid, 0].max
        [spent, allowed].min
      end

      def schedule_email_update(user_id, date)
        wait_time = Rails.env.production? ? 6.hours : 1.minute
        UserMailer.daily_rewards_update(user_id, date.to_s).deliver_later(wait: wait_time)
      end
    end
  end
end
