class AddDescriptionToSpreeProductCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_product_collections, :description, :text
  end
end
