class AddInfluencerSlugToProductCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_product_collections, :influencer_slug, :string
  end
end
