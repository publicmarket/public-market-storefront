class RemoveAttachedImageFromSpreeProductCollection < ActiveRecord::Migration[5.2]

  class Spree::ProductCollection201826010 < ApplicationRecord
    self.table_name = :spree_product_collections
    self.has_attached_file :image, path: Paperclip::Attachment.default_options[:path].gsub(':class', 'spree/product_collections')
  end

  class << self
    def up
      Spree::ProductCollection201826010.find_each do |pc|
        next if pc.image_file_name.blank?

        Spree::ProductCollection.find(pc.id).create_image(attachment: URI.parse(pc.image.url))
      end

      remove_attachment :spree_product_collections, :image
    end

    def down
      change_table :spree_product_collections do |t|
        t.attachment :image
      end
    end
  end
end
