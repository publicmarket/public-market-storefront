class AddUserToSpreeProductCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_product_collections, :user_id, :integer, index: true
  end
end
