class AddHiddenFlagToSpreeProperties < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_properties, :hidden, :boolean, default: false
  end
end
