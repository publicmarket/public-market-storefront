class AddCourierToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_shipments, :courier, :string
  end
end
