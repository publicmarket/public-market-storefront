class CreateSpreeLeads < ActiveRecord::Migration[5.2]
  def change
    create_table :spree_leads do |t|
      t.string :email
      t.string :source
      t.datetime :source_created_at
      t.string :source_id
      t.string :source_parent_id
      t.references :user, index: true
      t.json :metadata, null: false, default: {}

      t.timestamps
    end
  end
end
