class AddPmFulfillmentVendor < ActiveRecord::Migration[5.2]
  class Spree::Vendor20181005 < ApplicationRecord
    self.table_name = :spree_vendors
  end

  def up
    Spree::Vendor20181005.reset_column_information

    Spree::Vendor20181005.create(
      name: 'Public Market Fulfillment',
      slug: 'public-market',
      state: 'active',
      customer_support_email: 'team@publicmarket.io'
    )
  end
end
