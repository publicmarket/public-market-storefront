class AddUniqueIndexesToOptionValues < ActiveRecord::Migration[5.2]
  def up
    remove_index :spree_option_values, :option_type_id
    add_index :spree_option_values, [:option_type_id, :name], unique: true
  end

  def down
    remove_index :spree_option_values, [:option_type_id, :name]
    add_index :spree_option_values, :option_type_id, name: "index_spree_option_values_on_option_type_id"
  end
end
