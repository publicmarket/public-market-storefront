# This migration comes from spree_batch_api (originally 20181008100630)
class AddUploadIdAndIndextoVariants < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_variants, :upload_id, :integer
    add_column :spree_variants, :upload_index, :integer
  end
end
