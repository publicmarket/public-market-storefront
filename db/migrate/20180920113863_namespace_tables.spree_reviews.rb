# This migration comes from spree_reviews (originally 20120110172331)
class NamespaceTables < SpreeExtension::Migration[4.2]
  def change
    unless table_exists?('spree_reviews')
      rename_table :reviews, :spree_reviews
    end

    unless table_exists?('spree_feedback_reviews')
      rename_table :feedback_reviews, :spree_feedback_reviews
    end
  end
end
