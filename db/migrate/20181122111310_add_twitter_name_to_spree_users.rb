class AddTwitterNameToSpreeUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_users, :twitter_name, :string
  end
end
