class UnhideUncategorizedTaxons < ActiveRecord::Migration[5.2]
  class Spree::Taxon20180211 < ApplicationRecord
    self.table_name = :spree_taxons
    UNCATEGORIZED_NAME = 'Uncategorized'.freeze
  end

  class << self
    def up
      Spree::Taxon20180211.where(name: Spree::Taxon20180211::UNCATEGORIZED_NAME).find_each { |tax| tax.update(hidden: false) }
    end
  end
end
