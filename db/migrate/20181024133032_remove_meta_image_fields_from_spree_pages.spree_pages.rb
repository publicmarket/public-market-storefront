# This migration comes from spree_pages (originally 20181024132911)
class RemoveMetaImageFieldsFromSpreePages < ActiveRecord::Migration[5.2]
  def change
    remove_attachment :spree_pages, :meta_image
  end
end
