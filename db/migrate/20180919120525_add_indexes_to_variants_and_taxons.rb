class AddIndexesToVariantsAndTaxons < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def change
    ActiveRecord::Base.connection.execute('SET statement_timeout TO 0')

    add_index :spree_products_taxons, :taxon_id, name: 'index_spree_products_taxons_taxon_id_position', algorithm: :concurrently, where: 'position is not null'
    add_index :spree_variants, :sku, algorithm: :concurrently, name: 'index_spree_variants_master_sku', where: 'deleted_at is null and is_master'
  end
end
