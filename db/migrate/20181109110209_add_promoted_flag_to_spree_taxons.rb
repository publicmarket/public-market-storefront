class AddPromotedFlagToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :promoted, :boolean, default: false
  end
end
