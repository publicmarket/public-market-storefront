class AddBoostFactorToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :boost_factor, :integer, default: 0
  end
end
