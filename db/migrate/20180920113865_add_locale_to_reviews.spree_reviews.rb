# This migration comes from spree_reviews (originally 20120712182514)
class AddLocaleToReviews < SpreeExtension::Migration[4.2]
  def self.up
    unless column_exists?('spree_reviews', 'locale', :string)
      add_column :spree_reviews, :locale, :string, default: 'en'
    end
  end

  def self.down
    remove_column :spree_reviews, :locale
  end
end
