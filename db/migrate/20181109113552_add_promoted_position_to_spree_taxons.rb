class AddPromotedPositionToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :promoted_position, :integer, default: 0
  end
end
