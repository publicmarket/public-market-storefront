class ChangeRewardsType < ActiveRecord::Migration[5.2]
  def up
    change_column :spree_vendors, :rewards, :decimal, precision: 8, scale: 2
    change_column :spree_line_items, :rewards, :decimal, precision: 8, scale: 2
    change_column :spree_shipments, :rewards, :decimal, precision: 8, scale: 2
    change_column :spree_variants, :rewards, :decimal, precision: 8, scale: 2
  end

  def down
    change_column :spree_vendors, :rewards, :integer
    change_column :spree_line_items, :rewards, :integer
    change_column :spree_shipments, :rewards, :integer
    change_column :spree_variants, :rewards, :integer
  end
end
