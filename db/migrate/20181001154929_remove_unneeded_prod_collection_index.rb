class RemoveUnneededProdCollectionIndex < ActiveRecord::Migration[5.2]
  def change
    remove_index :spree_product_collection_products, name: 'index_spree_product_collection_products_on_product_id'
  end
end
