class AddRewardsToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_shipments, :rewards, :integer
  end
end
