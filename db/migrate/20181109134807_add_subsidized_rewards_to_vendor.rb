class AddSubsidizedRewardsToVendor < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_vendors, :subsidized_rewards, :decimal, precision: 8, scale: 2, null: false, default: 0
    add_column :spree_payment_transfers, :subsidized_rewards, :decimal, precision: 8, scale: 2, null: false, default: 0
  end
end
