class AddPaidAmountToUserRewards < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_user_rewards, :paid_amount, :decimal, precision: 10, scale: 2
  end
end
