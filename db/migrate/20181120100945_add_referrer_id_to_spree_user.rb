class AddReferrerIdToSpreeUser < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_users, :referrer_id, :integer, index: true
  end
end
