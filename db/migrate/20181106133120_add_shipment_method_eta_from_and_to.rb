class AddShipmentMethodEtaFromAndTo < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_shipping_methods, :eta_from, :integer, null: false, default: 5
    add_column :spree_shipping_methods, :eta_to, :integer, null: false, default: 9
  end
end
