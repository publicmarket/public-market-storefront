class ChangeTaxonIndex < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def up
    ActiveRecord::Base.connection.execute('SET statement_timeout TO 0')

    remove_index :spree_products_taxons, name: 'index_spree_products_taxons_taxon_id_position'
    add_index :spree_products_taxons, [:taxon_id, :position], name: 'index_spree_products_taxons_taxon_id_position', algorithm: :concurrently, where: 'position is not null'
  end
end
