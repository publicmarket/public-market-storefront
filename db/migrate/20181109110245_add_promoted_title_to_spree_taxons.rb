class AddPromotedTitleToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :promoted_title, :string
  end
end
