class FormatVendorSlugs < ActiveRecord::Migration[5.2]
  class Spree::Vendor20181311 < ApplicationRecord
    self.table_name = :spree_vendors
  end

  def up
    Spree::Vendor20181311.where(slug: 'bwb').update_all(slug: 'better-world-books')
    Spree::Vendor20181311.where(slug: 'indaba-direct').update_all(slug: 'direct-online')
    Spree::Vendor20181311.where(slug: 'rbm').update_all(slug: 'records-by-mail')
    Spree::Vendor20181311.where(slug: 'marketplacevalet').update_all(slug: 'marketplace-valet')
  end

  def down
    Spree::Vendor20181311.where(slug: 'better-world-books').update_all(slug: 'bwb')
    Spree::Vendor20181311.where(slug: 'direct-online').update_all(slug: 'indaba-direct')
    Spree::Vendor20181311.where(slug: 'records-by-mail').update_all(slug: 'rbm')
    Spree::Vendor20181311.where(slug: 'marketplace-valet').update_all(slug: 'marketplacevalet')
  end
end
