class CreateUserRewards < ActiveRecord::Migration[5.2]
  def change
    create_table :spree_user_rewards do |t|
      t.string :kind, null: false
      t.integer :user_id, null: false
      t.decimal :amount, precision: 10, scale: 2, null: false
      t.decimal :referees_amount, precision: 10, scale: 2
      t.date :date
      t.datetime :created_at, null: false
    end

    add_index :spree_user_rewards, :user_id
    add_index :spree_user_rewards, [:user_id, :date], unique: true, where: "kind = 'daily'"
  end
end
