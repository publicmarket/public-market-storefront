class AddUniqueIndexesToOptionTypes < ActiveRecord::Migration[5.2]
  def up
    remove_index :spree_option_types, :name
    add_index :spree_option_types, :name, unique: true
  end

  def down
    remove_index :spree_option_types, :name
    add_index :spree_option_types, :name, name: "index_spree_option_types_on_name"
  end
end
