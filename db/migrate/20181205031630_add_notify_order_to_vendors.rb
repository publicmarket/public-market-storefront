class AddNotifyOrderToVendors < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_vendors, :notify_orders, :boolean, default: false
  end
end
