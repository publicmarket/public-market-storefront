url_proto = %w[http https].include?(URI.parse(Spree::Store.default.url).scheme) ? '' : 'https://'
SitemapGenerator::Sitemap.default_host = url_proto + Spree::Store.default.url

SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::FogAdapter.new(
  fog_credentials: Rails.application.config.paperclip_defaults[:fog_credentials],
  fog_directory: Rails.application.credentials.google_uploads_bucket
)
SitemapGenerator::Sitemap.sitemaps_host = "https://storage.googleapis.com/#{Rails.application.credentials.google_uploads_bucket}/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  add_login
  add_signup
  add_password_reset

  add(top_selling_path)
  add(sell_apply_path)
  add(how_it_works_path)
  add(products_path)

  Spree::Taxon.visible.navigatable.select('spree_taxons.id, spree_taxons.permalink, spree_taxons.updated_at').find_each do |taxon|
    add(nested_taxons_path(taxon.permalink), lastmod: taxon.updated_at) if taxon.permalink.present?
  end

  products_query = Spree::Product.active
                                 .in_stock
                                 .where('spree_prices.amount > 0.3')
                                 .select('spree_products.id, spree_products.slug, spree_products.updated_at')

  products_query.find_each do |product|
    add(product_path(product), lastmod: product.updated_at)
  end

  Spree::Page.live.find_each do |page|
    add(page.slug, lastmod: page.updated_at)
  end

  Spree::Vendor.find_each do |vendor|
    add(vendor_path(vendor), lastmod: vendor.updated_at)
  end
end
