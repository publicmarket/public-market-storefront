Spree::Core::Engine.routes.draw do
  get '/top', to: 'products#best_selling', as: :top_selling
  get '/top/t/*id/', to: 'products#best_selling', as: :top_selling_taxon

  get 'merchants/apply', to: 'home#sell', as: :sell_apply
  get 'merchants/apply/confirm', to: 'home#sell_apply_confirm'
end

Spree::Core::Engine.add_routes do
  get '/early-access', to: redirect('/signup')

  namespace :admin, path: Spree.admin_path do
    post '/sales' => 'orders#sales_report', :as => :sales_report
    post '/reassign_item/:line_item_id', to: 'orders#reassign_item', as: :reassign_item

    resources :product_collections, except: :show
    resources :product_collection_products, only: :destroy
    resources :user_rewards, only: :index

    resources :leads do
      collection do
        get :export
        post 'upload', to: 'leads#upload'
      end
    end

    resources :stock_locations do
      member do
        post :reindex
      end
    end

    resources :taxons do
      member do
        post :reindex, to: 'taxons#reindex'
      end
    end
  end

  scope :account do
    get '/(:tab)', to: 'users#show', tab: /orders/, as: :account

    get '/shipping', to: 'addresses#index', as: :user_addresses
    get '/shipping/edit', to: 'addresses#new', as: :new_address

    get '/password', to: redirect('/account/edit') # fix 404 on page refresh after password change fail

    get '/payment', to: 'credit_cards#index', as: :user_payment_methods
    get '/payment/edit', to: 'credit_cards#new', as: :new_payment_method

    resources :credit_cards, except: %i[index show new]

    resources :rewards, only: %i[index]
  end

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      get '/inventory/fetch', to: 'inventory#fetch'
      get '/orders/fetch', to: 'orders#fetch'
      post '/orders/update', to: 'orders#update_items'
      put '/shipments/:id/cancel_item', to: 'shipments#cancel_item'
      put '/shipments/:id/confirm_item', to: 'shipments#confirm_item'
      post '/leads/wh', to: 'leads#wh'
    end
  end

  scope :orders do
    get '/:id/rate/:shipment_id', to: 'orders#rate_shipment', as: :rate_shipment
    post '/:id/rate/:shipment_id', to: 'orders#update_shipment_rate', as: :update_shipment_rate
  end

  scope :products do
    get '/:id/variations/(:variation)', to: 'products#similar_variants', as: :similar_variants
  end

  get '/c/:id', to: 'product_collections#show', as: :product_collections

  get '/taxons/mobile_menu_childs', to: 'taxons#mobile_menu_childs'

  get '/how-it-works', to: 'pages#how_it_works', as: :how_it_works

  post '/leads/subscribe', to: 'leads#subscribe'

  resources :addresses, except: %i[index show new]
end

Rails.application.routes.draw do
  get '/404', to: 'errors#not_found'
  get '/500', to: 'errors#internal_server_error'

  mount Spree::Core::Engine, at: '/'

  scope :freshdesk do
    get '/', to: 'freshdesk#login', as: :freshdesk
  end

  require 'sidekiq/web'
  require 'sidekiq-scheduler/web'
  authenticate :spree_user, ->(u) { u.admin? } do
    mount Sidekiq::Web, at: 'sidekiq'
    mount PgHero::Engine, at: 'pghero'
    mount Ckeditor::Engine, at: 'ckeditor'
  end

  resources :api_docs, path: 'apidocs', only: [:index] do
    collection do
      get :schema
    end
  end
end
