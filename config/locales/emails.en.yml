en:
  emails:
    view_order: VIEW YOUR ORDER
    view_order_history: VIEW ORDER HISTORY
    manage_order: VIEW/MANAGE ORDER
    contact_us: Contact Us
    quantity_shipped: QUANTITY SHIPPED
    select_rating: "Select your rating for this merchant:"
    question_about_order: "For details on our Buyer Rewards program and how it works, <a href='https://publicmarket.freshdesk.com/support/solutions/36000117831'>click here.</a>"
    address: "P.O. Box 410534 San Francisco, CA 94141 U.S.A."
    order_help: "To view, manage, or get help with your order click here:"
    strive: "We strive to keep our listings as accurate as possible, but sometimes there are mistakes. Unfortunately, this was one of them, and the merchant is unable to fill this order. Want to rate your experience with this merchant? <a href='%{rate_url}'>Click here</a>"

    unsubscribe_preferences:
      - engagement
      - buyer-feedback
      - rewards
      - vendor
    unsubscribe:
      category: Unsubscribe From This List
      preferences: Manage Email Preferences

    not_on_backorder: You will not be charged for any canceled items, and no canceled items are on backorder.

    missing_not_on_backorder: You will not be charged for any unshipped quantity, and no unshipped items are on backorder.

    welcome:
      categories:
        - engagement
      subject: Welcome to Public Market, where you earn big on every buy
      preview: Commerce should be for the people. That’s why we’ve built an open marketplace
      body: |
        <h1><strong>WELCOME TO PUBLIC MARKET</strong></h1>
        <p>
          Commerce should be <b>for the people</b>. That’s why we’ve built an open marketplace ー without middlemen taking a big cut. Thanks for joining our mission to replace the broken, monopolistic marketplaces of today with a system that is fair, open, and commission-free.
        </p>
        <p>
          By buying and selling on Public Market, you play a pivotal role in restoring the <a href="https://medium.com/public-market/liberating-ecommerce-public-market-the-new-commercial-commons-f71e176328f3" target="_blank">Commercial Commons</a>.
        </p>
        <p>
          And not only that, but you get rewarded for your efforts in the process.
        </p>

        <h4 style="margin: 8px 0 12px"><strong>WELCOME TO FAIR COMMERCE</strong></h4>

        <table class="button">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{root_url}">START EARNING REWARDS</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      footer: 'By creating an account, you agree to our <a href="https://public.market/terms" target="_blank">Terms of Service</a> and <a href="https://public.market/privacy" target="_blank">Privacy Policy</a> and subscribe to our mailing list.'

    reset_password_instructions:
      categories:
        - account
      subject: Change your account password
      preview: Change the password you use to access your account.
      body: |
        <h3><strong>RESET YOUR PASSWORD</strong></h3>

        <p>
        Hi there,
        </p>

        <p>
        Please click below to reset and change the password you use to access your account.
        </p>

        <table class="button">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{edit_password_reset_url}" class="btn">Change Password</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      footer: If you did not make this request, please <a href="mailto:team@publicmarket.io?Subject=[Support]%20I%20didn't%20request%20a%20password%20reset.">notify us</a> and change your password. Someone could be trying to meddle with your account! 💸

    confirmation:
      categories:
        - account
      subject: Verify your email address & protect your account
      preview: You’re one step away from verifying your Public Market account ー open the link to verify your email address.
      body: |
        <h3><strong>VERIFY YOUR EMAIL ADDRESS</strong></h3>
        <p>Hi there!</p>
        <p>You’re one step away from verifying your account. You'll need to have a verified account to eventually redeem any Buyer Rewards earned by shopping on Public Market.</p>
        <p>Click below to confirm your ownership over this email address and verify your account.</p>

        <table class="button">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{confirmation_url}" class="btn">Verify Email Address</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      footer: Confirming ownership over your email address helps protect your identity and prevent others from accessing your account.

    reconfirmation:
      categories:
        - account
      subject: Confirm your new email address
      preview: Please confirm you want to change the email address currently associated with your account...
      body: |
        <h3><strong>CHANGE ACCOUNT EMAIL ADDRESS</strong></h3>
        <p>
          Hi there,
        </p>
        <p>
          A request was made to change the email address associated with your account. If you made this request, please confirm you'd like "<strong>%{email}</strong>" to be the new email address you will use to access your Public Market account.
        </p>

        <table class="button">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{confirmation_url}" class="btn">Confirm Account Email Change</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      footer: If you did not make this request, please ignore this email.

    email_change:
      categories:
        - account
      subject: Your account email address changed
      body: |
        <h3><strong>NOTICE OF ACCOUNT EMAIL CHANGE</strong></h3>
        <p>Hi there,</p>
        <p>Public Market recently received a request to change the email address associated with your account.</p>
        <p>If you made this request, please disregard this notice.</p>
        <p>Best Regards,<br>The Public Market Team</p>
      footer: 'If you did not make this request, please contact us at <a href="mailto:support@publicmarket.io">support@publicmarket.io</a>.'

    order_confirmation:
      categories:
        - order
      subject:
        zero: 'Your order of %{qty}x "%{product_title}" has been placed | Order #%{order_id}'
        one: 'Your order of %{qty}x "%{product_title}" & %{count}x other item has been placed | Order #%{order_id}'
        other: 'Your order of %{qty}x "%{product_title}" & %{count}x other items has been placed | Order #%{order_id}'
      preview: Thanks for your order! We’ll let you know when your items have shipped.
      body: |
        <p>Hi %{first_name},</p>
        <br>
        <p>Thanks for your order! We’ll let you know when your items have shipped.</p>
        <br>
        %{order_card}
        %{order_info}
      footer: ''

    vendor_order:
      categories:
        - vendor
      subject:
        zero: 'A Public Market order of %{qty}x "%{product_title}" has been placed | Order #%{order_id}'
        one: 'A Public Market order of %{qty}x "%{product_title}" & %{count}x other item has been placed | Order #%{order_id}'
        other: 'A Public Market order of %{qty}x "%{product_title}" & %{count}x other items has been placed | Order #%{order_id}'
      preview: 'You have a new order! Your new orders will remain in the “unshipped” state until you hit confirm
        your shipments via your Merchant Dashboard.'
      body: |
        <p>Hi %{vendor_name},</p>
        <p>You have a new order! Your new orders will remain in the “unshipped” state until you hit confirm
        your shipments via your Merchant Dashboard.</p>
        <p><span class='text-semibold'>Hint:</span> make sure you input the tracking number before confirming your orders as shipped so the
        customers can track their package.</p>
        <br>
        <h3 class='text-bold'>ORDER #%{order_id}</h3>
        %{vendor_order_card}
        <hr>
        <p>To view, manage, or get help with your order click here: <a href='%{order_url}'>Order #%{order_id}</a></p>
      footer: ''

    shipment_shipped:
      categories:
        - shipment
      subject:
        zero: 'Your order for %{qty}x "%{product_title}" has shipped | Order #%{order_id}'
        one: 'Your order for %{qty}x "%{product_title}" & %{count}x other item has shipped | Order #%{order_id}'
        other: 'Your order for %{qty}x "%{product_title}" & %{count}x other items have shipped | Order #%{order_id}'
      preview: Good news! These items have shipped from %{vendor_name}
      body: |
        <p>Hi %{first_name},</p>
        <br>
        <p>Good news!</p>
        <br>
        <p>These items have shipped from <span class='text-semibold'>%{vendor_name}</span>:</p>
        %{shipped_items}
        %{shipment_tracking}
        %{shipment_rewards}
        %{order_info}
      footer: ''

    shipment_partially_shipped:
      categories:
        - shipment
      subject:
        zero: 'Notice: Your order for %{qty}x "%{product_title}" could only be partially filled | Order #%{order_id}'
        one: 'Notice: Your order for %{qty}x "%{product_title}" & others could only be partially filled | Order #%{order_id}'
        other: 'Your order for %{qty}x "%{product_title}" & others could only be partially filled | Order #%{order_id}'
      preview: "We’re sorry, but your order could only be partially filled by %{vendor_name}."
      body: |
        <p>Hi %{first_name}</p>
        <p>We’re sorry, but your order #%{order_id} could only be partially filled by <span class='text-semibold'>%{vendor_name}</span>.</p>
        %{shipped_items}
        %{shipment_tracking}
        %{shipment_rewards}
        %{canceled_items}
        %{strive}
        %{order_info}
      footer: ''

    shipment_cancel:
      categories:
        - shipment
      subject:
        zero: 'Notice: Your order for %{qty}x "%{product_title}" has been canceled | Order #%{order_id}'
        one: 'Notice: Your order for %{qty}x "%{product_title}" & %{count}x other item has been canceled | Order #%{order_id}'
        other: 'Notice: Your order for %{qty}x "%{product_title}" & %{count}x other items have been canceled | Order #%{order_id}'
      preview: "We are very sorry, but your order could not be filled by %{vendor_name}"
      body: |
        <p>Hi %{first_name},</p>
        <p>We are very sorry, but your order could not be filled by <span class='text-semibold'>%{vendor_name}</span>.</p>
        %{canceled_items}
        <hr>
        %{strive}
        %{order_info}
      footer: ''

    shipment_rate:
      categories:
        - buyer-feedback
      subject: "Please rate your experience with %{vendor_name}"
      preview: How was your experience with %{vendor_name}?
      body: |
        <p>Hi %{first_name},</p>
        <br>
        <p class='text-center'>How was your experience with<br><span class='text-semibold'>%{vendor_name}?</span></p>
        %{shipment_rate}
        <h4>SHIPMENT DETAILS</h4>
        %{shipped_items}
        %{canceled_items}
        %{shipment_rewards}
        %{order_info}
      footer: ''

    vendor_sales_report_daily:
      categories:
        - account
      subject: "%{vendor_name}, here's your daily Sales Report from Public Market"
      preview: "Find your daily Sales Report from Market attached to this email..."
      body: |
        <h3 class='upcase'>DAILY SALES REPORT</h3>
        <p>Hi there, <strong>%{vendor_name}</strong>.</p>
        <p>Please find your Public Market Sales Report, updated daily, attached to this email.</p>
      footer: "Please contact us at <a href='mailto:merchants@publicmarket.io?Subject=Inquiry about Sales Reporting'>merchants@publicmarket.io</a> if you have any questions."

    lead_whitelisted:
      categories:
        - account
      subject: "You’re approved! Early Access to our Beta Storefront"
      preview: "Congratulations — you got early access to our Beta! You’ve been approved to create your beta account on our Public Market Storefront."
      body: |
        <h2 class='upcase'>APPROVED! EARLY ACCESS TO OUR BETA</h2>
        <p>Congratulations! You’ve been approved for an Early Access Account to the Public Market Storefront.</p>
        <p>To create your beta account, just click the big button below and use your whitelisted email address: <b>%{whitelisted_email}</b></p>
        <p>Before you get started, here are <b>three things</b> we’d like you to know:</p>

        <table class="button secondary hide-for-large">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{access_url}" class="btn">GET EARLY ACCESS NOW</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <h5><i>1) The Storefront is still in its infancy</i></h5>
        <p>This Beta Storefront is fully operational and allows you to purchase over 5 million books from multiple merchants.
        That said, over the coming weeks and months, dozens more categories and thousands of additional items will be available to purchase.</p>

        <h5><i>2) It's the first of many built on the Public Market eCommerce Protocol</i></h5>
        <p>This Storefront is a proof of concept that shows what a storefront on the Public Market Protocol can do. Ultimately, the Protocol is designed to enable anyone,
        anywhere, to easily and quickly set up their own online store and sell from a shared inventory set. As such, this flagship storefront is only one of thousands to come
        and is meant to help future storefront creators both see what is possible as well as take advantage of the items from new merchants we bring online, which those future
        storefronts will be able to curate and select from to sell as part of our open inventory.</p>

        <h5><i>3) Our mission is fair, transparent, and distributed eCommerce for everyone</i></h5>
        <p>By <a href='https://medium.com/public-market/liberating-ecommerce-public-market-the-new-commercial-commons-f71e176328f3'>
        <span class='text-semibold'>building the new Commercial Commons</span></a>, Public Market is liberating eCommerce from dangerous monopolies that harm buyers and sellers alike.
        To learn more about what we’re building, and the ramifications of a truly open, portable, and transparent eCommerce protocol, we encourage you to read our Medium post:
        <a href='https://medium.com/public-market/announcing-the-beta-of-public-markets-flagship-storefront-7243834cf141'>
        <span class='text-semibold'>Announcing the Public Market Flagship Beta Storefront</span></a>.</p>

        <table class="button">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{access_url}" class="btn">CREATE BETA ACCOUNT</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <p>Lastly, I know you’ll have product feedback, and maybe even find a bug or two. When you do, there are a couple of ways to leave feedback inside the product,
        or feel free to simply send me an email.</p>
        <p>Welcome to Public Market! <br>Landon Howell</p>
      footer: "PRIVACY NOTICE: Since this is a private launch, we ask all beta participants to not publicly share any product information (beta screenshots, storefront products, etc). Thank you."

    daily_rewards_update:
      categories:
        - rewards
      subject: 'You earned %{daily_rewards_total} in Holiday Bonus Rewards yesterday!'
      preview: ''
      body: |
        <p>Based on yesterday’s activity on the Public Market Storefront, you earned:</p>
        %{daily_rewards}
        <br>
        <p>Your Daily Holiday Bonus Rewards are a result of purchases you and the people you referred
        made on our Storefront yesterday.</p>
        <p><span class='text-semibold'>From Nov 26 - Dec 15, we’ll be giving away $100,000 in Holiday Bonus Rewards.*</span><br>You earn bonus rewards by purchasing or referring friends who purchase. To view your total Buyer Rewards balance earned to date, click below: </p>

        <table class="button cryptoberry center">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="%{rewards_url}" class="btn">VIEW REWARDS BALANCE</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <hr>

        <h4 class='text-center'>WANT MORE REWARDS LIKE THESE?</h4>
        <p class='text-center'>
          Supercharge your Holiday Bonus Rewards by referring friends to purchase on Public Market.
          <span class='text-semibold'>We'll count 30% of their purchases each day</span> toward your Holiday Bonus Rewards calculations.
          To get your unique referral link, <a href='https://public.market/account'>visit your account</a> page and have your friends sign up through
          that link. But hurry — the promotion only last through December 15th!
        </p>
        <table class="button secondary center">
          <tr>
            <td>
              <table>
                <tr>
                  <td><a href="https://public.market/account" class="btn">GET YOUR REFERRAL LINK</a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <hr>

        <p>Your Buyer Rewards credits will be redeemable for the Patron (PTRN) token upon the launch of the token,
        anticipated in mid-2019.* </p>
        <p>So long as the items purchased by you or those you referred are shipped and paid for (and not returned
        to the merchant), you’ll keep all the Daily Holiday Bonus Rewards you earned on your purchases made today,
        %{date}.</p>

        <br>
        %{orders}
      footer: "Want details on our Bonus Holiday Rewards program works? <a href='https://public.market/how-it-works#holiday-bonus-rewards'>Learn more</a>"
      bottom: |
        <p style='font-size:10px;line-height:normal'>
        *Buyers will receive rewards credits (Rewards) to their rewards account as set forth in seller’s
        listing. In the near future, Public Market plans to launch the Patron token (PTRN), a blockchain-based
        rewards token. PTRN will exist on the Ethereum blockchain as a store of value and will contribute
        toward unlocking benefits in our upcoming membership program. Once the PTRN tokens have been made
        available to the public, your Rewards may be redeemed, at the option of the company, only for PTRN
        tokens at the then-market value. The PTRN token may then be deposited to your crypto wallet (see
        Rewards Terms for process). If PTRN tokens are not made available to the public by March 31, 2020,
        you may request redemption of the amount of Rewards in your Rewards account, and Company will deliver
        the Rewards to you in the form of, in its discretion, either (a) USD; or (b) an equivalent credit
        (e.g., Public Market store credit or other value). If the PTRN token is made available to the public,
        rewards are not redeemable in USD or an equivalent credit. Rewards are available only to US-based
        buyers using a US shipping and billing address. See <a href='https://public.market/how-it-works'>How It Works</a>,
        <a href='https://publicmarket.freshdesk.com/support/solutions/36000117831'>FAQs</a> and
        <a href='/rewards-terms'>Rewards Terms</a> for more information.</p>