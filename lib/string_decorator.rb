class String
  # https://stackoverflow.com/a/17859803
  def smart_titleize(options = {})
    exclusions = options[:exclude] || %w[a an and as at but by en for if in of on or the to v v. via vs vs.]
    regexp = /\b(?<!['’`])(?!(#{exclusions.join('|')})(?!$)\b)[a-z]/
    strip.humanize.gsub(regexp) { $&.capitalize }
  end
end
