module Spree
  class UserMailerPreview < ActionMailer::Preview
    def reset_password_instructions
      UserMailer.reset_password_instructions(User.first, 'token')
    end

    def welcome
      UserMailer.welcome(User.first.id)
    end

    def confirmation_instructions
      user = User.where(unconfirmed_email: nil).first
      UserMailer.confirmation_instructions(user, 'faketoken')
    end

    def reconfirmation_instructions
      user = User.first
      user.unconfirmed_email = 'newemail@public.market'
      UserMailer.confirmation_instructions(user, 'faketoken')
    end

    def email_change
      UserMailer.email_change(User.first.id, 'test@test.test')
    end

    def lead_whitelisted
      lead = Lead.last || Lead.new(email: 'test@email.com')
      UserMailer.lead_whitelisted(lead.id)
    end

    def daily_rewards_update
      reward = UserReward.last
      if reward
        UserMailer.daily_rewards_update(reward.user_id, reward.date.to_s)
      else
        UserMailer.preview_error_email('Please create daily user reward')
      end
    end
  end
end
