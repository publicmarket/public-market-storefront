module Spree
  class OrderMailerPreview < ActionMailer::Preview
    def order_placed_email
      OrderMailer.confirm_email(Order.complete.where(shipment_state: %i[pending ready]).last)
    end

    def vendor_order
      order = Order.complete.last
      OrderMailer.vendor_order(order, order.vendors.first)
    end

    def sales_report_daily
      OrderMailer.sales_report(Vendor.first, 'Test CSV')
    end
  end
end
