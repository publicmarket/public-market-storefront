module Spree
  class ShipmentMailerPreview < ActionMailer::Preview
    def shipped_email
      ShipmentMailer.shipped_email(Shipment.where(tracking: nil).shipped.first)
    end

    def shipped_email_with_tracking
      ShipmentMailer.shipped_email(Shipment.where.not(tracking: nil).shipped.first)
    end

    def some_shipped_some_canceled
      shipment = Shipment.shipped.joins(inventory_units: :line_item).where(spree_line_items: { quantity: 0 }).first
      if shipment
        ShipmentMailer.shipped_email(shipment)
      else
        ShipmentMailer.preview_error_email('Please create shipment with canceled items to preview')
      end
    end

    def partial_qty
      line_item = LineItem.positive.joins(:inventory_units).merge(InventoryUnit.canceled).last
      if line_item
        ShipmentMailer.shipped_email(line_item.inventory_units.first.shipment)
      else
        ShipmentMailer.preview_error_email('Please create shipment with partial QTY to preview')
      end
    end

    def partial_qty_with_canceled_items
      shipment = Shipment.joins(inventory_units: :line_item).where('spree_line_items.quantity > 0')
                         .merge(InventoryUnit.canceled)
                         .where(spree_shipments: { id: Shipment.joins(inventory_units: :line_item).where(spree_line_items: { quantity: 0 }) })
                         .first
      if shipment
        ShipmentMailer.shipped_email(shipment)
      else
        ShipmentMailer.preview_error_email('Please create shipment with partial fulfillment and canceled items to preview')
      end
    end

    def cancel_email
      ShipmentMailer.cancel_email(Shipment.with_state('canceled').last)
    end

    def rate_email
      ShipmentMailer.rate_email(Shipment.shipped.last)
    end

    def rate_some_shipped_some_canceled
      shipment = Shipment.shipped.joins(inventory_units: :line_item).where(spree_line_items: { quantity: 0 }).last
      if shipment
        ShipmentMailer.rate_email(shipment)
      else
        ShipmentMailer.preview_error_email('Please create shipment with canceled items to preview')
      end
    end
  end
end
