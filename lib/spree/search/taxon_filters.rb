module Spree
  module Search
    module TaxonFilters
      module_function

      def applicable_filters(taxon)
        return [] if (taxonomy = taxon.taxonomy).blank?

        filters(taxonomy)
      end

      def filters(taxonomy)
        filters = []

        if (variation_options = variation_filter(taxonomy)).present?
          filters << {
            name: 'Format',
            type: :variations,
            options: variation_options
          }
        end

        filters
      end

      def variation_filter(taxonomy)
        taxonomy.filterable_variations.map do |f|
          { label: f.titleize, value: f, id: f.parameterize }
        end
      end
    end
  end
end
