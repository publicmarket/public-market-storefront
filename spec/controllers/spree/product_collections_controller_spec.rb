RSpec.describe Spree::ProductCollectionsController, type: :controller do
  let(:product_collection) { create(:product_collection) }

  routes { Spree::Core::Engine.routes }

  describe '#show' do
    subject(:visit_product_collection) { get :show, params: { id: product_collection.slug } }

    describe '#set_influencer_cookie' do
      let(:product_collection) { create(:product_collection, influencer_slug: 'influencer-super-slug') }

      before { visit_product_collection }

      context 'when visiting with slug' do
        it 'does not set cookie' do
          expect(cookies['_inf_src']).to be_nil
        end
      end

      context 'when visiting with influencer slug' do
        subject(:visit_product_collection) { get :show, params: { id: product_collection.influencer_slug } }

        it 'sets cookie' do
          expect(cookies['_inf_src']).to eq 'influencer-super-slug'
        end

        context 'when product collection is assigned to user' do
          let(:user) { create(:user) }
          let(:product_collection) { create(:product_collection, influencer_slug: 'influencer-super-slug', user: user) }

          it 'sets refuid cookie' do
            expect(cookies['refuid']).to eq user.hash_id
          end
        end
      end
    end
  end
end
