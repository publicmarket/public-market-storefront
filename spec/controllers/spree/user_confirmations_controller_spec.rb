RSpec.describe Spree::UserConfirmationsController, type: :controller do
  let(:user) { create(:user, confirmed_at: nil) }

  routes { Spree::Core::Engine.routes }

  before do
    @request.env['devise.mapping'] = Devise.mappings[:spree_user] # rubocop:disable RSpec/InstanceVariable
  end

  describe '#create' do
    before { sign_in(user) }

    it 'shows notice message' do
      post :create

      expect(flash[:info]).to eq(I18n.t('devise.confirmations.send_instructions', email: user.email))
    end
  end
end
