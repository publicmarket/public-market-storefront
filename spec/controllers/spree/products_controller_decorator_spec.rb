RSpec.describe Spree::ProductsController, type: :controller do
  routes { Spree::Core::Engine.routes }

  describe '#top' do
    subject { get :best_selling }

    context 'without taxon' do
      it { is_expected.to be_successful }
    end

    context 'with taxon' do
      subject { get :best_selling, params: { id: taxon_id } }

      let(:taxon_id) { create(:taxon).id }

      it { is_expected.to be_successful }
    end
  end
end
