FactoryBot.define do
  factory :product_collection, class: Spree::ProductCollection do
    name { FFaker::Book.title }
  end
end
