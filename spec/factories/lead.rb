FactoryBot.define do
  factory :lead, class: Spree::Lead do
    email { generate(:random_email) }
  end
end
