RSpec.describe Spree::HomepageHelper, type: :helper do
  describe '#fetch_home_top_selling_products' do
    subject(:products) { fetch_home_top_selling_products(taxon) }

    let(:taxon) { nil }

    it { is_expected.to eq [] }

    context 'when taxon is given', search: true do
      let(:taxon) { create(:taxon) }

      context 'when no products assigned' do
        it { is_expected.to eq [] }
      end

      context 'when product is assigned' do
        let!(:product) { create(:product, :searchable, taxons: [taxon]) }

        it { is_expected.to eq [product] }

        context 'when taxon is hidden' do
          let(:taxon) { create(:taxon, hidden: true) }

          it { is_expected.to eq [] }
        end

        context 'when parent taxon is hidden' do
          let(:taxon) { create(:taxon, parent: create(:taxon, hidden: true)) }

          it { is_expected.to eq [] }
        end
      end
    end
  end
end
