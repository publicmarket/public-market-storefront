RSpec.describe Spree::Inventory::AmzCsvImportAction, type: :action do
  subject(:call) { described_class.call(local_file, upload: upload) }

  let(:local_file) { File.join(Dir.pwd, 'spec/fixtures/files', 'amz.csv') }
  let(:upload) { create(:upload, metadata: { format: 'amz_csv', file_path: local_file, product_type: 'generic' }) }

  context 'when file is absent' do
    let(:local_file) { File.join(Dir.pwd, 'spec/fixtures', 'unknown.csv') }

    it { expect { call }.to raise_error(Spree::ImportError, 'AMZ CSV file is invalid') }
  end

  context 'when file is correct' do
    it 'creates product' do
      expect {
        expect {
          call
          upload.reload
        }.to change(upload, :total).to(1)
      }.to change(upload, :processed).to(1)

      product = Spree::Product.last
      expect(product.description).to include('Point 1', 'Point 2')

      expect(product.master.sku).to eq 'upc-1234567890'
      expect(product.variants.first.sku).to eq 'TOP-PRODUCT-SKU-1234'
    end

    context 'when AMZ hint is missing from file header' do
      let(:local_file) { File.join(Dir.pwd, 'spec/fixtures/files', 'amz_no_head.csv') }

      it 'creates product' do
        expect {
          call
        }.to change(Spree::Product, :count).to(1)
      end
    end

    context 'when product identifier is missing' do
      let(:local_file) { File.join(Dir.pwd, 'spec/fixtures/files', 'amz_no_prod_id.csv') }

      before { call }

      it 'sets sku as product id' do
        product = Spree::Product.last
        expect(product.master.sku).to eq "sku-#{product.variants.first.sku.downcase}"
      end
    end
  end

  context 'when file contains accented chars' do
    let(:local_file) { File.join(Dir.pwd, 'spec/fixtures/files', 'amz_accented.csv') }

    before { call }

    it 'correctly saves data from file' do
      product = Spree::Product.last
      expect(product.name).to eq 'Outdoor Décor'
    end
  end
end
