RSpec.describe Spree::Inventory::Searchers::TaxonSearcher, type: :action, search: true do
  subject(:search) { described_class.call(root_taxon) }

  let(:root_taxon) { create(:taxonomy).root }
  let(:child_taxon) { create(:taxon, parent: root_taxon) }

  context 'when child taxons have products' do
    let(:price) { 1 }
    let!(:product) { create(:book, :searchable, price: price, taxons: [child_taxon]) }

    it 'returns child taxons' do
      expect(search).to eq product.taxons
    end

    context 'when products are not searchable' do
      let(:price) { 0.1 }

      it 'does not return taxons' do
        expect(search).to be_blank
      end
    end
  end

  context 'when child taxon have no products' do
    it { is_expected.to eq [] }
  end
end
