RSpec.describe Spree::Inventory::Searchers::SearchTaxonPreviews, type: :action, search: true do
  subject(:search) { described_class.call(root_taxon) }

  let(:root_taxon) { create(:taxonomy).root }
  let(:child_taxon) { create(:taxon, parent: root_taxon) }

  context 'when child taxons have products' do
    let!(:book) { create(:book, :searchable, taxons: [child_taxon]) }

    it 'includes taxon preview' do
      taxon_preview = search.first
      expect(taxon_preview[:taxon]).to eq child_taxon
      expect(taxon_preview[:products].to_a).to eq [book]
    end
  end

  context 'with hidden taxons' do
    let(:hidden_taxon) { create(:taxon, hidden: true, parent: root_taxon) }

    before do
      create(:book, :searchable, taxons: [child_taxon])
      create(:book, :searchable, taxons: [hidden_taxon])
    end

    it 'excludes hidden taxon preview' do
      expect(search.size).to eq 1
      expect(search.first[:taxon]).to eq child_taxon
    end
  end

  context 'when child taxon has no products' do
    it { is_expected.to eq [] }
  end
end
