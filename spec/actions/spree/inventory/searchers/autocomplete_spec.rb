RSpec.describe Spree::Inventory::Searchers::Autocomplete, type: :action, search: true do
  subject(:search) { described_class.call(keywords: 'cheese').results }

  let(:keyword) { 'cheese' }
  let!(:book) { create(:book, :searchable, name: keyword) }

  context 'when product with given keyword found' do
    it { is_expected.to eq [book] }
  end

  context 'when keyword provided with misspellings' do
    let(:keyword) { 'chess' }

    it { is_expected.to eq [book] }
  end

  context 'when product with given keyword found' do
    let(:keyword) { 'chessters' }

    it { is_expected.to eq [] }
  end
end
