RSpec.describe Spree::Inventory::Searchers::MinPrice, type: :action, search: true do
  subject(:search) { described_class.call }

  let(:cheap_price) { 1 }

  before do
    create(:book, :searchable, price: cheap_price)
  end

  it { is_expected.to eq cheap_price }

  context 'when other prices exists' do
    let(:cheap_price) { 0.5 }

    before do
      [3, 1, 10].each do |price|
        create(:book, :searchable, price: price)
      end
    end

    it { is_expected.to eq cheap_price }
  end
end
