RSpec.describe Spree::Orders::SalesReportAction, type: :action do
  subject(:csv) { described_class.new(vendor, date).call }

  let(:order) { create(:shipped_vendor_order, line_items_count: 2) }
  let(:line_item) { order.line_items.first }
  let(:payment) { order.payments.first }
  let(:other_line_item) { order.line_items.second }
  let(:vendor) { line_item.variant.vendor }
  let(:date) { Time.current }

  before { payment.send(:create_transfers) }

  it { expect(csv).to match(order.number) }
  it { expect(csv).to match(line_item.price.to_s) }
  it { expect(csv).to match(order.ship_address.address1) }
  it { expect(csv).to match(order.ship_address.city) }
  it { expect(csv).to match(order.ship_address.first_name) }

  describe 'admin report' do
    let(:vendor) { nil }

    it { expect(csv).to match(order.number) }
    it { expect(csv).to match(order.vendors.first.name) }
  end
end
