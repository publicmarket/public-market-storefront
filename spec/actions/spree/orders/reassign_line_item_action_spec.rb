RSpec.describe Spree::Orders::ReassignLineItemAction, type: :action do
  subject(:shipment) { described_class.new(line_item, pm_vendor).call }

  let(:order) { create(:completed_vendor_order, line_items_count: count) }
  let(:line_item) { order.line_items.first }
  let(:vendor) { line_item.variant.vendor }
  let(:pm_vendor) { create(:vendor, name: 'PM') }

  context 'when multiple line items' do
    let(:count) { 2 }

    before { shipment }

    it { expect(order.shipments.count).to eq(2) }
    it { expect(line_item.variant.vendor).to eq(pm_vendor) }
    it { expect(line_item.inventory_units.first.shipment).to eq(shipment) }
    it { expect(line_item.inventory_units.first.variant.vendor).to eq(pm_vendor) }
  end

  context 'when one line item' do
    let(:count) { 1 }

    before { shipment }

    it { expect(order.shipments.first).to eq(shipment) }
    it { expect(order.shipments.count).to eq(1) }
    it { expect(line_item.variant.vendor).to eq(pm_vendor) }
    it { expect(line_item.inventory_units.first.shipment).to eq(shipment) }
    it { expect(line_item.inventory_units.first.variant.vendor).to eq(pm_vendor) }
  end
end
