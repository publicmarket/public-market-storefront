RSpec.describe Spree::Leads::Upload, type: :action, vcr: true, enqueue: true do
  subject(:call) { described_class.call(file, source: source) }

  let(:source) { '' }

  let(:file) { File.new(File.join(Dir.pwd, 'spec/fixtures/files', 'leads.csv')) }

  let(:lead) { Spree::Lead.last }

  it 'saves leads' do
    expect {
      call
    }.to change { Spree::Lead.count }.by(1)

    expect(lead.email).to eq 'user@user.com'
    expect(lead.source_id).to eq 'id001'
    expect(lead.source_parent_id).to eq 'id100'
    expect(lead.source_created_at).not_to be nil
  end
end
