RSpec.describe Spree::Rewards::GenerateDailyRewardsAction, type: :action, enqueue: true, vcr: true do
  subject(:call) { described_class.new(payment.updated_at).call }

  let(:order) { create :order_with_vendor_items, vendor_accounts: true }
  let(:user) { order.user }
  let(:payment_method) { create(:stripe_card_payment_method) }
  let(:payment) { create(:payment, payment_method: payment_method, amount: order.total, response_code: nil, order: order) }
  let(:reward) { user.user_rewards.first }

  context 'when date is 30 Nov' do
    before do
      travel_to(Date.new(2018, 11, 30))
      payment.order.process_payments!
    end

    context 'when payment is not captured' do
      before { call }

      it { expect(reward).to be_truthy }
    end

    context 'when payment is captured' do
      before { payment.reload.capture! }

      context 'when user has no referrers' do
        before { call }

        it 'take whole pool' do
          expect(reward).to be_truthy
          expect(reward.amount).to eq(5000)
          expect(reward.referees_amount).to eq(0)
          expect(reward.paid_amount).to eq(10)
        end
      end

      context 'when user has referrer' do
        let(:referrer) { create(:user) }

        before do
          user.update(referrer: referrer)
          call
        end

        it 'take divide pool' do
          expect(reward).to be_truthy
          expect(reward.amount).to eq(3846.15)
          expect(reward.referees_amount).to eq(0)
          expect(referrer.user_rewards.count).to eq(1)
          expect(referrer.user_rewards.first.amount).to eq(1153.85)
          expect(referrer.user_rewards.first.referees_amount).to eq(1153.85)
        end

        context 'when referee gives more than 1000' do
          before do
            payment.update(amount: amount)
            described_class.new(payment.updated_at).call
          end

          context 'when amount is 900' do
            let(:amount) { 900 }

            it { expect(referrer.user_rewards.first.referees_amount).to eq(1153.85) }
          end

          context 'when amount is 1000' do
            let(:amount) { 1000 }

            it { expect(referrer.user_rewards.first.referees_amount).to eq(1153.85) }
          end

          context 'when amount is 1500' do
            let(:amount) { 1500 }

            it { expect(referrer.user_rewards.first.referees_amount).to eq(833.33) }
          end
        end
      end

      context 'when multiple users' do
        let(:other_order) { create :order_with_vendor_items, vendor_accounts: true }
        let(:other_user) { other_order.user }
        let(:other_reward) { other_user.user_rewards.first }

        before do
          other_payment = create(:payment, payment_method: payment_method, amount: other_order.total, response_code: nil, order: other_order)
          other_payment.order.process_payments!
          other_payment.reload.capture!
        end

        context 'when no referees' do
          before { call }

          it 'take divide pool' do
            expect(reward).to be_truthy
            expect(reward.amount).to eq(2500)
            expect(reward.referees_amount).to eq(0)
            expect(other_reward).to be_truthy
            expect(other_reward.amount).to eq(2500)
            expect(other_reward.referees_amount).to eq(0)
          end

          it 'estimate' do
            expect(user.estimate_daily_rewards(5.0)).to eq(24.88)
            expect(user.estimate_daily_rewards(10.0)).to eq(49.50)
          end
        end

        context 'when has referees' do
          before do
            user.update(referrer: other_user)
            other_user.update(referrer: user)

            call
          end

          it 'take divide pool' do
            expect(reward).to be_truthy
            expect(reward.amount).to eq(2500)
            expect(reward.referees_amount).to eq(BigDecimal('576.92'))
            expect(other_reward).to be_truthy
            expect(other_reward.amount).to eq(2500)
            expect(other_reward.referees_amount).to eq(BigDecimal('576.92'))
          end
        end

        context 'when called twice' do
          before { call }

          it { expect { described_class.new(payment.updated_at).call }.not_to raise_error }
        end
      end
    end
  end

  context 'when date is 3 Dec' do
    before do
      travel_to(Date.new(2018, 12, 3))
      payment.order.process_payments!
    end

    context 'when payment is captured' do
      before { payment.reload.capture! }

      context 'when user has no referrers' do
        before { call }

        it 'take whole pool' do
          expect(reward).to be_truthy
          expect(reward.amount).to eq(1500)
          expect(reward.referees_amount).to eq(0)
          expect(reward.paid_amount).to eq(10)
        end
      end

      context 'when user has referrer' do
        let(:referrer) { create(:user) }

        before do
          user.update(referrer: referrer)
          call
        end

        it 'take divide pool' do
          expect(reward).to be_truthy
          expect(reward.amount).to eq(1153.85)
          expect(reward.referees_amount).to eq(0)
          expect(referrer.user_rewards.count).to eq(1)
          expect(referrer.user_rewards.first.amount).to eq(346.15)
          expect(referrer.user_rewards.first.referees_amount).to eq(346.15)
        end
      end
    end
  end
end
