require 'spec_helper'

RSpec.describe 'Update line items', type: :request, enqueue: true do
  subject(:update) do
    post '/api/v1/orders/update', params: { token: token, updates: updates }
    response
  end

  let(:json) { JSON.parse(update.body, symbolize_names: true) }
  let(:token) { '' }
  let(:updates) { [] }

  context 'when user is unauthorized' do
    it { is_expected.to have_http_status(:unauthorized) }
    it { expect(update.body).to match('You must specify an API key') }
  end

  context 'when user is authorized' do
    let(:user) { create :user, spree_api_key: 'secure', vendors: [vendor] }
    let(:vendor) { order.vendors.first }
    let(:token) { user.spree_api_key }
    let(:order) { create(:vendor_order_ready_to_ship) }
    let(:unit) { order.inventory_units.first }

    context 'when unit is shipped' do
      let(:updates) { [{ order_number: order.number, item_number: unit.hash_id, courier: 'fedex', action: :confirm }] }

      it { is_expected.to have_http_status(:ok) }
      it { expect(json).to include(result: { unit.hash_id.to_s.to_sym => 'shipped' }) }
      it { expect { update }.to change { unit.reload.state }.from('on_hand').to('shipped') }
      it { expect { update }.to change { unit.shipment.reload.courier }.to('fedex') }
    end

    context 'when unit is canceled' do
      let(:updates) { [{ order_number: order.number, item_number: unit.hash_id, action: :cancel }] }

      it { is_expected.to have_http_status(:ok) }
      it { expect { update }.not_to change { unit.reload.state }.from('on_hand') }
      it { expect { update }.to change { PublicMarket::Workers::Slack::NotifierWorker.jobs.count }.by(1) }
      # it { expect(json).to include(result: { unit.hash_id.to_s.to_sym => 'canceled' }) }
      # it { expect { update }.to change { unit.reload.state }.from('on_hand').to('canceled') }

      context 'when it has shipped item before' do
        let(:order) { create(:vendor_order_ready_to_ship, line_items_quantity: 2) }
        let(:other_unit) { order.inventory_units.where.not(id: unit.id).first }

        before { other_unit.ship! }

        # it { expect { update }.to change { unit.shipment.reload.state }.from('ready').to('shipped') }
        it { expect { update }.not_to change { unit.reload.state }.from('on_hand') }
        it { expect { update }.to change { PublicMarket::Workers::Slack::NotifierWorker.jobs.count }.by(1) }
      end
    end

    context 'when order is shipped' do
      let(:updates) do
        [{ order_number: order.number, item_number: unit.hash_id, action: :confirm, tracking_number: '12313', shipped_at: shipped_at }]
      end

      let(:shipped_at) { 2.months.ago.to_i }

      it { is_expected.to have_http_status(:ok) }
      it { expect(json).to include(result: { unit.hash_id.to_s.to_sym => 'shipped' }) }
      it { expect { update }.to change { unit.reload.state }.from('on_hand').to('shipped') }
      it { expect { update }.to change { unit.shipment.reload.state }.from('ready').to('shipped') }
      it { expect { update }.to change { unit.shipment.reload.shipped_at.to_i }.from(0).to(shipped_at) }

      context 'when one item is canceled' do
        let(:order) { create(:vendor_order_ready_to_ship, line_items_quantity: 2) }
        let(:other_unit) { order.inventory_units.where.not(id: unit.id).first }

        before { other_unit.cancel! }

        it { is_expected.to have_http_status(:ok) }
      end
    end

    context 'when sku is passed' do
      let(:order) { create(:vendor_order_ready_to_ship, line_items_quantity: 2) }
      let(:updates) { [{ order_number: order.number, sku: unit.variant.sku, action: :confirm }] }
      let(:other_unit) { order.inventory_units.where.not(id: unit.id).first }

      it { is_expected.to have_http_status(:ok) }
      it { expect(json).to include(result: { unit.variant.sku.to_sym => 'shipped' }) }
      it { expect { update }.to change { unit.reload.state }.from('on_hand').to('shipped') }
      it { expect { update }.to change { other_unit.reload.state }.from('on_hand').to('shipped') }
    end

    describe 'shipment split' do
      let(:order) { create(:vendor_order_ready_to_ship, line_items_count: count, line_items_quantity: quantity) }
      let(:count) { 1 }
      let(:quantity) { 2 }

      context 'when items with same tracking number' do
        let(:updates) do
          order.inventory_units.map { |unit|
            {
              order_number: order.number, item_number: unit.hash_id, action: :confirm,
              tracking_number: '12313', shipped_at: Time.current
            }
          }
        end

        it { expect { update }.not_to change { order.reload.shipments.count } }
      end

      context 'when items with different tracking numbers' do
        context 'when update by unit id' do
          let(:updates) do
            order.inventory_units.map { |unit|
              {
                order_number: order.number, item_number: unit.hash_id, action: :confirm,
                tracking_number: unit.hash_id, shipped_at: Time.current
              }
            }
          end

          before { update }

          it { expect(order.shipments.count).to eq(2) }
          it { expect(order.shipments.map(&:shipped_at)).to all(be_truthy) }
          it { expect(order.shipments.map(&:state)).to all(eq('shipped')) }
          it { expect(order.shipments.map(&:tracking)).to all(be_truthy) }
          it { expect(order.shipments.first.tracking).not_to eq(order.shipments.last.tracking) }
        end

        context 'when update by sku' do
          let(:count) { 2 }
          let(:updates) do
            order.line_items.map { |item|
              {
                order_number: order.number, sku: item.variant.sku, action: :confirm,
                tracking_number: item.id
              }
            }
          end

          before { update }

          it { expect(order.shipments.count).to eq(2) }
          it { expect(order.shipments.map(&:shipped_at)).to all(be_truthy) }
          it { expect(order.shipments.map(&:state)).to all(eq('shipped')) }
          it { expect(order.shipments.map(&:tracking)).to all(be_truthy) }
          it { expect(order.shipments.first.tracking).not_to eq(order.shipments.last.tracking) }
        end
      end
    end
  end
end
