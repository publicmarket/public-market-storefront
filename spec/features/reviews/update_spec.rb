require 'contexts/review_form'

RSpec.describe 'update review', type: :feature, js: true do
  include_context 'when submit review form', create_review: true

  before do
    within('#product-review-modal') do
      expect(page).to have_field('review_title', with: review.title)
      expect(page).to have_field('review_review', with: review.review)
    end
  end

  context 'with valid input' do
    it 'saves review info' do
      expect {
        expect {
          expect {
            submit_review

            expect(page).to have_text Spree.t(:review_successfully_updated)
          }.to change { review.reload.title }.to(review_title)
        }.to change(review, :review).to(review_comment)
      }.to change(review, :rating).to(2)
    end
  end

  context 'with invalid input' do
    context 'with invalid title' do
      let(:review_title) { '   ' }

      it 'does not save review' do
        expect {
          submit_review

          expect(page).to have_text "Review title can't be blank"
        }.not_to change(review, :title)
      end
    end
  end
end
