require 'contexts/review_form'

RSpec.describe 'create review', type: :feature, js: true do
  include_context 'when submit review form'

  context 'with valid input' do
    it 'saves review' do
      submit_review

      expect(page).to have_text Spree.t(:review_successfully_submitted)

      expect(product.reviews.count).to eq 1
    end
  end

  context 'with invalid input' do
    context 'with invalid title' do
      let(:review_title) { '   ' }

      it 'does not save review' do
        submit_review

        expect(page).to have_text "Review title can't be blank"

        expect(product.reviews.count).to eq 0
      end
    end

    context 'with invalid rating' do
      let(:rating) { nil }

      it 'does not save review' do
        submit_review

        expect(page).to have_text Spree.t(:review_rating_error)

        expect(product.reviews.count).to eq 0

        within('#product-review-modal') do
          find("a[title='2 stars']", visible: false).click
        end

        expect(page).not_to have_text Spree.t(:review_rating_error)
      end
    end
  end
end
