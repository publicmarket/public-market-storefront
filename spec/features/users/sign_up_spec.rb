RSpec.describe 'Sign Up', type: :feature do
  subject(:sign_up) do
    visit spree.signup_path

    fill_in 'Email', with: email
    fill_in 'Create Password', with: password
    fill_in 'Confirm Password', with: password_confirmation
    page.check 'spree_user_accept_terms'

    click_button 'Sign Up'
  end

  let(:email) { 'email@spree.com' }
  let(:password) { 'password' }
  let(:password_confirmation) { 'password' }

  context 'with valid data' do
    before { sign_up }

    it 'create a new user' do
      expect(Spree::User.count).to eq(1)
    end
  end

  context 'with referral link' do
    let(:referral) { create(:user) }
    let(:refuid) { referral.referral_link_uid }

    before do
      visit spree.root_url(refuid: refuid)
    end

    it 'create a new user' do
      expect {
        sign_up
      }.to change(Spree::User, :count).by(1)

      expect(Spree::User.last.referrer).to eq referral
    end

    context 'with fake uid' do
      let(:refuid) { 'wrong-uid-12024' }

      it 'creates a new user with empty referral' do
        expect {
          sign_up
        }.to change(Spree::User, :count).by(1)

        expect(Spree::User.last.referrer).to be_nil
      end
    end
  end

  context 'with not matching password confirmation' do
    let(:password_confirmation) { '' }

    before { sign_up }

    it 'does not create a new user' do
      expect(page).to have_content("Mismatch – your passwords didn't match. Please try again.")
      expect(Spree::User.count).to eq(0)
    end
  end
end
