shared_context 'with filled stripe credit card in checkout' do
  before do
    setup_stripe_watcher

    fill_stripe_elements('4242424242424242')

    expect(page).to have_text 'BILLING ADDRESS'

    fill_in 'Name on Card', with: 'First name Last name'
    fill_in 'First Name', with: 'First name'
    fill_in 'Last Name', with: 'Last name'
    fill_in 'Street Address', with: 'Alaska'
    fill_in 'City', with: 'Wellington'
    fill_in 'ZIP / Postal Code', with: '94001'
  end
end

shared_context 'with filled stripe card in edit form' do
  before do
    setup_stripe_watcher

    fill_stripe_elements('4000056655665556')

    fill_in 'credit_card_name', with: 'First name Last name'
  end
end
