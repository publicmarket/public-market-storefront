shared_context 'when submit review form' do |args = {}|
  subject(:submit_review) do
    within('#product-review-modal') do
      expect(page).to have_text(/#{Spree.t(:write_review)}/i)

      input_by_char(review_title, 'review_title')
      input_by_char(review_comment, 'review_review')

      find("a[title='#{rating}']", visible: false).click if rating

      click_button Spree.t(:submit_your_review)
    end
  end

  let(:review_title) { 'Super review' }
  let(:review_comment) { 'Review' }
  let(:rating) { '2 stars' }
  let(:product) { create(:product) }
  let(:user) { create(:user) }

  let!(:review) { create(:review, rating: 4, product: product, user: user) } if args[:create_review]

  before do
    login_as(user, scope: :spree_user)

    visit spree.product_path(product)
    expect(page).to have_text(/#{Spree.t(:write_review)}/i)

    click_button Spree.t(:write_review)
  end
end
