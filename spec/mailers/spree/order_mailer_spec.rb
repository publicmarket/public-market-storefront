RSpec.describe Spree::OrderMailer, type: :mailer do
  let(:order) { create(:vendor_order_ready_to_ship) }

  before do
    allow(Spree::Vendor).to receive(:first) { build_stubbed(:vendor) }
  end

  describe '#confirm_email' do
    let(:mail) { described_class.confirm_email(order) }

    it 'contains correct subject' do
      expect(mail.subject).to include('has been placed')
    end

    it 'contains correct body' do
      expect(mail.body).to include('Thanks for your order! We’ll let you know when your items have shipped.')
    end

    it 'container support url' do
      expect(mail.body).to include('freshdesk')
    end

    it 'sends an email' do
      expect {
        mail.deliver_now
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end
end
