RSpec.describe Spree::ShipmentMailer, type: :mailer do
  let(:user) { create :user }
  let(:order) { create(:order_with_vendor_items, user: user) }
  let(:shipment) { order.shipments.first }

  describe '#shipped_email' do
    let(:mail) { described_class.shipped_email(shipment) }

    it 'contains correct subject' do
      expect(mail.subject).to include('has shipped')
    end

    it 'contains correct body' do
      expect(mail.body).to include('Good news!')
    end

    it 'sends an email' do
      expect {
        mail.deliver_now
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end

  describe '#cancel_email' do
    let(:mail) { described_class.cancel_email(shipment) }

    it 'contains correct subject' do
      expect(mail.subject).to include('has been canceled')
    end

    it 'contains correct body' do
      expect(mail.body).to include('your order could not be filled by')
    end

    it 'sends an email' do
      expect {
        mail.deliver_now
      }.to change(ActionMailer::Base.deliveries, :size).by(1)
    end
  end
end
