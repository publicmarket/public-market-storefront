# code from SpreeGateway: https://github.com/spree/spree_gateway/blob/master/spec/support/wait_for_stripe.rb

module WaitForStripe
  def wait_for_stripe
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop until page.evaluate_script('window.activeStripeRequests').zero?
    end
  end

  def setup_stripe_watcher
    page.evaluate_script <<-JS
      window.activeStripeRequests = 0;
      $('#checkout_form_payment [data-hook=buttons]').on('click', function() {
        window.activeStripeRequests = window.activeStripeRequests + 1;
      });
      stripeResponseHandler = (function() {
        var _f = stripeResponseHandler;
        return function() {
          window.activeStripeRequests = window.activeStripeRequests - 1;
          _f.apply(this, arguments);
        }
      })();
    JS
  end

  def fill_stripe_elements(card, expiry: "12/#{Time.current.year + 1}", cvc: '123')
    within_frame('__privateStripeFrame3') do
      input_by_char(card, 'cardnumber')
      input_by_char(expiry, 'exp-date')
      input_by_char(cvc, 'cvc')
    end
  end

  def input_by_char(string, field_path)
    field = find_field(field_path)
    field.native.clear

    string.to_s.chars.each do |piece|
      field.send_keys(piece)
    end
  end
end

RSpec.configure do |config|
  config.include WaitForStripe, type: :feature
end
