require 'sidekiq/testing'
Sidekiq::Testing.inline!

RSpec.configure do |config|
  config.around(:each, enqueue: true) do |example|
    ActiveJob::Base.queue_adapter = :test
    ActiveJob::Base.queue_adapter.enqueued_jobs.clear
    Sidekiq::Testing.fake!
    example.run
    ActiveJob::Base.queue_adapter = :inline
    Sidekiq::Testing.inline!
  end
end

RSpec::Sidekiq.configure do |config|
  # Clears all job queues before each example
  config.clear_all_enqueued_jobs = true # default => true

  # Whether to use terminal colours when outputting messages
  config.enable_terminal_colours = true # default => true

  # Warn when jobs are not enqueued to Redis but to a job array
  config.warn_when_jobs_not_processed_by_sidekiq = false # default => true
end
