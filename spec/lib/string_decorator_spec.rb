RSpec.describe String do
  describe '#smart_titleize' do
    subject { sentence.smart_titleize }

    let(:sentence) { 'super-duper product title' }

    it { is_expected.to eq 'Super-Duper Product Title' }

    context 'with leading space' do
      let(:sentence) { ' a. i. e. (a mwana) ' }

      it { is_expected.to eq 'A. I. E. (a Mwana)' }
    end

    context 'with dashes' do
      let(:sentence) { 'vol. 3 (1959-66)' }

      it { is_expected.to eq 'Vol. 3 (1959-66)' }
    end

    context 'when last word is exclusion' do
      let(:sentence) { 'put in' }

      it { is_expected.to eq 'Put In' }
    end
  end
end
