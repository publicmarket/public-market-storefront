RSpec.describe 'Variants', type: :request do
  let(:user) { create :user, spree_api_key: 'secure', vendors: [vendor] }
  let(:vendor) { order.vendors.first }
  let(:token) { user.spree_api_key }
  let(:order) { create(:vendor_order_ready_to_ship) }
  let(:json) { JSON.parse(response.body, symbolize_names: true) }
  let(:params) { {} }

  describe '#index' do
    let(:variant) { order.inventory_units.first.variant }
    let(:other_variant) { create(:variant, product: variant.product) }

    before { get '/api/v1/variants', params: params, headers: { 'X-Spree-Token': token } }

    it { expect(response.status).to eq(200) }
  end
end
