RSpec.describe Spree::OptionValue, type: :model do
  describe 'name + option_type uniqueness validation' do
    subject(:new_option_value) do
      new_option_value = build(:option_value, name: option_value.name, option_type: option_type)
      new_option_value.save(validate: false)
    end

    let!(:option_value) { create(:option_value) }
    let!(:option_type) { option_value.option_type }

    context 'with not unique name for option type' do
      it { expect { new_option_value }.to raise_error(ActiveRecord::RecordNotUnique) }
    end

    context 'with unique name for option type' do
      let(:option_type) { create(:option_type) }

      it { expect { new_option_value }.not_to raise_error }
    end
  end
end
