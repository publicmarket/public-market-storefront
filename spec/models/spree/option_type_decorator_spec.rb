RSpec.describe Spree::OptionType, type: :model do
  describe 'name uniqueness validation' do
    subject(:new_option_type) do
      new_option_type = build(:option_type, name: name)
      new_option_type.save(validate: false)
    end

    let!(:option_type) { create(:option_type) }
    let!(:name) { option_type.name }

    context 'with not unique name' do
      it { expect { new_option_type }.to raise_error(ActiveRecord::RecordNotUnique) }
    end

    context 'with unique name for option type' do
      let(:name) { 'Unique opt type name' }

      it { expect { new_option_type }.not_to raise_error }
    end
  end
end
