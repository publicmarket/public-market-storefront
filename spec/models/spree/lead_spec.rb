RSpec.describe Spree::Lead, type: :model do
  describe 'email validation' do
    subject { build_stubbed(:lead, email: email) }

    context 'when filled' do
      let(:email) { 'email' }

      it { is_expected.to be_valid }
    end

    context 'when blank' do
      let(:email) { nil }

      it { is_expected.not_to be_valid }
    end
  end

  describe 'email sending', enqueue: true do
    let!(:lead) { create(:lead) }

    it { expect(ActionMailer::DeliveryJob).not_to have_been_enqueued.with('Spree::UserMailer', 'lead_whitelisted', 'deliver_now', lead.id) }
  end

  describe 'email normalization' do
    subject(:lead) do
      lead = build_stubbed(:lead, email: email)
      lead.validate
      lead
    end

    context 'when has whitespaces' do
      let(:email) { ' user@user ' }

      it 'strips white spaces' do
        expect(lead.email).to eq 'user@user'
      end
    end

    context 'when case is incosistent' do
      let(:email) { 'UseR@user' }

      it 'applies downcase' do
        expect(lead.email).to eq 'user@user'
      end
    end
  end
end
