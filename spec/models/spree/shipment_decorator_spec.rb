RSpec.describe Spree::Shipment, type: :model do
  describe '#tracking_url' do
    subject { shipment.tracking_url }

    let(:shipment) { build_stubbed(:shipment, tracking: track_number) }
    let(:track_number) { '1Z5R89390357567127' }

    it { is_expected.to include 'ups.com' }

    context 'when empty' do
      let(:track_number) { nil }

      it { is_expected.to be nil }
    end
  end

  describe 'rewards' do
    let(:order) { create(:vendor_order_ready_to_ship, line_items_count: 2, line_items_quantity: 3) }
    let(:shipment) { order.shipments.first }

    before { order.update_with_updater! }

    it { expect(shipment.rewards).to eq(shipment.line_items.first.final_rewards) }
    it { expect(shipment.cost_rewards_amount).to be_zero }

    context 'when shipment cost is not zero' do
      before do
        shipment.update(cost: 10)
      end

      it { expect(shipment.cost_rewards_amount).to eq(1.5) }
    end
  end

  describe 'splitting' do
    let(:order) { create(:vendor_order_ready_to_ship, line_items_quantity: transfer) }
    let(:shipment) { order.shipments.first }
    let(:unit) { shipment.inventory_units.first }
    let(:variant) { unit.variant }

    before do
      variant.stock_items.first.set_count_on_hand(on_hand)
      shipment.transfer_to_location(unit.variant, transfer, shipment.stock_location)
    end

    context 'when less than on hands' do
      let(:transfer) { 2 }
      let(:on_hand) { 3 }

      it { expect(order.shipments.last.state).to eq('ready') }
      it { expect(unit.reload.state).to eq('on_hand') }
    end

    context 'when transfer more than on hands' do
      let(:transfer) { 2 }
      let(:on_hand) { 1 }

      it { expect(order.shipments.last.state).to eq('ready') }
      it { expect(unit.reload.state).to eq('on_hand') }
    end

    context 'when transfer more than on hands' do
      let(:transfer) { 1 }
      let(:on_hand) { 0 }

      it { expect(order.shipments.last.state).to eq('ready') }
      it { expect(unit.reload.state).to eq('on_hand') }
    end
  end
end
