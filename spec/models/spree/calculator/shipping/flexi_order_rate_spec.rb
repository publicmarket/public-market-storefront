RSpec.describe Spree::Calculator::Shipping::FlexiOrderRate, type: :model do
  let(:vendor) { create(:vendor) }
  let(:shipping_method) { vendor.shipping_methods.first }
  let(:calculator) { described_class.new(preferred_first_item: 4, preferred_additional_item: 1) }
  let(:order) { create :vendor_order_ready_to_ship, vendor: vendor, line_items_count: line_items_count, line_items_quantity: line_items_quantity }
  let(:line_items_quantity) { 1 }
  let(:line_items_count) { 1 }

  before { shipping_method.calculator = calculator }

  context 'when order is created with one item' do
    let(:order) { create :vendor_order_ready_to_ship, vendor: vendor }

    it { expect(order.ship_total).to eq(4) }
  end

  context 'when order is created with two items' do
    let(:line_items_quantity) { 2 }
    let(:shipment) { order.shipments.first }
    let(:unit) { shipment.inventory_units.first }
    let(:variant) { unit.variant }

    it { expect(order.ship_total).to eq(5) }
    it { expect(shipment.cost).to eq(5) }

    context 'when unit is cancelled' do
      before { unit.cancel! }

      it { expect(order.reload.ship_total).to eq(4) }
      it { expect(shipment.reload.cost).to eq(4) }
    end

    context 'when shipment splitted' do
      let(:split_count) { 1 }

      before do
        shipment.transfer_to_location(unit.variant, split_count, shipment.stock_location)
        order.update_with_updater!
      end

      it { expect(order.ship_total).to eq(5) }
      it { expect(shipment.cost_rewards_amount).to eq(5 * 0.15) }

      context 'when split all items' do
        let(:split_count) { line_items_quantity }

        it { expect(order.shipments.count).to eq(1) }
        it { expect(order.ship_total).to eq(5) }
        it { expect(shipment.cost_rewards_amount).to eq(0.75) }
      end

      context 'when last shipment is cancelled', enqueue: true do
        before do
          order.shipments.last.inventory_units.last.cancel!
          order.update_with_updater!
        end

        it { expect(order.ship_total).to eq(4) }
      end
    end
  end

  context 'when order is created with three items' do
    let(:line_items_quantity) { 3 }

    it { expect(order.ship_total).to eq(6) }

    context 'when shipment splitted' do
      let(:shipment) { order.shipments.first }
      let(:unit) { shipment.inventory_units.first }
      let(:variant) { unit.variant }
      let(:split_count) { 1 }

      before do
        shipment.transfer_to_location(unit.variant, split_count, shipment.stock_location)
        order.update_with_updater!
      end

      it { expect(order.shipments.count).to eq(2) }
      it { expect(order.ship_total).to eq(6) }

      context 'when split all items' do
        let(:split_count) { line_items_quantity }

        it { expect(order.shipments.count).to eq(1) }
        it { expect(order.ship_total).to eq(6) }
      end
    end

    context 'when split all items ony-by-one' do
      before do
        shipment = order.shipments.first
        shipment.inventory_units.each do |unit|
          shipment.transfer_to_location(unit.variant, 1, shipment.stock_location)
        end
        order.update_with_updater!
      end

      it { expect(order.shipments.count).to eq(3) }
      it { expect(order.ship_total).to eq(6) }
    end
  end

  context 'when order created with multiple line items' do
    let(:line_items_count) { 2 }
    let(:shipment) { order.shipments.first }
    let(:line_item) { shipment.line_items.first }

    it { expect(order.ship_total).to eq(5) }
    it { expect(shipment.cost_rewards_amount).to eq(0.75) }

    context 'when shipment is splitted' do
      let(:other_shipment) { order.shipments.where.not(id: shipment.id).first }

      before do
        shipment.transfer_to_location(line_item.variant, 1, shipment.stock_location)
        order.update_with_updater!
      end

      it { expect(order.ship_total).to eq(5) }
      it { expect(shipment.reload.cost_rewards_amount).to eq(0.15) }
      it { expect(other_shipment.reload.cost_rewards_amount).to eq(0.6) }
    end
  end
end
