RSpec.describe Spree::InventoryUnit, type: :model, enqueue: true do
  describe 'hash_id' do
    let(:unit) { create(:inventory_unit) }

    it { expect(unit.hash_id.to_i).to be > 0 }
    it { expect(described_class.decoded_id(unit.hash_id)).to eq(unit.id) }
    it { expect(described_class.find_by_hash_id(unit.hash_id)).to eq(unit) } # rubocop:disable Rails/DynamicFindBy
  end

  describe 'state machine' do
    subject(:state) { unit.state }

    let(:unit) { create(:inventory_unit) }

    it { is_expected.to eq('on_hand') }

    context 'when cancel' do
      before { unit.cancel! }

      it { is_expected.to eq('canceled') }
      it { expect(unit.line_item.quantity).to eq(0) }
    end

    context 'when ship' do
      before { unit.ship! }

      it { is_expected.to eq('shipped') }
    end
  end

  describe 'shipments and order' do
    let(:order) { create(:vendor_order_ready_to_ship, line_items_count: count) }
    let(:shipment) { order.shipments.first }
    let(:unit) { shipment.inventory_units.first }

    context 'when cancel' do
      before { unit.cancel! }

      context 'when one line item' do
        let(:count) { 1 }

        it { expect(unit.line_item.quantity).to eq(0) }
        it { expect(order.reload.state).to eq('canceled') }
        it { expect(shipment.reload.state).to eq('canceled') }
        it do
          expect(ActionMailer::DeliveryJob).to have_been_enqueued.exactly(:once)
                                                                 .with('Spree::ShipmentMailer', 'cancel_email', 'deliver_now', shipment.id)
        end
      end

      context 'when multiple line items' do
        let(:count) { 2 }

        it { expect(unit.line_item.quantity).to eq(0) }
        it { expect(order.reload.state).to eq('complete') }
        it { expect(shipment.reload.state).to eq('canceled') }
        it 'enqueues email' do
          expect(ActionMailer::DeliveryJob).to have_been_enqueued.exactly(:once)
                                                                 .with('Spree::ShipmentMailer', 'cancel_email', 'deliver_now', shipment.id)
        end
      end
    end

    context 'when confirm' do
      let(:order) { create(:vendor_order_ready_to_ship, line_items_quantity: 5) }

      before { order.inventory_units.each(&:ship!) }

      it { expect(order.inventory_units.map(&:state)).to all(eq('shipped')) }
      it { expect(order.reload.state).to eq('complete') }
      it { expect(shipment.reload.state).to eq('shipped') }
      it 'enqueues emails' do
        expect(ActionMailer::DeliveryJob).to have_been_enqueued.exactly(:once)
                                                               .with('Spree::ShipmentMailer', 'shipped_email', 'deliver_now', shipment.id)

        expect(ActionMailer::DeliveryJob).to have_been_enqueued.exactly(:once)
                                                               .with('Spree::ShipmentMailer', 'rate_email', 'deliver_now', shipment.id)
      end
    end
  end

  describe 'create units for each quantity' do
    let(:quantity) { 5 }
    let(:order) { create(:order_with_vendor_items, line_items_quantity: quantity) }
    let(:line_item) { order.line_items.first }

    it { expect(line_item.inventory_units.count).to eq(quantity) }
    it { expect(line_item.inventory_units.first.quantity).to eq(1) }
  end
end
